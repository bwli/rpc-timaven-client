let timeAssignment = function () {

    let selectedEmployeeId = [];
    let employeeMap = {};

    $.fn.regexMask = function (mask) {
        $(this).keypress(function (event) {
            if (!event.charCode) return true;
            let part1 = this.value.substring(0, this.selectionStart);
            let part2 = this.value.substring(this.selectionEnd, this.value.length);
            if (!mask.test(part1 + String.fromCharCode(event.charCode) + part2)) {
                return false;
            }
        });
    };

    function init() {

        $("#btn-apply-ok").on("click", function () {
            let hour = parseFloat($("#timeModal").find("#input-apply-time").val());
            $("table.grid-view tbody tr").each(function () {
                $(this).find('input[name="input-time"]:visible').first().val(hour);
            });
        });

        $('#timeModal').on('shown.bs.modal', function (e) {
            $('#input-apply-time').focus();
        })

        $('#timeModal').on('hidden.bs.modal', function (e) {
            setTimeout(function () {
                $("table.grid-view tbody").find('input[name="input-time"]').first().focus();
            }, 1);
        });

        employeeMap = employees.reduce(function (map, obj) {
            map[obj.empId] = obj;
            return map;
        }, {});

        $('#sl-employee').change(function () {
            let val = $(this).val();
            if (!selectedEmployeeId.includes(val)) {
                let employee = employeeMap[val];
                selectedEmployeeId.push(val);

                let tr = `<tr data-emp-id="${employee.empId}">` +
                    '<td style="vertical-align: middle">' +
                    `<span data-type="content">${employee.lastName}, ${employee.firstName}</span>` +
                    '</td>' +
                    '<td style="vertical-align: middle">' +
                    `<span data-type="content">${employee.craft}</span>` +
                    '</td>' +
                    '<td style="vertical-align: middle">' +
                    `<span data-type="content">${employee.crew}</span>` +
                    '</td>' +
                    '<td>' +
                    '<label>' +
                    '<input class="form-control positive-num" min="0" step="any" name="input-time" type="number"/>' +
                    '</label>' +
                    '</td>' +
                    '</tr>';
                $("table.grid-view tbody").append(tr);
            }
        })

        initEvent();
    }

    function initEvent() {
        let mask = new RegExp(/^[0-9.]*$/);
        $(".positive-num").regexMask(mask)

        let elements = $("table.grid-view tbody").find('input[name="input-time"]');
        elements.keydown(function (event) {
            focusToNext.call(this, event, elements);
        });

    }

    function saveTimeAssignment() {

        let table = $("table.grid-view").first();
        let allocationTimes = [];
        table.find('tbody tr').each(function () {
            let empId = $(this).data('emp-id');
            let inputTime = $(this).find('td input[name="input-time"]').first().val();
            let employee = employeeMap[empId];
            let allocationTime = {
                firstName: employee.firstName,
                lastName: employee.lastName,
                empId: employee.empId,
                teamName: employee.teamName,
                totalHour: inputTime,
                netHour: inputTime,
            }
            allocationTimes.push(allocationTime);
        });
        let allocationSubmission = {
            projectId: sessionProjectId,
            dateOfService: dateOfService,
            payrollDate: payrollDate,
            teamName: teamName,
            allocationTimes: allocationTimes,
            status: 'PENDING',
            submitUserId: submitUserId,
            isManual: true,
        }
        submitModule.saveTimeAllocation(allocationSubmission, 'save-time', 'timesheets');
    }

    return {
        init: init,
        // switchView: switchView,
        saveTimeAssignment: saveTimeAssignment,
    }
}();

$.ajaxSetup({
    crossDomain: true,
    xhrFields: {
        withCredentials: true
    },
    beforeSend: function (xhr, settings) {
        if (settings.type == 'POST' || settings.type == 'PUT'
            || settings.type == 'DELETE') {
            if (!(/^http:.*/.test(settings.url) || /^https:.*/
                .test(settings.url))) {
                // Only send the token to relative URLs i.e. locally.
                xhr.setRequestHeader("X-XSRF-TOKEN",
                    Cookies.get('XSRF-TOKEN'));
            }
        }
    }
});


let token = function () {
    function getAccessToken(callback) {
        $.ajax({
            type: 'POST',
            url:  "access_token",
            dataType: "text",
            success: function (token) {
                // Cookies.set('access_token', token, {
                //     path: 'timekeeping'
                // });
                if(callback) {
                    callback(token);
                }
            },
            error: function (xhr, status, error) {
                if (401 === xhr.status || 403 === xhr.status) {
                    window.location.href = "home";
                } else {
                    alert(error);
                }
            },
            async: false
        });
    }
    return {
        getAccessToken: getAccessToken
    }
}();

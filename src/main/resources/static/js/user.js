let userModule = function () {

    function init() {
        let teamSelector = $('input[data-role="foreman"]');
        teamSelector.change(function() {
            selectChange($(this));
        });
        selectChange(teamSelector);
    }

    function selectChange(input) {
        let checked = $(input).is(':checked');
        let divTeamSelect = $('#div-teams');
        if (checked) {
            $(divTeamSelect).show();
            $(divTeamSelect).removeClass("hidden").addClass("visible")
        } else {
            $(divTeamSelect).hide();
            $(divTeamSelect).removeClass("visible").addClass("hidden")
        }
    }

    return {
        init: init,
    }
}();

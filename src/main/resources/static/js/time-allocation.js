let timeAllocation = function () {
    let allocationTimeMap;
    $.fn.regexMask = function (mask) {
        $(this).keypress(function (event) {
            if (!event.charCode) return true;
            let part1 = this.value.substring(0, this.selectionStart);
            let part2 = this.value.substring(this.selectionEnd, this.value.length);
            if (!mask.test(part1 + String.fromCharCode(event.charCode) + part2)) {
                return false;
            }
        });
    };

    function initGridViewListener() {
        $('button.remove-cost-code').on("click", function () {
            let ndx = $(this).closest("th").index();
            let table = $(this).closest("table");
            table.find("tr").find("td:eq(" + ndx + "), th:eq(" + ndx + ")").remove();
            // updateAllListTableCCS();
            calculateUnallocatedTimeForAll();
        });
        $('input.input-gv').on("change", function () {
            let tr = $(this).closest("tr");
            calculateUnallocatedTime(tr);
            calculateCostCodeTime($(this));
        });
        $(".apply-percentage").on("click", function (e) {
            e.preventDefault();
            let th = $(this).closest('th');
            let colIndex = th.parent().children().index(th);
            updateDataAttr($('#btn-apply-ok'), "col-index", colIndex);
            $('#input-apply-perc').val(100);
            $('#percModal').modal('show');
        });
        $('input[data-name="mobMileage"]').on("change", function () {
            let mobMileageRate = rule.mobMileageRate ? rule.mobMileageRate : 0;
            let mobMileage = parseFloat($(this).val());
            let mobAmount = (mobMileageRate * mobMileage).toFixed(2);
            $(this).closest('td').find('span[data-name=mobAmount]').text(mobAmount)
            let checked = !!mobMileage;
            let td = $(this).closest("td")
            $(td).find('input[name="inputHasMob"]').prop("checked", checked);
        });

        $('.add-tag').on('click', function () {
            let buttonName = $(this).attr("name");
            let btnOk = $('#btn-add-tag-ok');
            if (buttonName === 'perDiemAddTag') {
                let empId = $(this).closest('tr').data('emp-id');
                btnOk.data('tag-type', 'perDiem')
                btnOk.removeData('col-index');
                btnOk.data('emp-id', empId);
            } else if (buttonName === 'mobAddTag') {
                let empId = $(this).closest('tr').data('emp-id');
                btnOk.data('tag-type', 'mob')
                btnOk.removeData('col-index');
                btnOk.data('emp-id', empId);
            } else {
                let ndx = $(this).closest("th").index();
                btnOk.data('tag-type', 'labor')
                btnOk.data('col-index', ndx);
                btnOk.removeData('emp-id');
            }
            $('#addTagModal').modal('show');
        });
        $('#deleteModal').on("show.bs.modal", function (e) {
            // set the username
            let deleteBtn = $(e.relatedTarget);
            let id = deleteBtn.data('id');
            let index = deleteBtn.data('index');
            let empId = deleteBtn.data('emp-id');
            let dos = deleteBtn.data('dos');
            let team = deleteBtn.data('team');
            let okBtn = $('#btn-delete-time');
            okBtn.data('id', id);
            okBtn.data('index', index);
            okBtn.data('emp-id', empId);
            okBtn.data('dos', dos);
            okBtn.data('team', team);
        });
        $('#btn-delete-time').on('click', function () {
            let id = $(this).data('id');
            let index = $(this).data('index');
            let empId = $(this).data('emp-id');
            let dos = $(this).data('dos');
            let team = $(this).data('team');
            let url;
            if (!!id) {
                url = `allocation-times?id=${id}`;
            } else {
                let teamQuery = !!team ? `&teamName=${team}` : '';
                url = `allocation-times?empId=${empId}&dateOfService=${dos}${teamQuery}`
            }
            $(function () {
                // noinspection JSUnresolvedFunction
                $.ajax({
                    type: "DELETE",
                    url: url,
                    success: function (callback) {
                        $(`tr[data-index=${index}]`).remove();
                        delete allocationTimeMap[index];
                        calculateCostCodeTimeForAll();
                    },
                    error: function () {
                        alert("delete failed");
                    }
                });
            });

        });
        // fixateLeftPanel($('#tb-time'), 6);
        // $(window).resize(function() {
        //     fixateLeftPanel($('#tb-time'), 6);
        // });
    }

    //
    // function fixateLeftPanel(table, nColumn) {
    //
    //     $(function() {
    //         // TM-297  (start) fixed table columns
    //         // thead fixed
    //
    //         table.find(`th:nth-child(-n + ${nColumn}), td:nth-child(-n + ${nColumn})`)
    //             .css("position", "sticky")
    //             .css("background-color", "white");
    //
    //         table.find(`th:nth-child(-n + ${nColumn})`)
    //             .css("z-index", 15)
    //         table.find(`td:nth-child(-n + ${nColumn})`)
    //             .css("z-index", 13)
    //
    //         let left = 0;
    //         for (let i = 1; i <= nColumn; i++) {
    //             let thTd = table.find(`th:nth-child(${i}), td:nth-child(${i})`);
    //             thTd.css("left", left);
    //             left += thTd.first().outerWidth();
    //         }
    //     });
    // }

    function calculateUnallocatedTimeForAll() {
        $("table.grid-view tbody tr").each(function () {
            calculateUnallocatedTime($(this));
        });
    }

    function calculateCostCodeTimeForAll() {
        $("table.grid-view tbody tr:first input.input-gv").each(function () {
            calculateCostCodeTime($(this));
        })
    }

    function calculateCostCodeTime(input) {
        let td = $(input).closest('td');
        let colIndex = td.parent().children().index(td);
        let table = td.closest('table');
        let inputs = table.find('tbody td:nth-child(' + (colIndex + 1) + ') input.input-gv');
        let sum = 0;
        $(inputs).each(function () {
            if ($(this).val()) {
                let val = $(this).val();
                val = !!val ? val : 0;
                sum += Number(val);
                sum = parseFloat(sum.toFixed(2));
            }
        });
        sum = parseFloat(sum.toFixed(2));
        let span = table.find('thead th:nth-child(' + (colIndex + 1) + ') span[data-name="allocated-hour"]');
        span.text(sum);
    }

    function calculateUnallocatedTime(tr) {
        let cur_index = parseInt($(tr).data('index'));
        let allocationTime = allocationTimeMap[cur_index];
        let totalHour = allocationTime.totalHour;

        let allocatedTime = 0.0;
        let tdInputs = tr.find('td input[name="subTotalHours"]');
        tdInputs.each(function () {
            let val = $(this).val();
            val = !!val ? val : 0;
            allocatedTime = allocatedTime + parseFloat(val);
        });
        allocatedTime = parseFloat(allocatedTime.toFixed(2));
        let unallocatedTime = parseFloat((totalHour - allocatedTime).toFixed(2));
        tr.find('td[data-type="adjusted-time"]').first().text(unallocatedTime);
        updateDataAttr(tr, "allocated-time", allocatedTime);
        updateDataAttr(tr, "unallocated-time", unallocatedTime);
        let perDiem = allocationTime.perDiem;
        if (perDiem) {
            let isManual = perDiem.perDiemMode === 'MANUAL';
            if (!isManual && !initialize) {
                let hasPerDiem = allocationTime.employeeHasPerDiem;
                let perDiemThreshold = perDiem.dailyThreshold;
                let perDiemInput = $(tr).find('input[name="manualPerDiem"]');
                if (hasPerDiem && allocatedTime >= perDiemThreshold) {
                    $(perDiemInput).prop('checked', true).change();
                } else {
                    $(perDiemInput).prop('checked', false).change();
                }
            }
        }
        let hasRigPay = allocationTime.employeeHasRigPay;
        let rigPayThreshold = rule.rigPayThreshold;
        let rigPaySpan = $(tr).find('td.td-rig-pay span[name="rigPayAmount"]');
        if (hasRigPay && allocatedTime >= rigPayThreshold) {
            $(rigPaySpan).show();
            $(rigPaySpan).removeClass("hidden").addClass("visible")
        } else {
            $(rigPaySpan).hide();
            $(rigPaySpan).removeClass("visible").addClass("hidden")
        }
    }

    let initialize = true;

    function init() {
        allocationTimeMap = allocationTimes.reduce((map, val, index) => {
            map[index] = val
            return map;
        }, {});
        $("input[type='number']").on("click", function () {
            $(this).focus();
        });

        initGridViewListener();

        calculateUnallocatedTimeForAll();
        calculateCostCodeTimeForAll();

        $('#sl-cost-code-type').on('change', function () {
            let divRainOut = $('#div-rain-out-option');
            if ($(this).val() !== 'rain_out') {
                divRainOut.hide();
            } else {
                divRainOut.show();
            }
        });

        $("#btn-add-new").on("click", function (e) {

            $("select[name='costCode'] option").filter(function () {
                return this.value !== '';
            }).attr('disabled', false);
            let costCodeTypeOptions = $('#sl-cost-code-type option');
            costCodeTypeOptions.filter(function () {
                return this.value === 'default' || this.value === 'rain_out';
            }).show();
            costCodeTypeOptions.filter(function () {
                return this.value === 'default';
            }).prop('selected', true);
            costCodeTypeOptions.filter(function () {
                return this.value !== 'default' && this.value !== 'rain_out';
            }).hide();

            let modal = $('#myModal');
            modal.find('#btn-ok').removeData('emp-id')
            modal.modal('show');
        });

        $('button[name="perDiemBtn"]').on('click', function (e) {
            $("select[name='costCode'] option").filter(function () {
                return this.value !== '';
            }).attr('disabled', false);
            let costCodeTypeOptions = $('#sl-cost-code-type option');
            costCodeTypeOptions.filter(function () {
                return this.value !== 'per_diem';
            }).hide();
            costCodeTypeOptions.filter(function () {
                return this.value === 'per_diem';
            }).prop('selected', true).show();

            let empId = $(this).closest('tr').data('emp-id');
            let modal = $('#myModal');
            modal.find('#btn-ok').data('emp-id', empId);
            modal.modal('show');
        });

        $('button[name="mobBtn"]').on('click', function (e) {
            $("select[name='costCode'] option").filter(function () {
                return this.value !== '';
            }).attr('disabled', false);
            let costCodeTypeOptions = $('#sl-cost-code-type option');
            costCodeTypeOptions.filter(function () {
                return this.value !== 'mob';
            }).hide();
            costCodeTypeOptions.filter(function () {
                return this.value === 'mob';
            }).prop('selected', true).show();

            let empId = $(this).closest('tr').data('emp-id');
            let modal = $('#myModal');
            modal.find('#btn-ok').data('emp-id', empId);
            modal.modal('show');
        });

        $("#btn-copy-previous").on("click", function (e) {
            $.ajax({
                type: "GET",
                url: `last-cost-code-tags?teamName=${teamName}&dateOfService=${dateOfService}`,
                success: function (data) {
                    let costCodeTagDtos = data["costCodeTagDtos"];
                    let empIdPerDiemCostCodeTagAssociationMap = new Map(Object.entries(data["empIdPerDiemCostCodeTagAssociationsMap"]));
                    let empIdMobCostCodeTagAssociationMap = new Map(Object.entries(data["empIdMobCostCodeTagAssociationsMap"]));
                    let empIdPerDiemCostCodeMap = new Map(Object.entries(data['empIdPerDiemCostCodeMap']));
                    let empIdMobCostCodeMap = new Map(Object.entries(data['empIdMobCostCodeMap']));
                    if (costCodeTagDtos && costCodeTagDtos.length > 0) {
                        $.each(costCodeTagDtos, function (index, value) {
                            let costCode = value['costCodeFull'];
                            let ndx = gridViewAddCostCode(costCode);
                            let costCodeTagAssociationDtos = value['costCodeTagAssociationDtos'];
                            $.each(costCodeTagAssociationDtos, function (index, value) {
                                let tagType = value['tagType'];
                                let typeDesc = value['typeDescription'];
                                let code = value['codeName'];
                                let desc = value['codeDescription'];
                                addTag($("table.grid-view").find("tr").find('th:eq(' + ndx + ') ol[data-name="ol-tag"]'), tagType, typeDesc, code, desc);
                            });
                        });
                    }
                    if (empIdPerDiemCostCodeTagAssociationMap && empIdPerDiemCostCodeTagAssociationMap.size > 0) {
                        empIdPerDiemCostCodeTagAssociationMap.forEach((associations, key, map) => {
                            let empId = key;
                            if (associations && associations.length > 0) {
                                $.each(associations, function (index, value) {
                                    let tagType = value['tagType'];
                                    let typeDesc = value['typeDescription'];
                                    let code = value['codeName'];
                                    let desc = value['codeDescription'];
                                    addTag($("table.grid-view").find(`tr[data-emp-id='${empId}'] td.td-per-diem ol[data-name="ol-tag"]`), tagType, typeDesc, code, desc);
                                });
                            }
                        })
                    }
                    if (empIdMobCostCodeTagAssociationMap && empIdMobCostCodeTagAssociationMap.size > 0) {
                        empIdMobCostCodeTagAssociationMap.forEach((associations, key, map) => {
                            let empId = key;
                            if (associations && associations.length > 0) {
                                $.each(associations, function (index, value) {
                                    let tagType = value['tagType'];
                                    let typeDesc = value['typeDescription'];
                                    let code = value['codeName'];
                                    let desc = value['codeDescription'];
                                    addTag($("table.grid-view").find(`tr[data-emp-id='${empId}'] td.td-mob ol[data-name="ol-tag"]`), tagType, typeDesc, code, desc);
                                });
                            }
                        })
                    }
                    if (empIdPerDiemCostCodeMap && empIdPerDiemCostCodeMap.size > 0) {
                        empIdPerDiemCostCodeMap.forEach((costCode, key) => {
                            let span = $(`#tb-time tr[data-emp-id="${key}"] span.per-diem-span`);
                            span.text(costCode);
                        });
                    }
                    if (empIdMobCostCodeMap && empIdMobCostCodeMap.size > 0) {
                        empIdMobCostCodeMap.forEach((costCode, key) => {
                            let span = $(`#tb-time tr[data-emp-id="${key}"] span.mob-span`);
                            span.text(costCode);
                        });
                    }
                },
            });
        });

        $("#btn-ok").on("click", function () {
            let select = $("#myModal").find("select[name='costCode']");
            let costCode = select.first().val();
            if (costCode) {

                let costCodeType = $('#sl-cost-code-type').val();

                if (costCodeType === 'per_diem') {
                    let empId = $(this).data('emp-id');
                    let span = $(`#tb-time tr[data-emp-id="${empId}"] span.per-diem-span`);
                    span.text(costCode);
                } else if (costCodeType === 'mob') {
                    let empId = $(this).data('emp-id');
                    let span = $(`#tb-time tr[data-emp-id="${empId}"] span.mob-span`);
                    span.text(costCode);
                } else {
                    if (costCodeType === 'default' || costCodeType === 'rain_out') {
                        gridViewAddCostCode(costCode);
                        if (costCodeType === 'rain_out') {
                            $('#sl-cost-code-type option[value="rain_out"]').remove();
                            let rainOutTime = $("#myModal").find("select[name='rain-out-policy']").first().val();
                            let table = $("#tb-time").first();
                            let colIndex = table.find(".th-per-diem").index() - 1;
                            table.find('tbody tr:visible').each(function () {
                                let input = $(this).find(`td:nth-child(${colIndex + 1}) input[data-type="allocation"]`).first();
                                let unallocatedTime = parseFloat($(this).data('unallocated-time'));
                                let inputException = $(this).find('td input[data-type="exception"]').first();
                                let val = parseFloat(input.val());
                                let newVal = parseFloat((val + unallocatedTime).toFixed(2));
                                if (newVal < rainOutTime) {
                                    input.val(rainOutTime);
                                    let exception = inputException.val();
                                    if (!exception.includes("Rain out")) {
                                        inputException.val(exception + "Rain out. ");
                                    }
                                } else {
                                    input.val(newVal);
                                }
                            });
                            $('#btn-rain-out').remove();
                            calculateUnallocatedTimeForAll();
                            let firstInput = $(`#tb-time tbody tr:visible td:nth-child(${colIndex + 1}) input[data-type="allocation"]`).first();
                            calculateCostCodeTime(firstInput);
                        }
                    }
                }
            }
        });

        $("#btn-apply-ok").on("click", function () {
            let perc = $("#percModal").find("#input-apply-perc").val();
            perc = parseInt(perc);
            if (perc > 100) {
                perc = 100
            } else if (perc < 0) {
                perc = 0
            }

            let colIndex = parseInt($(this).data("col-index"));
            $(`table.grid-view tbody tr:visible`).each(function () {
                let allocatedToThisVal = $(this).find(`td:nth-child(${colIndex + 1}) input[data-type="allocation"]`).first().val();
                allocatedToThisVal = !!allocatedToThisVal ? allocatedToThisVal : 0;
                let allocatedToThis = parseFloat(allocatedToThisVal);
                let unallocatedTime = parseFloat($(this).find('td[data-type="adjusted-time"]').first().text());
                // if (unallocatedTime < 0) {
                //     unallocatedTime = 0;
                // }
                let applyTime;
                if (perc === 100) {
                    applyTime = allocatedToThis + unallocatedTime;
                } else {
                    // * 10 / 10 to round number
                    applyTime = allocatedToThis + Math.round(unallocatedTime * perc * 10 / 100) / 10;
                }
                applyTime = parseFloat(applyTime.toFixed(2));
                $(this).find(`td:nth-child(${colIndex + 1}) input[data-type="allocation"]`).first().val(applyTime);
            });
            let firstInput = $(`table.grid-view tbody tr:visible td:nth-child(${colIndex + 1}) input[data-type="allocation"]`).first();
            calculateCostCodeTime(firstInput);
            calculateUnallocatedTimeForAll();
        });

        popoverOptions = {
            content: function () {
                let empId = $(this).data('emp-id');
                return $('#popover-' + empId).html();
            },
            animation: false,
            placement: 'bottom',
            html: true
        };

        $('.info-circle').popover(popoverOptions);

        // Dismiss bootstrap popover by clicking outside
        $('body').on('click', function (e) {
            //did not click a popover toggle, or icon in popover toggle, or popover
            if ($(e.target).data('bs-toggle') !== 'popover'
                && $(e.target).parents('[data-bs-toggle="popover"]').length === 0
                && $(e.target).parents('.popover.in').length === 0) {
                $('[data-bs-toggle="popover"]').popover('hide');
            }
        });

        // Init filter
        $(".checkbox-menu").on("change", "input[type='checkbox']", function () {
            $(this).closest("li").toggleClass("active", this.checked);
        });

        $(document).on('click', '.allow-focus', function (e) {
            e.stopPropagation();
        });

        $('#form-allocation').validate({
            //Otherwise, validator will use title as error message
            ignoreTitle: true,
            onfocusout: false,
            invalidHandler: function (form, validator) {
                let errors = validator.numberOfInvalids();
                if (errors) {
                    validator.errorList[0].element.focus();
                }
            }
        });

        let elements = $("#tb-time tbody").find('input[data-type="allocation"],input[data-type="exception"]');
        elements.keydown(function (event) {
            focusToNext.call(this, event, elements);
        });

        let sortableDiv = $("th.sortable span[data-type='title']").closest('div');
        sortableDiv.append('<i data-type="i-sort" class="fa fa-fw fa-sort"></i>')
        sortableDiv.css('cursor', 'pointer');
        sortableDiv.on("click", function () {
            let asc = $(this).find('i[data-type="i-sort"]').hasClass('fa-sort-up');
            let desc = $(this).find('i[data-type="i-sort"]').hasClass('fa-sort-down');
            // reset all icons
            let th = $(this).closest('th');
            let colIndex = th.parent().children().index(th);
            sortableDiv.find('i[data-type="i-sort"]').removeClass('fa-sort-up fa-sort-down').addClass('fa-sort');
            if (asc) {
                $(this).find('i[data-type="i-sort"]').addClass('fa-sort-down').removeClass('fa-sort');
            } else if (!asc && !desc) {
                $(this).find('i[data-type="i-sort"]').addClass('fa-sort-up').removeClass('fa-sort');
            }
            let tbody = $("#tb-time tbody");
            tbody.find('tr').sort(function (a, b) {
                if (asc) {
                    return $('td:nth-child(' + (colIndex + 1) + ') span[data-type="content"]', b).text().localeCompare($('td:nth-child(' + (colIndex + 1) + ') span[data-type="content"]', a).text());
                } else if (!desc) {
                    return $('td:nth-child(' + (colIndex + 1) + ') span[data-type="content"]', a).text().localeCompare($('td:nth-child(' + (colIndex + 1) + ') span[data-type="content"]', b).text());
                } else {
                    return parseInt($(a).data('index')) - parseInt($(b).data('index'));
                }
            }).appendTo(tbody);
        });

        addRemoveTagEvent();

        $('select[name="select-tag"]').change(function () {
            let typeId = $(this).val();
            $.ajax({
                type: "POST",
                url: `cost-code-tags?typeId=${typeId}`,
                success: function (data) {
                    let select = $("select[name='select-code']");
                    let selectOptionsStr = '';
                    if (data.length !== 0) {
                        select.find("option").remove();
                        $.each(data, function (index, value) {
                            selectOptionsStr += `<option data-id="${value['id']}" data-code="${value['codeName']}" data-desc="${!!value['description'] ? value['description'] : ''}" value="${value['id']}">${value['codeName']}${!!value['description'] ? ' | ' + value['description'] : ''}</option>`;
                        });
                        select.append(selectOptionsStr);
                    } else {
                        select.find('option').remove();
                        select.append('<option selected disabled hidden>Select</option>');
                    }
                },
            });
        });
        $("#btn-add-tag-ok").on("click", function () {
            let modal = $("#addTagModal");
            let typeOption = modal.find("select[name='select-tag']").find(":selected");
            let codeOption = modal.find("select[name='select-code']").find(":selected");
            let typeText = ('' + typeOption.text()).trim();
            let typeDesc = typeOption.data('desc');
            typeDesc = !!typeDesc ? typeDesc : '';
            let code = ('' + codeOption.data('code')).trim();
            let desc = codeOption.data('desc');
            desc = !!desc ? desc : '';
            if (!code) return;
            if ($(this).data('tag-type') === 'perDiem') {
                let empId = $(this).data('emp-id');
                addTag($("table.grid-view").find(`tr[data-emp-id='${empId}'] td.td-per-diem ol[data-name="ol-tag"]`), typeText, typeDesc, code, desc);
            } else if ($(this).data('tag-type') === 'mob') {
                let empId = $(this).data('emp-id');
                addTag($("table.grid-view").find(`tr[data-emp-id='${empId}'] td.td-mob ol[data-name="ol-tag"]`), typeText, typeDesc, code, desc);
            } else {
                let ndx = $(this).data('col-index');
                addTag($("table.grid-view").find("tr").find('th:eq(' + ndx + ') ol[data-name="ol-tag"]'), typeText, typeDesc, code, desc);
            }
        });
        initialize = false
    }

    function addTag(ol, typeText, typeDesc, code, desc) {
        if (ol.length > 0) {
            ol.append(
                `<li class="list-group-item d-flex justify-content-between align-items-start">
                <div class="ms-2 me-auto">
                    <span data-name="tag" class="fw-bold">${typeText}</span> :
                    <span data-name="code" style="font-weight: 100">${code}</span>
                    <div style="position: absolute; left: -999em">
                        <span data-name="type-desc" style="visibility: hidden">${typeDesc}</span>
                        <span data-name="code-desc" style="visibility: hidden">${desc}</span>
                    </div>
                </div>
                <span style="color: red" data-name="remove-tag">
                    <i class="fas fa-minus-circle"></i>
                </span>
            </li>`
            );
            addRemoveTagEvent();
        }
    }

    function addRemoveTagEvent() {
        $('span[data-name="remove-tag"]').on('click', function () {
            $(this).closest('li').remove();
        });
    }

    function gridViewAddCostCode(costCode) {
        let table = $("table.grid-view").first();
        let newColumnIndex = table.find("tr .th-per-diem").index();


        table.find("tr").each(function (index) {
            if (index === 0) {
                // noinspection JSCheckFunctionSignatures
                $(`
                    <th style="vertical-align: top">
                        <div style="white-space: nowrap">
                            <span data-type="costCode">${costCode}</span>
                            <button type="button" class="btn btn-danger btn-sm remove-cost-code" style="margin-left: 15px;">
                                <i class="fas fa-minus"></i>
                            </button> 
                            <button type="button" class="btn btn-primary btn-sm apply-percentage" style="margin-left: 5px;" data-toggle="modal" data-target="#percModal">
                                <i class="fas fa-angle-double-right"></i>
                            </button> 
                            <button type="button" name="laborAddTag" class="btn btn-info btn-sm add-tag" style="margin-left: 5px;">
                                <i class="fas fa-tags"></i>
                            </button>
                        </div>
                        <div class="mt-2">
                            Allocated <span data-name="allocated-hour">0</span> hours
                        </div>
                        <div class="mt-2">
                            <ol data-name="ol-tag" class="list-group list-group-numbered">
                            </ol>
                        </div>
                    </th>
                `).insertBefore($(this).find(".th-per-diem"));
            } else {
                // noinspection JSCheckFunctionSignatures
                $(`
                    <td>
                        <input class="form-control input-gv" 
                            data-type="allocation"
                            name="subTotalHours"
                            type="number" step="any" value="0"/>
                    </td>    
                `).insertBefore($(this).find(".td-per-diem"));
            }
        });
        initGridViewListener();
        return newColumnIndex;
    }

    function updateDataAttr(element, name, value) {
        // Update Dom
        $(element).attr("data-" + name, JSON.stringify(value));
        // Update real data
        $(element).data(name, value);
    }

    function saveTimeDistribution(newStatus, url, successPage) {

        if ((newStatus === 'RELEASED' || 'APPROVED' === newStatus) && !$('#form-allocation').valid()) {
            alert('Please fill the highlighted exception and try again');
            return;
        }

        let status = null;
        if (!newStatus) {
            status = $('#div-status').data('status').toUpperCase();
        } else {
            status = newStatus
        }

        let table = $("table.grid-view").first();

        let mobMileageRate = rule.mobMileageRate ? rule.mobMileageRate : 0;
        table.find('tbody tr').each(function () {
            let allocatedHour = $(this).data("allocated-time");
            let costCodePercs = [];
            let tdInputs = $(this).find('td input[name="subTotalHours"]');
            let perDiemInput = $(this).find('td.td-per-diem input[name=perDiemAmount]');
            let hasPerDiem = perDiemInput.hasClass('visible');
            tdInputs.each(function () {
                let td = $(this).closest('td');
                let col = $(td).parent().children().index(td);
                let th = table.find('th').eq(td.index());
                let costCode = th.find('span[data-type="costCode"]').text().trim();
                let tagLi = th.find('ol[data-name="ol-tag"] li');
                let tags = [];
                tagLi.each(function () {
                    let tag = $(this).find('span[data-name="tag"]').text().trim();
                    let typeDesc = $(this).find('span[data-name="type-desc"]').text().trim();
                    let code = $(this).find('span[data-name="code"]').text().trim();
                    let codeDesc = $(this).find('span[data-name="code-desc"]').text().trim();
                    let costCodeTagAssociation = {
                        tagType: tag,
                        typeDescription: typeDesc,
                        codeName: code,
                        codeDescription: codeDesc,
                    };
                    tags.push(costCodeTagAssociation);
                });
                let subTotalHours = $(this).val();
                subTotalHours = !!subTotalHours ? subTotalHours : 0;
                let subTotalHoursFloat = parseFloat(subTotalHours);
                let percentage = allocatedHour === 0 ? 0 : Math.floor(subTotalHoursFloat * 100 / parseFloat(allocatedHour));
                let costCodePerc = {
                    costCodeFull: costCode,
                    stHour: subTotalHours,
                    totalHour: subTotalHours,
                    percentage: percentage,
                    costCodeTagAssociationDtos: tags,
                    displayOrder: col,
                };
                costCodePercs.push(costCodePerc);
            });
            let exceptionInput = $(this).find('td input[data-type="exception"]').first();
            let allocationException = {
                id: exceptionInput.data('id'),
                exception: exceptionInput.val()
            };
            let cur_index = parseInt($(this).data('index'));
            let allocationTime = allocationTimeMap[cur_index];
            allocationTime.allocatedHour = allocatedHour;
            allocationTime.costCodePercs = costCodePercs;
            allocationTime.allocationException = allocationException;
            allocationTime.hasPerDiem = hasPerDiem;
            if (hasPerDiem) {
                let perDiemSpan = $(this).find('span.per-diem-span');
                let perDiemCostCode = ('' + perDiemSpan.text()).trim();
                allocationTime.perDiemCostCode = perDiemCostCode;
                let tagLi = $(this).find('td.td-per-diem ol[data-name="ol-tag"] li');
                let tags = [];
                tagLi.each(function () {
                    let tag = $(this).find('span[data-name="tag"]').text().trim();
                    let typeDesc = $(this).find('span[data-name="type-desc"]').text().trim();
                    let code = $(this).find('span[data-name="code"]').text().trim();
                    let codeDesc = $(this).find('span[data-name="code-desc"]').text().trim();
                    let costCodeTagAssociation = {
                        tagType: tag,
                        typeDescription: typeDesc,
                        codeName: code,
                        codeDescription: codeDesc,
                    };
                    tags.push(costCodeTagAssociation);
                });
                allocationTime.perDiemCostCodeTagAssociations = tags;
            } else {
                allocationTime.perDiemCostCode = null;
            }

            let perDiemAmount = perDiemInput.val();
            allocationTime.perDiemAmount = perDiemAmount ? perDiemAmount : 0;
            let rigPaySpan = $(this).find('td.td-rig-pay span[name="rigPayAmount"]');
            if (rigPaySpan.hasClass('visible')) {
                allocationTime.rigPay = allocationTime.craftRigPay ?? 0;
            }
            let inputHasMob = $(this).find('td input[name="inputHasMob"]');
            let hasMob = $(inputHasMob).is(':checked');
            if (hasMob) {
                let mobSpan = $(this).find('span.mob-span');
                allocationTime.mobCostCode = ('' + mobSpan.text()).trim();
                let tagLi = $(this).find('td.td-mob ol[data-name="ol-tag"] li');
                let tags = [];
                tagLi.each(function () {
                    let tag = $(this).find('span[data-name="tag"]').text().trim();
                    let typeDesc = $(this).find('span[data-name="type-desc"]').text().trim();
                    let code = $(this).find('span[data-name="code"]').text().trim();
                    let codeDesc = $(this).find('span[data-name="code-desc"]').text().trim();
                    let costCodeTagAssociation = {
                        tagType: tag,
                        typeDescription: typeDesc,
                        codeName: code,
                        codeDescription: codeDesc,
                    };
                    tags.push(costCodeTagAssociation);
                });
                allocationTime.mobCostCodeTagAssociations = tags;
                allocationTime.hasMob = true;
                let mobAmount = $(this).find('td.td-mob span[data-name="mobAmount"]').text().trim();
                allocationTime.mobAmount = parseFloat(mobAmount);
                let mobMileage = $(this).find('td.td-mob input[data-name="mobMileage"]').val();
                allocationTime.mobMileage = mobMileage ? mobMileage : 0;
            } else {
                allocationTime.mobCostCodeTagAssociations = null;
                allocationTime.mobCostCode = null;
                allocationTime.mobAmount = 0;
                allocationTime.mobMileage = 0;
                allocationTime.hasMob = false;
            }
            allocationTime.mobMileageRate = mobMileageRate;
        });
        let billingPurposeOnly = $('#billing-purpose-only').is(':checked');
        let allocationSubmissionDto = {
            id: submissionId,
            status: status,
            previousStatus: previousStatus,
            dateOfService: dateOfService,
            payrollDate: payrollDate,
            submitUserId: submitUserId,
            projectId: projectId,
            approveUserId: approveUserId,
            reportSubmissionId: reportSubmissionId,
            teamName: teamName,
            allocationTimes: Object.keys(allocationTimeMap).map((key) => allocationTimeMap[key]),
            billingPurposeOnly: billingPurposeOnly,
        };
        submitModule.saveTimeAllocation(allocationSubmissionDto, url, successPage);
    }

    function filterCrew(input) {
        let crew = $(input).val();
        let checked = $(input).is(':checked');
        let tr = $("td[data-crew='" + crew + "']").closest('tr');
        if (checked) {
            $(tr).show();
            $(tr).removeClass("hidden").addClass("visible")
        } else {
            $(tr).hide();
            $(tr).removeClass("visible").addClass("hidden")
        }
    }

    function filterAllocation(input) {
        let val = $(input).val();
        let checked = $(input).is(':checked');
        let tr = undefined;
        if ("over" === val) {
            tr = $('tr:not(.tr-head)').filter(function () {
                return parseFloat($(this).data('unallocated-time')) < 0;
            });
        } else if ("under" === val) {
            tr = $('tr:not(.tr-head)').filter(function () {
                return parseFloat($(this).data('unallocated-time')) > 0;
            });
        } else {
            tr = $('tr:not(.tr-head)').filter(function () {
                return parseFloat($(this).data('unallocated-time')) === 0.0;
            });
        }
        if (checked) {
            $(tr).show();
            $(tr).removeClass("hidden").addClass("visible")
        } else {
            $(tr).hide();
            $(tr).removeClass("visible").addClass("hidden")
        }
    }

    $.validator.addMethod("requiredIfAbnormal", function (value, element, params) {
        let tr = $(element).closest('tr');
        let val = $(element).val();
        return val || parseFloat($(tr).data('unallocated-time')) === 0.0;
    }, function (params, element) {
        let tr = $(element).closest('tr');
        return parseFloat($(tr).data('unallocated-time')) > 0.0 ?
            'Please provide the reason for under allocating' : "Please provide the reason for over allocating";
    });

    $.validator.addMethod("requiredIfInvalid", function (value, element, params) {
        let tr = $(element).closest('tr');
        let val = $(element).val();
        // let isActive = tr.data('is-active');
        let isInvalid = tr.data('invalid-reason') == null;
        return val || isInvalid || parseFloat($(tr).data('allocated-time')) === 0.0;
    }, function (params, element) {
        let tr = $(element).closest('tr');
        let invalidReason = tr.data('invalid-reason');
        return `${invalidReason}<br/> Please provide a reason or ask the project manager to update the roster`;
    });

    $.validator.addMethod("overwritePerDiem", function (value, element, params) {
        let tr = $(element).closest('tr');
        let perDiemMode = tr.data('per-diem-mode');
        let dailyThreshold = parseInt(tr.data('per-diem-daily-threshold'))
        let isManual = perDiemMode === 'MANUAL';
        let val = $(element).val();
        if (isManual || val) return true;
        let checked = tr.find('input[name="manualPerDiem"]').is(':checked');
        let perDiemThreshold = dailyThreshold;
        let allocatedTime = parseFloat($(tr).data('allocated-time'));
        let allowPerDiem = tr.data('allow-per-diem');
        let qualified = allowPerDiem && (allocatedTime >= perDiemThreshold);
        return checked === qualified;
    }, "Please provide the reason for overwriting the per diem");

    $.validator.addMethod("requiredIfMob", function (value, element, params) {
        let td = $(element).closest('td');
        let hasMob = td.find('input[name="inputHasMob"]').is(':checked');
        let mobCostCode = td.find('span.mob-span').text().trim();
        return !hasMob || (!!mobCostCode);
    }, "Cost code is required");

    $.validator.classRuleSettings.requiredIfInvalid = {
        requiredIfInvalid: true
    };

    $.validator.classRuleSettings.requiredIfAbnormal = {
        requiredIfAbnormal: true
    };

    $.validator.classRuleSettings.requiredIfMob = {
        requiredIfMob: true
    };

    $.validator.classRuleSettings.overwritePerDiem = {
        overwritePerDiem: true
    };

    function filterException(input) {
        let isNotEmpty = "true" === $(input).val();
        let checked = $(input).is(':checked');
        let td = $("table.grid-view").first().find('tr:not(.tr-head) td.td-exception');
        let tr;
        if (isNotEmpty) {
            tr = td.find('input[data-type="exception"]:text').filter(function () {
                let val = $(this).val();
                return val.length !== 0 && val.trim();
            }).closest('tr');
        } else {
            tr = td.find('input[data-type="exception"]:text').filter(function () {
                let val = $(this).val();
                return val.length === 0 || !val.trim();
            }).closest('tr');
        }
        if (checked) {
            $(tr).show();
            $(tr).removeClass("hidden").addClass("visible")
        } else {
            $(tr).hide();
            $(tr).removeClass("visible").addClass("hidden")
        }
    }

    return {
        init: init,
        // switchView: switchView,
        saveTimeDistribution: saveTimeDistribution,
        filterCrew: filterCrew,
        filterAllocation: filterAllocation,
        filterException: filterException,
    }
}();

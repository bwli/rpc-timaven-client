let weeklyProcess = function () {

    function init() {
        let firstHeight = $('thead tr:nth-child(1)').height();
        $('thead tr:nth-child(2) th').css("top", firstHeight);

        $('#datepicker-dos').datepicker({
            uiLibrary: 'bootstrap4',
            iconsLibrary: 'fontawesome',
        });
        $('select[name="costCode"]').select2({
            templateResult: function (data, container) {
                if (data.element) {
                    $(container).addClass($(data.element).attr("class"));
                }
                return data.text;
            }
        });
        // TM-314 Start, using multiselect for empId
        $('select#select-emp-id').multiselect({
            inheritClass: true,
            maxHeight: 200,
            buttonTextAlignment: 'none',
            buttonClass: ['custom-select', 'text-start'],
            nonSelectedText: 'Select'
        });
        $('#costCodeModal span.multiselect-native-select').css('display', 'block');
        $('#costCodeModal div.btn-group').css('display', 'block');
        $('#costCodeModal div.multiselect-container.dropdown-menu').addClass('form-control');
        // TM-314 End

        $('td input[data-type="pd"]').on("change", function () {
            selectPerDiem($(this));
        });

        $('td input[data-type="mob"]').on("change", function () {
            selectMob($(this));
        });

        $('input[data-type="st"],input[data-type="ot"],input[data-type="dt"]').on("change", function () {
            let empId = $(this).data("emp-id");
            let type = $(this).data("type");
            let laborTable = $('#weekly-labor-table'); let totalInput = laborTable.find("input[data-type='total-" + type + "'][data-emp-id='" + empId + "']");
            let groupInput = laborTable.find("input[data-type='" + type + "'][data-emp-id='" + empId + "']");
            let sum = 0;
            $(groupInput).each(function (){
                if ($(this).val()) {
                    sum += Number($(this).val());
                    sum = parseFloat(sum.toFixed(2));
                }
            });
            totalInput.val(sum);
            let date = $(this).data("date");

            let perDiemTable = $('#weekly-per-diem-table');
            let perDiemInput = perDiemTable.find("input[data-type='pd'][data-emp-id='" + empId + "'][data-date='" + date + "']");
            let perDiemMode = perDiemInput.data('per-diem-mode');
            let perDiemThreshold = parseInt(perDiemInput.data('per-diem-threshold'));
            let perDiemCostCode = perDiemInput.data('per-diem-cost-code');

            let isManual = perDiemMode === 'MANUAL';

            // Decide whether to display per diem number
            let cur_index = parseInt(laborTable.find("input[data-type='employee'][data-emp-id='" + empId + "']").data("index"));
            let empCostCodeDateDto = weeklyProcessDto.empCostCodeDateDtos[cur_index];
            if (!isManual && empCostCodeDateDto.hasPerDiem) {
                let stInput = laborTable.find("input[data-date='" + date + "'][data-emp-id='" + empId + "'][data-type='st']");
                let otInput = laborTable.find("input[data-date='" + date + "'][data-emp-id='" + empId + "'][data-type='ot']");
                let dtInput = laborTable.find("input[data-date='" + date + "'][data-emp-id='" + empId + "'][data-type='dt']");

                let dateSum = 0;
                $(stInput).each(function (){
                    if ($(this).val()) {
                        dateSum += Number($(this).val());
                        dateSum = parseFloat(dateSum.toFixed(2));
                    }
                });
                $(otInput).each(function (){
                    if ($(this).val()) {
                        dateSum += Number($(this).val());
                        dateSum = parseFloat(dateSum.toFixed(2));
                    }
                });
                $(dtInput).each(function (){
                    if ($(this).val()) {
                        dateSum += Number($(this).val());
                        dateSum = parseFloat(dateSum.toFixed(2));
                    }
                });

                if (dateSum >= perDiemThreshold) {
                    $(perDiemInput).prop('checked', true).change();
                } else {
                    $(perDiemInput).prop('checked', false).change();
                }
            }


            let hasDedicatedPdCostCode = false;
            if (!!perDiemCostCode) {
                hasDedicatedPdCostCode = perDiemCostCode.trim() !== '';
            }
            if (!hasDedicatedPdCostCode) {
                let maximumCode = '';
                let maximumTotal = 0;
                let date = $(this).data("date");
                empCostCodeDateDto.costCodes.forEach(function (e) {
                    let ccRow = laborTable.find('tr[name="' + empId + '"] td[data-cost-code="' + e + '"]').closest('tr');
                    let stInput = ccRow.find("input[data-date='" + date + "'][data-emp-id='" + empId + "'][data-type='st']");
                    if (stInput.length > 0) {
                        let otInput = ccRow.find("input[data-date='" + date + "'][data-emp-id='" + empId + "'][data-type='ot']");
                        let dtInput = ccRow.find("input[data-date='" + date + "'][data-emp-id='" + empId + "'][data-type='dt']");
                        let dateSum = Number($(stInput).val()) + Number($(otInput).val()) + Number($(dtInput).val());
                        if (dateSum > maximumTotal) {
                            maximumTotal = dateSum;
                            maximumCode = e;
                        }
                    }
                });
            }
        });
        if (typeof weeklyProcessDto && weeklyProcessDto.hasOwnProperty("empCostCodeDateDtos")) {
            weeklyProcessDto.empCostCodeDateDtos.forEach(function (e) {
                updatePerDiemTotal(e.empId, e.team);
            });
        }

        $("#btn-ok").on("click", function () {
            let costCodeType = $('#sl-cost-code-type').val();
            let costCode= $("#costCodeModal").find("select[name='costCode']").first().val();
            let dateOfService = $('#datepicker-dos').val();
            let team = $('#select-team').val();

            // TM-314 multiple empId
            let empIds = [];
            $('select#select-emp-id').data('multiselect').getSelected()
                .each((idx, option) => empIds.push($(option).val()));

            if (!team && empIds.length < 1) { // TM-314 multiple empId
                alert("Please select either team or employee or both.");
                return;
            }

            if (costCode && dateOfService) {
                let splitDate = dateOfService.split(/\//);
                let isoDate = [splitDate[2],splitDate[0],splitDate[1]].join('-');

                if (days.includes(isoDate)) {
                    if (team === null) team = '';
                    // TM-314 multiple empId
                    let empIdParamStr = '';
                    empIds.forEach(id => empIdParamStr+=('&empId='+encodeURIComponent(id)));

                    // noinspection JSUnresolvedFunction
                    $.ajax({
                        type: "post",
                        url: `weekly-process-table?costCodeType=${costCodeType}&dateOfService=${dateOfService}&team=${team}${empIdParamStr}&costCode=${costCode}`,
                        data: JSON.stringify(weeklyProcessDto),
                        contentType: "application/json;charset=utf-8",
                        traditional: true,
                        success: function (data) {
                            /*<![CDATA[*/
                            $('#div-table').html(data);
                            weeklyProcess.init();
                            /*]]>*/
                            if (costCodeType === '0') {
                                $('#div-table button#pills-labor-tab').tab('show');
                            } else if (costCodeType === '1') {
                                $('#div-table button#pills-perdiem-tab').tab('show');
                            } else if (costCodeType === '2') {
                                $('#div-table button#pills-mob-tab').tab('show');
                            }
                            addDefaultTableEvents();
                            addSelectableTableEvents(); // TM-314
                        },
                        error: function (xhr, textStatus, errorThrown) {
                            if (401 === xhr.status || 403 === xhr.status) {
                                $("html").html(xhr.responseText);
                            }
                        }
                    });
                } else {
                    alert("Selected date is invalid. It must be already existed in the list.");
                }
            } else {
                alert("All fields are required");
            }
        });

        addRemoveTagEvent();

        $('#addTagModal').on("show.bs.modal", function (e) {
            // set the username
            let th = $(e.relatedTarget).closest("th");
            let okBtn = $('#btn-add-tag-ok');
            okBtn.data('date', th.data('date'));
            okBtn.data('tag-for', th.data('tag-for'));
        });

        $('select[name="select-tag"]').change(function () {
            let typeId = $(this).val();
            $.ajax({
                type: "POST",
                url: `cost-code-tags?typeId=${typeId}`,
                success: function (data) {
                    let select = $("select[name='select-code']");
                    let selectOptionsStr = '';
                    if (data.length !== 0) {
                        select.find("option").remove();
                        $.each(data, function (index, value) {
                            selectOptionsStr += `<option data-id="${value['id']}" data-code="${value['codeName']}" data-desc="${!!value['description'] ? value['description'] : ''}" value="${value['id']}">${value['codeName']}${!!value['description'] ? ' | ' + value['description'] : ''}</option>`;
                        });
                        select.append(selectOptionsStr);
                    } else {
                        select.find('option').remove();
                        select.append('<option selected disabled hidden>Select</option>');
                    }
                },
            });
        });
        $("#btn-add-tag-ok").unbind().on("click", function () {
            let modal = $("#addTagModal");
            let empId = $('#tag-emp-id').val();
            let typeOption = modal.find("select[name='select-tag']").find(":selected");
            let typeText = ('' + typeOption.text()).trim();
            let codeOption = modal.find("select[name='select-code']").find(":selected");
            let typeDesc = typeOption.data('desc');
            typeDesc = !!typeDesc ? typeDesc : '';
            let code = ('' + codeOption.data('code')).trim();
            let day = $(this).data('date');
            let tagFor = $(this).data('tag-for');
            if (code) {
                let desc = codeOption.data('desc');
                desc = !!desc ? desc : '';
                $.ajax({
                    type: "post",
                    url: `weekly-process-tag?tagFor=${tagFor}&dateOfService=${day}&empId=${empId}&tagType=${typeText}&code=${code}&tagDesc=${typeDesc}&codeDesc=${desc}`,
                    data: JSON.stringify(weeklyProcessDto),
                    contentType: "application/json;charset=utf-8",
                    traditional: true,
                    success: function (data) {
                        /*<![CDATA[*/
                        $('#div-table').html(data);
                        weeklyProcess.init();
                        /*]]>*/
                        if (tagFor === 2) {
                            $('#div-table button#pills-mob-tab').tab('show');
                        } else {
                            $('#div-table button#pills-perdiem-tab').tab('show');
                        }
                        addDefaultTableEvents();
                    },
                    error: function (xhr, textStatus, errorThrown) {
                        if (401 === xhr.status || 403 === xhr.status) {
                            $("html").html(xhr.responseText);
                        }
                    }
                });
            }
        });
        updateZeroElementStyle();
        $('#weekly-labor-table').find('input[data-type="st"],input[data-type="ot"],input[data-type="dt"]')
            .on('focusin', function() {
                $(this).data('val', $(this).val());
        }).change(function (){
            let prev = parseFloat($(this).data('val'));
            let current = parseFloat($(this).val());
            if (prev !== 0.0 && current === 0.0) {
                $(this).addClass('zero');
                $(this).closest('td').addClass('table-secondary');
            } else if (prev === 0.0 && current !== 0.0) {
                $(this).removeClass('zero');
                $(this).closest('td').removeClass('table-secondary');
            }
        });
        updateColumnTotal();
        $("#btn-apply-rule").on("click", function () {
            $.ajax({
                type: "post",
                url: `weekly-process-apply-rule?weekEndDate=${weekEndDate}`,
                traditional: true,
                success: function (data) {
                    window.location.reload();
                },
                error: function (xhr, textStatus, errorThrown) {
                    if (401 === xhr.status || 403 === xhr.status) {
                        $("html").html(xhr.responseText);
                    }
                }
            });
        });
        $('input[data-name=perDiemAmount]').on("change", function () {
            updatePerDiemTotal($(this).data("emp-id"), $(this).data('team'));
        });
        $('input[data-name=mobMileage]').on("change", function () {
            let mobMileage = parseFloat($(this).val());
            let mobAmountInput = $(this).closest('td').find('input[data-name="mobAmount"]');
            let mobMileageRate = parseFloat(mobAmountInput.data('mob-mileage-rate'));
            if (isNaN(mobMileageRate)) {
                mobMileageRate = 0.0;
            } else {
                mobMileageRate = parseFloat(mobMileageRate.toFixed(2));
            }
            mobAmountInput.val(parseFloat((mobMileage * mobMileageRate).toFixed(2)));
        });

        fixateLeftPanel($('#weekly-labor-table'), 4);
        fixateLeftPanel($('#weekly-per-diem-table'), 4);
        fixateLeftPanel($('#weekly-mob-table'), 4);
        fixateLeftPanel($('#weekly-absentee-table'), 3);
        $(window).resize(function() {
            fixateLeftPanel($('#weekly-labor-table'), 4);
            fixateLeftPanel($('#weekly-per-diem-table'), 4);
            fixateLeftPanel($('#weekly-mob-table'), 4);
            fixateLeftPanel($('#weekly-absentee-table'), 3);
        });

    }


    function fixateLeftPanel(table, nColumn) {

        $(function() {
            // TM-297  (start) fixed table columns
            // thead fixed

            table.find(`th:nth-child(-n + ${nColumn}), td:nth-child(-n + ${nColumn})`)
                .css("position", "sticky")
                .css("background-color", "white");

            // TM-314
            for (let i = 0, zIndex = 15+nColumn; i < nColumn; i++,zIndex--) {
                table.find(`th:nth-child(${i+1})`).css("z-index", zIndex);
            }
            table.find(`td:nth-child(-n + ${nColumn})`)
                .css("z-index", 13)

            let left = 0;
            for (let i = 1; i <= nColumn; i++) {
                let thTd = table.find(`th:nth-child(${i}), td:nth-child(${i})`);
                thTd.css("left", left);
                left += thTd.first().outerWidth();
            }
        });
    }

    function updateZeroElementStyle() {
        let inputs = $('#weekly-labor-table').find('input[data-type="st"],input[data-type="ot"],input[data-type="dt"]');
        let zeroInputs = inputs.filter(function () {
            return parseFloat(this.value) === 0.0;
        });
        let nonZeroInputs = inputs.filter(function () {
            return parseFloat(this.value) !== 0.0;
        });
        zeroInputs.addClass('zero');
        zeroInputs.closest('td').addClass('table-secondary');
        nonZeroInputs.removeClass('zero');
        nonZeroInputs.closest('td').removeClass('table-secondary');
    }

    function selectPerDiem(input) {
        let perDiemTable = $('#weekly-per-diem-table');
        let laborTable = $('#weekly-labor-table');
        let empId = $(input).data('emp-id');
        let date = $(input).data('date');
        let checked = $(input).is(':checked');
        let laborPerDiemInput = laborTable.find('input[data-emp-id="' + empId + '"][data-date="' + date + '"][data-type="pd"]');
        let inputGroup = perDiemTable.find('input[data-emp-id="' + empId + '"][data-date="' + date + '"][data-type="pd"]');
        if (checked) {
            $(input).closest('td').find('.tag-group').removeClass('hidden').addClass('show');
        } else {
            $(input).closest('td').find('.tag-group').removeClass('show').addClass('hidden');
        }
        if ($(inputGroup).filter(':checked').length === 0) {
            laborPerDiemInput.prop('checked', false);
        } else {
            laborPerDiemInput.prop('checked', true);
        }
        updatePerDiemTotal($(input).data("emp-id"), $(input).data("team"));
    }

    function selectMob(input) {
        let mobTable = $('#weekly-mob-table');
        let empId = $(input).data('emp-id');
        let date = $(input).data('date');
        let checked = $(input).is(':checked');
        let inputGroup = mobTable.find('input[data-emp-id="' + empId + '"][data-date="' + date + '"][data-type="mob"]');
        if (checked) {
            $(input).closest('td').find('.tag-group').removeClass('hidden').addClass('show');
            $(inputGroup).not(input).closest('td').find('.tag-group').removeClass('show').addClass('hidden');
        } else {
            $(input).closest('td').find('.tag-group').removeClass('show').addClass('hidden');
        }
        $(inputGroup).not(input).prop('checked', false);
    }

    function updatePerDiemTotal(empId, team) {
        let perDiemTable = $('#weekly-per-diem-table');
        let totalPd = perDiemTable.find(`input[data-type='total-pd'][data-emp-id='${empId}'][data-team='${team}']`);
        let perDiemInputs = perDiemTable.find(`input[data-type='pd'][data-emp-id='${empId}'][data-team='${team}']:checked`);
        if (perDiemInputs.length === 0) {
            totalPd.val(0);
            return;
        }
        // let perDiemMode = totalPd.data('per-diem-mode');
        // let perDiemWeeklyThreshold = parseInt(totalPd.data('per-diem-weekly-threshold'));
        // let perDiemSize = perDiemInputs.length;
        let perDiemAmountInput = $(perDiemInputs).closest('td').find('input[data-name=perDiemAmount]');
        let total = 0;
        $(perDiemAmountInput).each(function (){
            if ($(this).val()) {
                total += Number($(this).val());
            }
        });
        // let perDiemUnitAmount = parseInt(perDiemAmountInput.val().replace(/^\$/i, ''));
        // switch (perDiemMode) {
        //     case "'MANUAL'":
        //     case "PAY_AS_YOU_GO":
        //         total = perDiemUnitAmount * perDiemSize;
        //         break;
        //     case "N_PLUS_ONE":
        //         if (perDiemSize >= perDiemWeeklyThreshold) {
        //             total = perDiemUnitAmount * (perDiemSize + 1);
        //         } else {
        //             total = perDiemUnitAmount * perDiemSize;
        //         }
        //         break;
        //     case "SEVEN_DAYS":
        //         if (perDiemSize >= perDiemWeeklyThreshold) {
        //             total = perDiemUnitAmount * 7;
        //         } else {
        //             total = perDiemUnitAmount * perDiemSize;
        //         }
        //         break;
        //     default:
        //         break;
        // }
        totalPd.val(total);
    }

    function addRemoveTagEvent() {
        $('span[data-name="remove-tag"]').unbind().on('click', function () {
            let td = $(this).closest('td');
            let li = $(this).closest('li');
            let tagType = li.find('span[data-name="tag"]').text().trim();
            let codeName = li.find('span[data-name="code"]').text().trim();
            let empId = td.data('emp-id');
            let day = td.data('date');
            let tagFor = td.data('tag-for');
            $.ajax({
                type: "delete",
                url: `weekly-process-tag?tagFor=${tagFor}&dateOfService=${day}&empId=${empId}&tagType=${tagType}&code=${codeName}`,
                data: JSON.stringify(weeklyProcessDto),
                contentType: "application/json;charset=utf-8",
                traditional: true,
                success: function (data) {
                    /*<![CDATA[*/
                    $('#div-table').html(data);
                    weeklyProcess.init();
                    /*]]>*/
                    if (tagFor === 2) {
                        $('#div-table button#pills-mob-tab').tab('show');
                    } else {
                        $('#div-table button#pills-perdiem-tab').tab('show');
                    }
                    addDefaultTableEvents();
                },
                error: function (xhr, textStatus, errorThrown) {
                    if (401 === xhr.status || 403 === xhr.status) {
                        $("html").html(xhr.responseText);
                    }
                }
            });
        });
    }

    function updateColumnTotal() {
        let table = $('#weekly-labor-table')
        // each day
        $.each(weeklyProcessDto.days, function (index, value) {
            let sum = 0;
            let inputs = table.find(`tbody input[type="number"][data-date="${value}"]`);
            $(inputs).each(function (){
                if ($(this).val()) {
                    sum += Number($(this).val());
                    sum = parseFloat(sum.toFixed(2));
                }
            });
            sum = parseFloat(sum.toFixed(2));
            let span = table.find(`thead th span[data-name="allocated-hour"][data-date="${value}"]`);
            span.text(sum);

            // TM-292 Add total number for ST
            let stSum = 0;
            let stInputs = table.find(`tbody input[type="number"][data-date="${value}"][data-type="st"]`);
            $(stInputs).each(function (){
                if ($(this).val()) {
                    stSum += Number($(this).val());
                    stSum = parseFloat(stSum.toFixed(2));
                }
            });
            stSum = parseFloat(stSum.toFixed(2));
            let st = table.find(`thead th span[data-date-st=${value}]`);
            st.text(stSum)

            // TM-292 Add total number for OT
            let otSum = 0;
            let otInputs = table.find(`tbody input[type="number"][data-date="${value}"][data-type="ot"]`);
            $(otInputs).each(function (){
                if ($(this).val()) {
                    otSum += Number($(this).val());
                    otSum = parseFloat(otSum.toFixed(2));
                }
            });
            otSum = parseFloat(otSum.toFixed(2));
            let ot = table.find(`thead th span[data-date-ot=${value}]`);
            ot.text(otSum)
        });

        let sum = 0;
        let inputs = table.find(`tbody input[data-type="total-st"],input[data-type="total-ot"],input[data-type="total-dt"]`);
        $(inputs).each(function (){
            if ($(this).val()) {
                sum += Number($(this).val());
                sum = parseFloat(sum.toFixed(2));
            }
        });
        sum = parseFloat(sum.toFixed(2));
        let span = table.find(`thead th span[data-name="allocated-total-hour"]`);
        span.text(sum);

        // TM-292 Add total number for ST
        let totalStSum = 0;
        let stInputs = table.find(`tbody input[type="number"][data-type="total-st"]`);
        $(stInputs).each(function (){
            if ($(this).val()) {
                totalStSum += Number($(this).val());
                totalStSum = parseFloat(totalStSum.toFixed(2));
            }
        });
        totalStSum = parseFloat(totalStSum.toFixed(2));
        let st = table.find(`thead th span[data-date-st="total"]`);
        st.text(totalStSum)

        // TM-292 Add total number for OT
        let totalOtSum = 0;
        let otInputs = table.find(`tbody input[type="number"][data-type="total-ot"]`);
        $(otInputs).each(function (){
            if ($(this).val()) {
                totalOtSum += Number($(this).val());
                totalOtSum = parseFloat(totalOtSum.toFixed(2));
            }
        });
        totalOtSum = parseFloat(totalOtSum.toFixed(2));
        let ot = table.find(`thead th span[data-date-ot="total"]`);
        ot.text(totalOtSum)
    }

    return {
        init: init,
        selectPerDiem: selectPerDiem,
        selectMob: selectMob,
    }
}();

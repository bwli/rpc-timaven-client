package com.timaven.timekeeping.api.provider;

import com.timaven.timekeeping.api.ApiBinding;
import com.timaven.timekeeping.model.CostCodeTagType;
import com.timaven.timekeeping.model.Employee;
import com.timaven.timekeeping.model.EmployeeFilter;
import com.timaven.timekeeping.model.Shift;
import com.timaven.timekeeping.model.dto.EmployeeIdNameDto;
import com.timaven.timekeeping.model.enums.WeeklyProcessStatus;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.web.reactive.function.client.ClientRequest;
import org.springframework.web.reactive.function.client.ExchangeStrategies;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import java.time.LocalDate;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ForkJoinPool;

@Getter
public class WebClientAPI extends ApiBinding {

    private final WebClient webClient;

    @Autowired
    private OAuth2RestTemplate oAuth2RestTemplate;

    private final String version = "/v1";

    public WebClientAPI(String providerApiUrl) {
        this.webClient = WebClient.builder()
                .baseUrl(providerApiUrl)
                .filter(((clientRequest, exchangeFunction) -> {
                    ClientRequest clientRequest1 = ClientRequest.from(clientRequest)
                            .header("Authorization", "Bearer " + oAuth2RestTemplate.getAccessToken().getValue())
                            .build();
                    return exchangeFunction.exchange(clientRequest1);
                }))
                .exchangeStrategies(
                        ExchangeStrategies.builder()
                                .codecs(clientCodecConfigurer ->
                                        clientCodecConfigurer.defaultCodecs().maxInMemorySize(16 * 1024 * 1024))
                                .build())
                .build();
    }

    public WebClientAPI(WebClient webClient) {
        this.webClient = webClient;
    }

    public Mono<Set<String>> getSignInSheets(EmployeeFilter filter) {
        return webClient.post()
                .uri("", uriBuilder -> uriBuilder.path(version).path("/sign-in-sheets").build())
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue(filter)
                .retrieve()
                .bodyToMono(new ParameterizedTypeReference<>() {
                });
    }

    public Mono<Set<String>> getTeamNames(EmployeeFilter filter) {
        return webClient.post()
                .uri("", uriBuilder -> uriBuilder.path(version).path("/team-names").build())
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue(filter)
                .retrieve()
                .bodyToMono(new ParameterizedTypeReference<>() {
                });
    }

    public Mono<Set<EmployeeIdNameDto>> getEmployeeIdNames(EmployeeFilter filter) {
        return webClient.post()
                .uri("", uriBuilder -> uriBuilder.path(version).path("/employees/id/name").build())
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue(filter)
                .retrieve()
                .bodyToMono(new ParameterizedTypeReference<>() {
                });
    }

    public void deleteAllocationTimeById(Long id) {
        webClient.method(HttpMethod.DELETE)
                .uri("", uriBuilder -> uriBuilder
                        .path(version)
                        .path("/allocation-times/{id}")
                        .build(id))
                .retrieve()
                .bodyToMono(Void.class)
                .block();
    }

    public void deleteStageTransactionsByProjectIdAndEmpIdAndTeamAndDateOfService(Long projectId, String empId, String team, LocalDate dateOfService) {
        webClient.method(HttpMethod.DELETE)
                .uri("", uriBuilder -> uriBuilder
                        .path(version)
                        .path("/stage-transactions")
                        .queryParam("projectId", projectId)
                        .queryParam("empId", empId)
                        .queryParam("teamName", team)
                        .queryParam("dateOfService", dateOfService)
                        .build())
                .retrieve()
                .bodyToMono(Void.class)
                .block();
    }

    public Mono<List<Employee>> saveEmployees(Collection<Employee> employees) {
        return webClient.post()
                .uri("", uriBuilder -> uriBuilder
                        .path(version)
                        .path("/employee-collection")
                        .build())
                .bodyValue(employees)
                .retrieve()
                .bodyToMono(new ParameterizedTypeReference<>() {
                });
    }


    public Mono<List<CostCodeTagType>> findAllCostCodeTagTypes() {
        return webClient.get()
                .uri("", uriBuilder -> uriBuilder
                        .path(version)
                        .path("/cost-code-tag-types")
                        .build())
                .retrieve()
                .bodyToMono(new ParameterizedTypeReference<>() {
                });
    }

    public Mono<Shift> findShiftByProjectIdAndName(Long projectId, String name) {
        return webClient.get()
                .uri("", uriBuilder -> uriBuilder
                        .path(version)
                        .path("/shifts")
                        .queryParam("projectId", projectId)
                        .queryParam("name", name)
                        .build())
                .retrieve()
                .bodyToMono(Shift.class);
    }

    public void updateWeeklyProcesses(Collection<Long> ids, WeeklyProcessStatus status) {
        webClient.put()
                .uri("", uriBuilder -> uriBuilder.path(version)
                        .path("/weekly-processes/update")
                        .queryParam("weeklyProcessStatus", status)
                        .build())
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue(ids)
                .retrieve()
                .bodyToMono(Void.class)
                .block();
    }
}

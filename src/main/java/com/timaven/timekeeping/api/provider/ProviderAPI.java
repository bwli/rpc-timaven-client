package com.timaven.timekeeping.api.provider;

import com.timaven.timekeeping.api.ApiBinding;
import com.timaven.timekeeping.model.*;
import com.timaven.timekeeping.model.dto.*;
import com.timaven.timekeeping.model.enums.*;
import com.timaven.timekeeping.model.paging.Page;
import com.timaven.timekeeping.model.paging.PagingRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.util.StringUtils;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

public class ProviderAPI extends ApiBinding {

    @Value("${api.url.provider}")
    private String providerApiUrl;

    private RestTemplate restTemplate;

    @Autowired
    private OAuth2RestTemplate oAuth2RestTemplate;

    private final String version = "/v1";

    public ProviderAPI() {
        this.restTemplate = new RestTemplate(new HttpComponentsClientHttpRequestFactory());
        this.restTemplate.getInterceptors().add(getBearerTokenInterceptor());
    }


    private ClientHttpRequestInterceptor getBearerTokenInterceptor() {
        return (request, bytes, execution) -> {
            request.getHeaders().add("Authorization",
                    "Bearer " + oAuth2RestTemplate.getAccessToken().getValue());
            System.out.println(oAuth2RestTemplate.getAccessToken().getValue());
            return execution.execute(request, bytes);
        };
    }

    public Set<Project> getProjectsByUsername(String username) {
        MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
        params.add("username", username);
        URI uri = UriComponentsBuilder.fromHttpUrl(providerApiUrl)
                .path(version)
                .path("/projects")
                .queryParams(params)
                .build()
                .toUri();
        Project[] result = restTemplate.getForObject(uri, Project[].class);
        if (null == result) return new HashSet<>();
        return Arrays.stream(result).collect(Collectors.toCollection(TreeSet::new));
    }

    public Page<Project> getProjectsByUsername(PagingRequest pagingRequest, String username, boolean isAll) {
        URI uri = UriComponentsBuilder.fromHttpUrl(providerApiUrl)
                .path(version)
                .path("/projects-page")
                .queryParam("username", username)
                .queryParam("isAll", isAll)
                .build()
                .toUri();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<PagingRequest> entity = new HttpEntity<>(pagingRequest, headers);
        ResponseEntity<Page<Project>> response = restTemplate
                .exchange(uri, HttpMethod.POST, entity, new ParameterizedTypeReference<>() {
                });
        return response.getBody();
    }

    public Optional<Rule> getRuleByProjectId(Long projectId) {
        URI uri = UriComponentsBuilder.fromHttpUrl(providerApiUrl)
                .path(version)
                .path("/rules")
                .queryParam("projectId", projectId)
                .build()
                .toUri();
        return Optional.ofNullable(restTemplate.getForObject(uri, Rule.class));
    }

    public List<Rule> getRules() {
        URI uri = UriComponentsBuilder.fromHttpUrl(providerApiUrl)
                .path(version)
                .path("/rules")
                .build()
                .toUri();
        Rule[] rules = restTemplate.getForObject(uri, Rule[].class);
        if (rules == null) return new ArrayList<>();
        return Arrays.stream(rules).collect(Collectors.toList());
    }

    public Rule saveRule(Rule rule) {
        URI uri = UriComponentsBuilder.fromHttpUrl(providerApiUrl)
                .path(version)
                .path("/rules")
                .build()
                .toUri();
        ResponseEntity<Rule> response;
        if (rule.getId() == null) {
            response = restTemplate.postForEntity(uri, rule, Rule.class);
        } else {
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            HttpEntity<Rule> entity = new HttpEntity<>(rule, headers);
            response = restTemplate.exchange(uri, HttpMethod.PUT, entity, Rule.class);
        }
        return response.getBody();
    }

    public Set<AllocationSubmission> getAllocationSubmissions(LocalDate startDate,
                                                              LocalDate endDate,
                                                              Long projectId,
                                                              String teamName,
                                                              AllocationSubmissionStatus status,
                                                              AllocationSubmissionStatus notStatus,
                                                              List<String> includes) {
        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(providerApiUrl)
                .path(version)
                .path("/allocation-submissions")
                .queryParam("startDate", startDate)
                .queryParam("endDate", endDate)
                .queryParam("projectId", projectId)
                .queryParam("status", status)
                .queryParam("notStatus", notStatus)
                .queryParam("includes", includes);
        if (StringUtils.hasText(teamName)) builder = builder.queryParam("teamName", teamName);
        URI uri = builder.build().toUri();
        AllocationSubmission[] result = restTemplate.getForObject(uri, AllocationSubmission[].class);
        if (result == null) return new HashSet<>();
        return Arrays.stream(result).collect(Collectors.toCollection(TreeSet::new));
    }

    public Set<AllocationSubmission> getAllocationSubmissions(LocalDate startDate,
                                                              LocalDate endDate,
                                                              Long projectId,
                                                              AllocationSubmissionStatus status,
                                                              AllocationSubmissionStatus notStatus,
                                                              List<String> includes) {
        return getAllocationSubmissions(startDate, endDate, projectId, null, status, notStatus, includes);
    }

    public List<Employee> getEmployees(EmployeeFilter filter) {
        URI uri = UriComponentsBuilder.fromHttpUrl(providerApiUrl)
                .path(version)
                .path("/employees-filtered")
                .build()
                .toUri();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<EmployeeFilter> entity = new HttpEntity<>(filter, headers);
        ResponseEntity<Employee[]> response = restTemplate
                .exchange(uri, HttpMethod.POST, entity, new ParameterizedTypeReference<>() {
                });
        Employee[] result = response.getBody();
        if (null == result) return new ArrayList<>();
        return Arrays.stream(result).sorted().collect(Collectors.toList());
    }

    public List<CostCodeLog> getCostCodesByProjectIdAndDateOfService(Long projectId, LocalDate dateOfService,
                                                                     List<String> costCodes, boolean includeGlobal) {
        return getCostCodesByProjectIdAndDateRange(projectId, dateOfService, null, costCodes, includeGlobal);
    }


    public List<CostCodeLog> getCostCodesByProjectIdAndDateRange(Long projectId, LocalDate startDate,
                                                                 LocalDate endDate, List<String> costCodes,
                                                                 boolean includeGlobal) {
        URI uri = UriComponentsBuilder.fromHttpUrl(providerApiUrl)
                .path(version)
                .path("/cost-code-log-filtered")
                .queryParam("projectId", projectId)
                .queryParam("startDate", startDate)
                .queryParam("endDate", endDate)
                .queryParam("includeGlobal", includeGlobal)
                .build()
                .toUri();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<List<String>> entity = new HttpEntity<>(costCodes, headers);
        ResponseEntity<CostCodeLog[]> response = restTemplate
                .exchange(uri, HttpMethod.POST, entity, new ParameterizedTypeReference<>() {
                });
        CostCodeLog[] result = response.getBody();
        if (result == null) return new ArrayList<>();
        return Arrays.stream(result).collect(Collectors.toList());
    }

    public List<CostCodeLog> getCostCodesByProjectIdAndDateRangeIncludeGlobal(Long projectId, LocalDate startDate,
                                                                              LocalDate endDate, List<String> costCodes) {
        return getCostCodesByProjectIdAndDateRange(projectId, startDate, endDate, costCodes, true);
    }

    public List<CostCodeLog> getCostCodesByProjectIdAndDateOfServiceIncludeGlobal(Long projectId, LocalDate dateOfService,
                                                                                  List<String> costCodes) {
        return getCostCodesByProjectIdAndDateOfService(projectId, dateOfService, costCodes, true);
    }

    public List<CostCodeBillingCode> getCostCodeBillingCodesByCostCodeIds(CostCodeBillingCodeFilter filter) {
        URI uri = UriComponentsBuilder.fromHttpUrl(providerApiUrl)
                .path(version)
                .path("/cost-code-billing-codes-filtered")
                .build()
                .toUri();
        ResponseEntity<CostCodeBillingCode[]> response = restTemplate.postForEntity(uri, filter, CostCodeBillingCode[].class);
        CostCodeBillingCode[] result = response.getBody();
        if (null == result) return new ArrayList<>();
        return Arrays.stream(result).collect(Collectors.toList());
    }

    public Set<AllocationTime> getAllocationTimes(LocalDate startDate,
                                                  LocalDate endDate,
                                                  LocalDate payrollStartDate,
                                                  LocalDate payrollEndDate,
                                                  Long projectId,
                                                  String teamName,
                                                  AllocationSubmissionStatus status,
                                                  AllocationSubmissionStatus notStatus,
                                                  List<String> includes,
                                                  Set<String> empIds) {
        AllocationTimeFilter filter = new AllocationTimeFilter(null, startDate, endDate, payrollStartDate, payrollEndDate, false, projectId, teamName, status, notStatus, includes, empIds);
        return getAllocationTimes(filter);
    }

    public Set<AllocationTime> getAllocationTimes(LocalDate startDate,
                                                  LocalDate endDate,
                                                  LocalDate payrollStartDate,
                                                  LocalDate payrollEndDate,
                                                  Long projectId,
                                                  AllocationSubmissionStatus status,
                                                  AllocationSubmissionStatus notStatus,
                                                  List<String> includes,
                                                  Set<String> empIds) {
        AllocationTimeFilter filter = new AllocationTimeFilter(null, startDate, endDate, payrollStartDate, payrollEndDate, false, projectId, null, status, notStatus, includes, empIds);
        return getAllocationTimes(filter);
    }

    public Set<AllocationTime> getAllocationTimes(AllocationTimeFilter filter) {
        URI uri = UriComponentsBuilder.fromHttpUrl(providerApiUrl)
                .path(version)
                .path("/allocation-times-filtered")
                .build()
                .toUri();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<AllocationTimeFilter> entity = new HttpEntity<>(filter, headers);
        ResponseEntity<AllocationTime[]> response = restTemplate
                .exchange(uri, HttpMethod.POST, entity, new ParameterizedTypeReference<>() {
                });
        AllocationTime[] result = response.getBody();
        if (result == null) return new HashSet<>();
        return Arrays.stream(result).collect(Collectors.toSet());
    }

    public Project getProjectById(Long id) {
        URI uri = UriComponentsBuilder.fromHttpUrl(providerApiUrl)
                .path(version)
                .path("/projects/{id}")
                .buildAndExpand(id)
                .toUri();
        return restTemplate.getForObject(uri, Project.class);
    }

    public Set<User> getUsersByProjectIdAndRoles(Long projectId, Set<String> roles) {
        URI uri = UriComponentsBuilder.fromHttpUrl(providerApiUrl)
                .path(version)
                .path("/users")
                .queryParam("projectId", projectId)
                .queryParam("roles", roles)
                .build()
                .toUri();
        User[] result = restTemplate.getForObject(uri, User[].class);
        if (null == result) return new HashSet<>();
        return Arrays.stream(result).collect(Collectors.toSet());
    }

    public Set<User> getUsersByProjectIdAndRoles(Long projectId, Set<String> roles,
                                                 Boolean includeSelf, Boolean includeNoRole) {
        URI uri = UriComponentsBuilder.fromHttpUrl(providerApiUrl)
                .path(version)
                .path("/users")
                .queryParam("projectId", projectId)
                .queryParam("roles", roles)
                .queryParam("includeSelf", includeSelf)
                .queryParam("includeNoRoe", includeNoRole)
                .build()
                .toUri();
        User[] result = restTemplate.getForObject(uri, User[].class);
        if (null == result) return new HashSet<>();
        return Arrays.stream(result).collect(Collectors.toSet());
    }

    public Role getRoleByName(String roleName) {
        URI uri = UriComponentsBuilder.fromHttpUrl(providerApiUrl)
                .path(version)
                .path("/roles")
                .queryParam("roleName", roleName)
                .build()
                .toUri();
        return restTemplate.getForObject(uri, Role.class);
    }

    public User getUserById(Long id) {
        URI uri = UriComponentsBuilder.fromHttpUrl(providerApiUrl)
                .path(version)
                .path("/users/{id}")
                .buildAndExpand(id)
                .toUri();
        return restTemplate.getForObject(uri, User.class);
    }

    public String changeUserValidity(Long id, boolean active) {
        URI uri = UriComponentsBuilder.fromHttpUrl(providerApiUrl)
                .path(version)
                .path("/users/{id}/active")
                .queryParam("active", active)
                .buildAndExpand(id)
                .toUri();
        ResponseEntity<String> response;
        response = restTemplate.exchange(uri, HttpMethod.PUT, null, String.class);
        return response.getBody();
    }

    public User getUserByUsername(String username) {
        return getUserByUsername(username, null);
    }

    public User getUserByUsername(String username, Collection<String> includes) {
        URI uri = UriComponentsBuilder.fromHttpUrl(providerApiUrl)
                .path(version)
                .path("/users")
                .queryParam("username", username)
                .queryParam("includes", includes)
                .build()
                .toUri();
        return restTemplate.getForObject(uri, User.class);
    }

    public Set<Role> getRolesByNames(Set<String> names) {
        URI uri = UriComponentsBuilder.fromHttpUrl(providerApiUrl)
                .path(version)
                .path("/roles")
                .queryParam("roleNames", names)
                .build()
                .toUri();
        Role[] result = restTemplate.getForObject(uri, Role[].class);
        if (null == result) return new HashSet<>();
        return Arrays.stream(result).collect(Collectors.toSet());
    }

    public User addUser(User user, Long projectId) {
        URI uri = UriComponentsBuilder.fromHttpUrl(providerApiUrl)
                .path(version)
                .path("/users")
                .queryParam("projectId", projectId)
                .build()
                .toUri();
        ResponseEntity<User> response;
        response = restTemplate.postForEntity(uri, user, User.class);
        return response.getBody();
    }

    public List<User> getUsers(Set<String> includeRoles, Set<String> notIncludeRoles, Set<String> includes) {
        URI uri = UriComponentsBuilder.fromHttpUrl(providerApiUrl)
                .path(version)
                .path("/users")
                .queryParam("includeRoles", includeRoles)
                .queryParam("notIncludeRoles", notIncludeRoles)
                .queryParam("includes", includes)
                .build()
                .toUri();
        User[] result = restTemplate.getForObject(uri, User[].class);
        if (null == result) return new ArrayList<>();
        return Arrays.stream(result).collect(Collectors.toList());
    }

    public User saveUser(User user) {
        URI uri = UriComponentsBuilder.fromHttpUrl(providerApiUrl)
                .path(version)
                .path("/users")
                .build()
                .toUri();
        ResponseEntity<User> response;
        if (user.getId() == null) {
            response = restTemplate.postForEntity(uri, user, User.class);
        } else {
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            HttpEntity<User> entity = new HttpEntity<>(user, headers);
            response = restTemplate.exchange(uri, HttpMethod.PUT, entity, User.class);
        }
        return response.getBody();
    }

    public UserRole saveUserRole(UserRole userRole) {
        URI uri = UriComponentsBuilder.fromHttpUrl(providerApiUrl)
                .path(version)
                .path("/user-roles")
                .build()
                .toUri();
        ResponseEntity<UserRole> response;
        response = restTemplate.postForEntity(uri, userRole, UserRole.class);
        return response.getBody();
    }

    public List<Project> getAllProjects() {
        return getAllProjects(null);
    }

    public List<Project> getAllProjects(List<String> includes) {
        URI uri = UriComponentsBuilder.fromHttpUrl(providerApiUrl)
                .path(version)
                .path("/projects")
                .queryParam("includes", includes)
                .build()
                .toUri();
        Project[] result = restTemplate.getForObject(uri, Project[].class);
        if (null == result) return new ArrayList<>();
        return Arrays.stream(result).collect(Collectors.toList()).stream().sorted().collect(Collectors.toList());
    }

    public String deleteProject(Long id) {
        URI uri = UriComponentsBuilder.fromHttpUrl(providerApiUrl)
                .path(version)
                .path("/projects/{id}")
                .buildAndExpand(id)
                .toUri();
        ResponseEntity<String> response = restTemplate.exchange(uri, HttpMethod.DELETE, null, String.class);
        return response.getBody();
    }

    public Project saveProject(Project project, Set<Long> userIds) {
        URI uri = UriComponentsBuilder.fromHttpUrl(providerApiUrl)
                .path(version)
                .path("/projects")
                .queryParam("userIds", userIds)
                .build()
                .toUri();
        ResponseEntity<Project> response;
        if (project.getId() == null) {
            response = restTemplate.postForEntity(uri, project, Project.class);
        } else {
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            HttpEntity<Project> entity = new HttpEntity<>(project, headers);
            response = restTemplate.exchange(uri, HttpMethod.PUT, entity, Project.class);
        }
        return response.getBody();
    }

    public Project getProjectBySubJob(String jobNumber, String subJob) {
        URI uri = UriComponentsBuilder.fromHttpUrl(providerApiUrl)
                .path(version)
                .path("/projects")
                .queryParam("jobNumber", jobNumber)
                .queryParam("subJob", subJob)
                .build()
                .toUri();
        return restTemplate.getForObject(uri, Project.class);
    }

    public List<BillingCodeType> getBillingCodeTypes(Boolean levelNotNull) {
        URI uri = UriComponentsBuilder.fromHttpUrl(providerApiUrl)
                .path(version)
                .path("/billing-code-types")
                .queryParam("levelNotNull", levelNotNull)
                .build()
                .toUri();
        BillingCodeType[] result = restTemplate.getForObject(uri, BillingCodeType[].class);
        if (null == result) return new ArrayList<>();
        return Arrays.stream(result).collect(Collectors.toList());
    }

    public Page<CostCodeAssociationDto> getCostCodeAssociationDtoPage(Long projectId, PagingRequest pagingRequest) {
        URI uri = UriComponentsBuilder.fromHttpUrl(providerApiUrl)
                .path(version)
                .path("/cost-code-associations")
                .queryParam("projectId", projectId)
                .build()
                .toUri();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<PagingRequest> entity = new HttpEntity<>(pagingRequest, headers);
        ResponseEntity<Page<CostCodeAssociationDto>> response = restTemplate
                .exchange(uri, HttpMethod.POST, entity, new ParameterizedTypeReference<>() {
                });
        return response.getBody();
    }

    public String saveCostCodeStructure(List<Long> ids) {
        URI uri = UriComponentsBuilder.fromHttpUrl(providerApiUrl)
                .path(version)
                .path("/billing-code-types/level")
                .queryParam("ids", ids)
                .build()
                .toUri();
        ResponseEntity<String> response = restTemplate.postForEntity(uri, ids, String.class);
        return response.getBody();
    }

    public List<BillingCodeDto> getBillingCodesByCostCodeIdAndProjectId(Long id, Long projectId) {
        URI uri = UriComponentsBuilder.fromHttpUrl(providerApiUrl)
                .path(version)
                .path("/billing-code-dtos")
                .queryParam("costCodeId", id)
                .queryParam("projectId", projectId)
                .build()
                .toUri();
        BillingCodeDto[] result = restTemplate.getForObject(uri, BillingCodeDto[].class);
        if (null == result) return new ArrayList<>();
        return Arrays.stream(result).collect(Collectors.toList());
    }

    public List<BillingCodeDto> getBillingCodesByTypeIdAndProjectId(Long id, Long projectId) {
        URI uri = UriComponentsBuilder.fromHttpUrl(providerApiUrl)
                .path(version)
                .path("/billing-code-dtos")
                .queryParam("typeId", id)
                .queryParam("projectId", projectId)
                .build()
                .toUri();
        BillingCodeDto[] result = restTemplate.getForObject(uri, BillingCodeDto[].class);
        if (null == result) return new ArrayList<>();
        return Arrays.stream(result).collect(Collectors.toList());
    }

    public CostCodeLog getCostCodeById(Long id) {
        URI uri = UriComponentsBuilder.fromHttpUrl(providerApiUrl)
                .path(version)
                .path("/cost-codes/{id}")
                .buildAndExpand(id)
                .toUri();
        return restTemplate.getForObject(uri, CostCodeLog.class);
    }

    public String saveCostCodeBillingCode(Long projectId, SaveCostCodeAssociationDto dto) {
        URI uri = UriComponentsBuilder.fromHttpUrl(providerApiUrl)
                .path(version)
                .path("/cost-code-billing-codes")
                .queryParam("projectId", projectId)
                .build()
                .toUri();
        ResponseEntity<String> response = restTemplate.postForEntity(uri, dto, String.class);
        return response.getBody();
    }

    public BillingCodeType findBillingCodeTypeById(Long id) {
        URI uri = UriComponentsBuilder.fromHttpUrl(providerApiUrl)
                .path(version)
                .path("/billing-code-types/{id}")
                .buildAndExpand(id)
                .toUri();
        return restTemplate.getForObject(uri, BillingCodeType.class);
    }

    public BillingCodeType saveBillingCodeType(BillingCodeType billingCodeType) {
        URI uri = UriComponentsBuilder.fromHttpUrl(providerApiUrl)
                .path(version)
                .path("/billing-code-types")
                .build()
                .toUri();
        ResponseEntity<BillingCodeType> response;
        if (billingCodeType.getId() == null) {
            response = restTemplate.postForEntity(uri, billingCodeType, BillingCodeType.class);
        } else {
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            HttpEntity<BillingCodeType> entity = new HttpEntity<>(billingCodeType, headers);
            response = restTemplate.exchange(uri, HttpMethod.PUT, entity, BillingCodeType.class);
        }
        return response.getBody();
    }

    public String deleteBillingCodeTypeById(Long id) {
        URI uri = UriComponentsBuilder.fromHttpUrl(providerApiUrl)
                .path(version)
                .path("/billing-code-types/{id}")
                .buildAndExpand(id)
                .toUri();
        ResponseEntity<String> response = restTemplate.exchange(uri, HttpMethod.DELETE, null, String.class);
        return response.getBody();
    }

    public Page<BillingCodeDto> findBillingCodesByTypeId(Long typeId, PagingRequest pagingRequest, Long projectId) {
        URI uri = UriComponentsBuilder.fromHttpUrl(providerApiUrl)
                .path(version)
                .path("/billing-code-dtos")
                .queryParam("projectId", projectId)
                .queryParam("typeId", typeId)
                .build()
                .toUri();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<PagingRequest> entity = new HttpEntity<>(pagingRequest, headers);
        ResponseEntity<Page<BillingCodeDto>> response = restTemplate
                .exchange(uri, HttpMethod.POST, entity, new ParameterizedTypeReference<>() {
                });
        return response.getBody();
    }

    public String deleteBillingCodeById(Long id) {
        URI uri = UriComponentsBuilder.fromHttpUrl(providerApiUrl)
                .path(version)
                .path("/billing-codes/{id}")
                .buildAndExpand(id)
                .toUri();
        ResponseEntity<String> response = restTemplate.exchange(uri, HttpMethod.DELETE, null, String.class);
        return response.getBody();
    }

    public String deleteBillingCodeOverrideByBillingCodeIdAndProjectId(Long billingCodeId, Long projectId) {
        URI uri = UriComponentsBuilder.fromHttpUrl(providerApiUrl)
                .path(version)
                .path("/billing-code-overrides")
                .queryParam("projectId", projectId)
                .queryParam("billingCodeId", billingCodeId)
                .build()
                .toUri();
        ResponseEntity<String> response = restTemplate.exchange(uri, HttpMethod.DELETE, null, String.class);
        return response.getBody();
    }

    public BillingCode findBillingCodeById(Long billingCodeId) {
        URI uri = UriComponentsBuilder.fromHttpUrl(providerApiUrl)
                .path(version)
                .path("/billing-codes/{id}")
                .buildAndExpand(billingCodeId)
                .toUri();
        return restTemplate.getForObject(uri, BillingCode.class);
    }

    public BillingCodeOverride findBillingCodeOverrideByBillingCodeIdAndProjectId(Long billingCodeId, Long projectId) {
        URI uri = UriComponentsBuilder.fromHttpUrl(providerApiUrl)
                .path(version)
                .path("/billing-code-overrides")
                .queryParam("projectId", projectId)
                .queryParam("billingCodeId", billingCodeId)
                .build()
                .toUri();
        return restTemplate.getForObject(uri, BillingCodeOverride.class);
    }

    public List<BillingCodeOverride> findBillingCodeOverridesByProjectId(Long projectId) {
        URI uri = UriComponentsBuilder.fromHttpUrl(providerApiUrl)
                .path(version)
                .path("/billing-code-override-collection")
                .queryParam("projectId", projectId)
                .build()
                .toUri();
        BillingCodeOverride[] result = restTemplate.getForObject(uri, BillingCodeOverride[].class);
        if (result == null) return new ArrayList<>();
        return Arrays.stream(result).collect(Collectors.toList());
    }

    public BillingCodeOverride saveBillingCodeOverride(BillingCodeOverride billingCodeOverride) {
        URI uri = UriComponentsBuilder.fromHttpUrl(providerApiUrl)
                .path(version)
                .path("/billing-code-overrides")
                .build()
                .toUri();
        ResponseEntity<BillingCodeOverride> response;
        if (billingCodeOverride.getId() == null) {
            response = restTemplate.postForEntity(uri, billingCodeOverride, BillingCodeOverride.class);
        } else {
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            HttpEntity<BillingCodeOverride> entity = new HttpEntity<>(billingCodeOverride, headers);
            response = restTemplate.exchange(uri, HttpMethod.PUT, entity, BillingCodeOverride.class);
        }
        return response.getBody();
    }

    public BillingCode saveBillingCode(BillingCode billingCode) {
        URI uri = UriComponentsBuilder.fromHttpUrl(providerApiUrl)
                .path(version)
                .path("/billing-codes")
                .build()
                .toUri();
        ResponseEntity<BillingCode> response;
        if (billingCode.getId() == null) {
            response = restTemplate.postForEntity(uri, billingCode, BillingCode.class);
        } else {
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            HttpEntity<BillingCode> entity = new HttpEntity<>(billingCode, headers);
            response = restTemplate.exchange(uri, HttpMethod.PUT, entity, BillingCode.class);
        }
        return response.getBody();
    }

    public BillingCode findBillingCodeByTypeIdAndCodeName(Long typeId, String codeName) {
        URI uri = UriComponentsBuilder.fromHttpUrl(providerApiUrl)
                .path(version)
                .path("/billing-codes")
                .queryParam("typeId", typeId)
                .queryParam("codeName", codeName)
                .build()
                .toUri();
        return restTemplate.getForObject(uri, BillingCode.class);
    }

    public List<Equipment> getEquipmentByActive(boolean active) {
        URI uri = UriComponentsBuilder.fromHttpUrl(providerApiUrl)
                .path(version)
                .path("/equipment")
                .queryParam("active", active)
                .build()
                .toUri();
        Equipment[] result = restTemplate.getForObject(uri, Equipment[].class);
        if (null == result) return new ArrayList<>();
        return Arrays.stream(result).collect(Collectors.toList());
    }

    public List<EquipmentUsage> getEquipmentUsageByIdsAndDates(Long equipmentId, Long projectId, LocalDate startDate, LocalDate endDate) {
        URI uri = UriComponentsBuilder.fromHttpUrl(providerApiUrl)
                .path(version)
                .path("/equipment-usages")
                .queryParam("equipmentId", equipmentId)
                .queryParam("projectId", projectId)
                .queryParam("startDate", startDate)
                .queryParam("endDate", endDate)
                .build()
                .toUri();
        EquipmentUsage[] result = restTemplate.getForObject(uri, EquipmentUsage[].class);
        if (null == result) return new ArrayList<>();
        return Arrays.stream(result).collect(Collectors.toList());
    }

    public Equipment getEquipmentById(Long id) {
        URI uri = UriComponentsBuilder.fromHttpUrl(providerApiUrl)
                .path(version)
                .path("/equipment/{id}")
                .buildAndExpand(id)
                .toUri();
        return restTemplate.getForObject(uri, Equipment.class);
    }

    public Equipment getEquipmentByEquipmentId(String equipmentId) {
        URI uri = UriComponentsBuilder.fromHttpUrl(providerApiUrl)
                .path(version)
                .path("/equipment")
                .queryParam("equipmentId", equipmentId)
                .build()
                .toUri();
        return restTemplate.getForObject(uri, Equipment.class);
    }

    public Equipment saveEquipment(Equipment equipment) {
        URI uri = UriComponentsBuilder.fromHttpUrl(providerApiUrl)
                .path(version)
                .path("/equipment")
                .build()
                .toUri();
        ResponseEntity<Equipment> response;
        if (equipment.getId() == null) {
            response = restTemplate.postForEntity(uri, equipment, Equipment.class);
        } else {
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            HttpEntity<Equipment> entity = new HttpEntity<>(equipment, headers);
            response = restTemplate.exchange(uri, HttpMethod.PUT, entity, Equipment.class);
        }
        return response.getBody();
    }

    public String changeEquipmentValidity(Long id, boolean active) {
        URI uri = UriComponentsBuilder.fromHttpUrl(providerApiUrl)
                .path(version)
                .path("/equipment/{id}/active")
                .queryParam("active", active)
                .buildAndExpand(id)
                .toUri();
        ResponseEntity<String> response;
        response = restTemplate.exchange(uri, HttpMethod.PUT, null, String.class);
        return response.getBody();
    }

    public EquipmentUsage saveEquipmentUsage(Long equipmentId, Long projectId, Long id, LocalDate date, String type) {
        URI uri = UriComponentsBuilder.fromHttpUrl(providerApiUrl)
                .path(version)
                .path("/equipment-usages")
                .queryParam("projectId", projectId)
                .queryParam("equipmentId", equipmentId)
                .queryParam("id", id)
                .queryParam("date", date)
                .queryParam("type", type)
                .build()
                .toUri();
        HttpMethod method = id == null ? HttpMethod.POST : HttpMethod.PUT;
        ResponseEntity<EquipmentUsage> response = restTemplate.exchange(uri, method, null, EquipmentUsage.class);
        return response.getBody();
    }

    public String deactivateEquipmentUsage(Long id) {
        URI uri = UriComponentsBuilder.fromHttpUrl(providerApiUrl)
                .path(version)
                .path("/equipment-usages/{id}")
                .buildAndExpand(id)
                .toUri();
        ResponseEntity<String> response = restTemplate.exchange(uri, HttpMethod.DELETE, null, String.class);
        return response.getBody();
    }

    public List<Equipment> getEquipmentByOwnershipType(EquipmentOwnershipType ownershipType) {
        URI uri = UriComponentsBuilder.fromHttpUrl(providerApiUrl)
                .path(version)
                .path("/equipment")
                .queryParam("ownershipType", ownershipType)
                .build()
                .toUri();
        Equipment[] result = restTemplate.getForObject(uri, Equipment[].class);
        if (null == result) return new ArrayList<>();
        return Arrays.stream(result).collect(Collectors.toList());
    }

    public Page<EquipmentPriceDto> getEquipmentPricesByProjectId(Long projectId, PagingRequest pagingRequest) {
        URI uri = UriComponentsBuilder.fromHttpUrl(providerApiUrl)
                .path(version)
                .path("/equipment-price-dtos")
                .queryParam("projectId", projectId)
                .build()
                .toUri();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<PagingRequest> entity = new HttpEntity<>(pagingRequest, headers);
        ResponseEntity<Page<EquipmentPriceDto>> response = restTemplate
                .exchange(uri, HttpMethod.POST, entity, new ParameterizedTypeReference<>() {
                });
        return response.getBody();
    }

    public EquipmentPrice getEquipmentPriceById(Long id) {
        URI uri = UriComponentsBuilder.fromHttpUrl(providerApiUrl)
                .path(version)
                .path("/equipment-prices/{id}")
                .buildAndExpand(id)
                .toUri();
        return restTemplate.getForObject(uri, EquipmentPrice.class);
    }

    public EquipmentPrice saveEquipmentPrice(EquipmentPrice equipmentPrice) {
        URI uri = UriComponentsBuilder.fromHttpUrl(providerApiUrl)
                .path(version)
                .path("/equipment-prices")
                .build()
                .toUri();
        ResponseEntity<EquipmentPrice> response;
        if (equipmentPrice.getId() == null) {
            response = restTemplate.postForEntity(uri, equipmentPrice, EquipmentPrice.class);
        } else {
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            HttpEntity<EquipmentPrice> entity = new HttpEntity<>(equipmentPrice, headers);
            response = restTemplate.exchange(uri, HttpMethod.PUT, entity, EquipmentPrice.class);
        }
        return response.getBody();
    }

    public String deleteEquipmentPriceById(Long id) {
        URI uri = UriComponentsBuilder.fromHttpUrl(providerApiUrl)
                .path(version)
                .path("/equipment-prices/{id}")
                .buildAndExpand(id)
                .toUri();
        ResponseEntity<String> response = restTemplate.exchange(uri, HttpMethod.DELETE, null, String.class);
        return response.getBody();
    }

    public Set<Equipment> getEquipmentLog(Long projectId) {
        URI uri = UriComponentsBuilder.fromHttpUrl(providerApiUrl)
                .path(version)
                .path("/equipment-logs")
                .queryParam("projectId", projectId)
                .build()
                .toUri();
        Equipment[] result = restTemplate.getForObject(uri, Equipment[].class);
        if (null == result) return new HashSet<>();
        return Arrays.stream(result).collect(Collectors.toSet());
    }

    public List<Notification> getNotificationsByUserId() {
        URI uri = UriComponentsBuilder.fromHttpUrl(providerApiUrl)
                .path(version)
                .path("/notifications")
                .build()
                .toUri();
        Notification[] result = restTemplate.getForObject(uri, Notification[].class);
        if (null == result) return new ArrayList<>();
        return Arrays.stream(result).collect(Collectors.toList());
    }

    public String updateNotificationRead(Long id, boolean read) {
        URI uri = UriComponentsBuilder.fromHttpUrl(providerApiUrl)
                .path(version)
                .path("/notifications/{id}/read")
                .queryParam("read", read)
                .buildAndExpand(id)
                .toUri();
        ResponseEntity<String> response;
        response = restTemplate.exchange(uri, HttpMethod.PUT, null, String.class);
        return response.getBody();
    }

    public int getNotificationCountByUserIdAndRead(boolean read) {
        URI uri = UriComponentsBuilder.fromHttpUrl(providerApiUrl)
                .path(version)
                .path("/notifications")
                .queryParam("read", read)
                .build()
                .toUri();
        Integer result = restTemplate.getForObject(uri, Integer.class);
        return result == null ? 0 : result;
    }

    public List<Notification> saveNotifications(List<Notification> notifications) {
        URI uri = UriComponentsBuilder.fromHttpUrl(providerApiUrl)
                .path(version)
                .path("/notifications")
                .build()
                .toUri();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<List<Notification>> entity = new HttpEntity<>(notifications, headers);
        ResponseEntity<List<Notification>> response = restTemplate
                .exchange(uri, HttpMethod.POST, entity, new ParameterizedTypeReference<>() {
                });
        return response.getBody();
    }

    public List<StageTransaction> getStageTransactions(Long projectId, String teamName, LocalDate startDate, LocalDate endDate, List<String> includes) {
        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(providerApiUrl)
                .path(version)
                .path("/stage-transactions")
                .queryParam("projectId", projectId)
                .queryParam("startDate", startDate)
                .queryParam("endDate", endDate)
                .queryParam("includes", includes);
        if (StringUtils.hasText(teamName)) builder = builder.queryParam("teamName", teamName);
        URI uri = builder.build().toUri();
        StageTransaction[] result = restTemplate.getForObject(uri, StageTransaction[].class);
        if (null == result) return new ArrayList<>();
        return Arrays.stream(result).collect(Collectors.toList());
    }

    public List<TransSubmission> getTransSubmissions(Long projectId, LocalDate startDate, LocalDate endDate, List<String> includes) {
        URI uri = UriComponentsBuilder.fromHttpUrl(providerApiUrl)
                .path(version)
                .path("/trans-submissions")
                .queryParam("projectId", projectId)
                .queryParam("startDate", startDate)
                .queryParam("endDate", endDate)
                .queryParam("includes", includes)
                .build()
                .toUri();
        TransSubmission[] result = restTemplate.getForObject(uri, TransSubmission[].class);
        if (null == result) return new ArrayList<>();
        return Arrays.stream(result).collect(Collectors.toList());
    }

    public String saveUserTeams(Long userId, Long projectId, Set<String> teams) {
        URI uri = UriComponentsBuilder.fromHttpUrl(providerApiUrl)
                .path(version)
                .path("/user-teams")
                .queryParam("projectId", projectId)
                .queryParam("userId", userId)
                .build()
                .toUri();
        ResponseEntity<String> response;
        response = restTemplate.postForEntity(uri, teams, String.class);
        return response.getBody();
    }

    public Set<UserTeam> getUserTeams(Long userId, Long projectId) {
        URI uri = UriComponentsBuilder.fromHttpUrl(providerApiUrl)
                .path(version)
                .path("/user-teams")
                .queryParam("projectId", projectId)
                .queryParam("userId", userId)
                .build()
                .toUri();
        UserTeam[] result = restTemplate.getForObject(uri, UserTeam[].class);
        if (null == result) return new HashSet<>();
        return Arrays.stream(result).collect(Collectors.toSet());
    }

    public Set<CostCodeLog> getCostCodeLogsByProjectIdAndTeamAndDateOfService(Long projectId, String teamName, LocalDate dateOfService) {
        if (!StringUtils.hasText(teamName)) return new HashSet<>();
        URI uri = UriComponentsBuilder.fromHttpUrl(providerApiUrl)
                .path(version)
                .path("/cost-codes")
                .queryParam("projectId", projectId)
                .queryParam("dateOfService", dateOfService)
                .queryParam("teamName", teamName)
                .build()
                .toUri();
        CostCodeLog[] result = restTemplate.getForObject(uri, CostCodeLog[].class);
        if (null == result) return new HashSet<>();
        return Arrays.stream(result).collect(Collectors.toSet());
    }

    public String saveReports(List<ReportRecord> reportRecords, String teamName, Long projectId) {
        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(providerApiUrl)
                .path(version)
                .path("/reports")
                .queryParam("projectId", projectId);
        if (StringUtils.hasText(teamName)) builder = builder.queryParam("teamName", teamName);
        URI uri = builder.build().toUri();
        ResponseEntity<String> response;
        response = restTemplate.postForEntity(uri, reportRecords, String.class);
        return response.getBody();
    }

    public AllocationSubmissionDto getAllocationSubmissionDtoByDateOfService(Long projectId, String teamName, LocalDate dateOfService) {
        URI uri = UriComponentsBuilder.fromHttpUrl(providerApiUrl)
                .path(version)
                .path("/allocation-submission-dtos")
                .queryParam("projectId", projectId)
                .queryParam("teamName", teamName)
                .queryParam("dateOfService", dateOfService)
                .build()
                .toUri();
        return restTemplate.getForObject(uri, AllocationSubmissionDto.class);
    }

    public AllocationSubmissionDto getAllocationSubmissionDtoById(Long allocationSubmissionId) {
        URI uri = UriComponentsBuilder.fromHttpUrl(providerApiUrl)
                .path(version)
                .path("/allocation-submission-dtos/{id}")
                .buildAndExpand(allocationSubmissionId)
                .toUri();
        return restTemplate.getForObject(uri, AllocationSubmissionDto.class);
    }

    public Set<RainOut> getRainOuts(Long projectId) {
        URI uri = UriComponentsBuilder.fromHttpUrl(providerApiUrl)
                .path(version)
                .path("/rain-outs")
                .queryParam("projectId", projectId)
                .build()
                .toUri();
        RainOut[] result = restTemplate.getForObject(uri, RainOut[].class);
        if (null == result) return new HashSet<>();
        return Arrays.stream(result).collect(Collectors.toSet());
    }

    public RainOut getRainOutById(Long id) {
        URI uri = UriComponentsBuilder.fromHttpUrl(providerApiUrl)
                .path(version)
                .path("/rain-outs/{id}")
                .buildAndExpand(id)
                .toUri();
        return restTemplate.getForObject(uri, RainOut.class);
    }

    public RainOut saveRainOut(RainOut rainOut) {
        URI uri = UriComponentsBuilder.fromHttpUrl(providerApiUrl)
                .path(version)
                .path("/rain-outs")
                .build()
                .toUri();
        ResponseEntity<RainOut> response;
        response = restTemplate.postForEntity(uri, rainOut, RainOut.class);
        return response.getBody();
    }

    public List<ReportSubmission> getReportSubmissions(Long projectId, String teamName, LocalDate startDate, LocalDate endDate, List<String> includes) {
        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(providerApiUrl)
                .path(version)
                .path("/report-submissions")
                .queryParam("projectId", projectId)
                .queryParam("startDate", startDate)
                .queryParam("endDate", endDate)
                .queryParam("includes", includes);
        if (StringUtils.hasText(teamName)) builder = builder.queryParam("teamName", teamName);
        URI uri = builder.build().toUri();
        ReportSubmission[] result = restTemplate.getForObject(uri, ReportSubmission[].class);
        if (null == result) return new ArrayList<>();
        return Arrays.stream(result).collect(Collectors.toList());
    }

    public String lockCostCodes(Set<String> costCodes, Long projectId) {
        URI uri = UriComponentsBuilder.fromHttpUrl(providerApiUrl)
                .path(version)
                .path("/cost-codes/is-lock")
                .queryParam("projectId", projectId)
                .build()
                .toUri();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<Set<String>> entity = new HttpEntity<>(costCodes, headers);
        ResponseEntity<String> response;
        response = restTemplate.exchange(uri, HttpMethod.PUT, entity, String.class);
        return response.getBody();
    }

    public AllocationSubmission saveAllocationSubmission(AllocationSubmission allocationSubmission) {
        URI uri = UriComponentsBuilder.fromHttpUrl(providerApiUrl)
                .path(version)
                .path("/allocation-submissions")
                .build()
                .toUri();
        ResponseEntity<AllocationSubmission> response;
        if (allocationSubmission.getId() == null) {
            response = restTemplate.postForEntity(uri, allocationSubmission, AllocationSubmission.class);
        } else {
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            HttpEntity<AllocationSubmission> entity = new HttpEntity<>(allocationSubmission, headers);
            response = restTemplate.exchange(uri, HttpMethod.PUT, entity, AllocationSubmission.class);
        }
        return response.getBody();
    }

    public WeeklyProcess saveWeeklyProcess(WeeklyProcess weeklyProcess) {
        URI uri = UriComponentsBuilder.fromHttpUrl(providerApiUrl)
                .path(version)
                .path("/weekly-processes")
                .build()
                .toUri();
        ResponseEntity<WeeklyProcess> response;
        if (weeklyProcess.getId() == null) {
            response = restTemplate.postForEntity(uri, weeklyProcess, WeeklyProcess.class);
        } else {
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            HttpEntity<WeeklyProcess> entity = new HttpEntity<>(weeklyProcess, headers);
            response = restTemplate.exchange(uri, HttpMethod.PUT, entity, WeeklyProcess.class);
        }
        return response.getBody();
    }

    public AllocationSubmission getAllocationSubmissionById(Long allocationSubmissionId) {
        URI uri = UriComponentsBuilder.fromHttpUrl(providerApiUrl)
                .path(version)
                .path("/allocation-submissions/{id}")
                .buildAndExpand(allocationSubmissionId)
                .toUri();
        return restTemplate.getForObject(uri, AllocationSubmission.class);
    }

    public String deleteStageTransactionsByTeamAndDateOfService(String team, LocalDate dateOfService, Long projectId) {
        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(providerApiUrl)
                .path(version)
                .path("/stage-transactions")
                .queryParam("projectId", projectId)
                .queryParam("dateOfService", dateOfService);
        if (StringUtils.hasText(team)) builder = builder.queryParam("team", team);
        URI uri = builder.build().toUri();
        ResponseEntity<String> response = restTemplate.exchange(uri, HttpMethod.DELETE, null, String.class);
        return response.getBody();
    }

    public String deleteAllocationSubmissionById(Long allocationSubmissionId) {
        URI uri = UriComponentsBuilder.fromHttpUrl(providerApiUrl)
                .path(version)
                .path("/allocation-submissions/{id}")
                .buildAndExpand(allocationSubmissionId)
                .toUri();
        ResponseEntity<String> response = restTemplate.exchange(uri, HttpMethod.DELETE, null, String.class);
        return response.getBody();
    }

    public TransSubmission saveTransSubmission(TransSubmission transSubmission) {
        URI uri = UriComponentsBuilder.fromHttpUrl(providerApiUrl)
                .path(version)
                .path("/trans-submissions")
                .build()
                .toUri();
        ResponseEntity<TransSubmission> response;
        response = restTemplate.postForEntity(uri, transSubmission, TransSubmission.class);
        return response.getBody();
    }

    public String saveAllocationSubmissions(Set<AllocationSubmission> allocationSubmissions) {
        URI uri = UriComponentsBuilder.fromHttpUrl(providerApiUrl)
                .path(version)
                .path("/allocation-submission-collection")
                .build()
                .toUri();
        ResponseEntity<String> response;
        response = restTemplate.postForEntity(uri, allocationSubmissions, String.class);
        return response.getBody();
    }

    public String saveAllocationTimes(Set<AllocationTime> allocationTimes) {
        URI uri = UriComponentsBuilder.fromHttpUrl(providerApiUrl)
                .path(version)
                .path("/allocation-time-collection")
                .build()
                .toUri();
        ResponseEntity<String> response;
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<Set<AllocationTime>> entity = new HttpEntity<>(allocationTimes, headers);
        response = restTemplate.exchange(uri, HttpMethod.PATCH, entity, String.class);
        return response.getBody();
    }

    public List<AllocationTime> getAllocationTimesById(Long projectId, Set<Long> allocationTimeIds, Set<String> includes) {
        URI uri = UriComponentsBuilder.fromHttpUrl(providerApiUrl)
                .path(version)
                .path("/allocation-time-collection")
                .queryParam("projectId", projectId)
                .queryParam("includes", includes)
                .build()
                .toUri();
        ResponseEntity<AllocationTime[]> response = restTemplate.postForEntity(uri, allocationTimeIds, AllocationTime[].class);
        AllocationTime[] result = response.getBody();
        if (null == result) return new ArrayList<>();
        return Arrays.stream(result).collect(Collectors.toList());
    }

    public Set<EquipmentAllocationTime> getEquipmentAllocationTimes(EquipmentAllocationTimeFilter filter) {
        URI uri = UriComponentsBuilder.fromHttpUrl(providerApiUrl)
                .path(version)
                .path("/equipment-allocation-times-filtered")
                .build()
                .toUri();

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<EquipmentAllocationTimeFilter> entity = new HttpEntity<>(filter, headers);
        ResponseEntity<EquipmentAllocationTime[]> response = restTemplate
                .exchange(uri, HttpMethod.POST, entity, new ParameterizedTypeReference<>() {
                });
        EquipmentAllocationTime[] result = response.getBody();
        if (result == null) return new HashSet<>();
        return Arrays.stream(result).collect(Collectors.toSet());
    }

    public Set<EquipmentAllocationTime> getEquipmentAllocationTimesById(Set<Long> ids) {
        URI uri = UriComponentsBuilder.fromHttpUrl(providerApiUrl)
                .path(version)
                .path("/equipment-allocation-time-collection")
                .build()
                .toUri();
        ResponseEntity<EquipmentAllocationTime[]> response = restTemplate.postForEntity(uri, ids, EquipmentAllocationTime[].class);
        EquipmentAllocationTime[] result = response.getBody();
        if (null == result) return new HashSet<>();
        return Arrays.stream(result).collect(Collectors.toSet());
    }

    public String saveEquipmentAllocationSubmissions(Set<EquipmentAllocationSubmission> submissions) {
        URI uri = UriComponentsBuilder.fromHttpUrl(providerApiUrl)
                .path(version)
                .path("/equipment-allocation-submission-collection")
                .build()
                .toUri();
        ResponseEntity<String> response;
        response = restTemplate.postForEntity(uri, submissions, String.class);
        return response.getBody();
    }

    public String saveEquipmentAllocationTimes(Set<EquipmentAllocationTime> equipmentAllocationTimes) {
        URI uri = UriComponentsBuilder.fromHttpUrl(providerApiUrl)
                .path(version)
                .path("/equipment-allocation-time-collection")
                .build()
                .toUri();
        ResponseEntity<String> response;
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<Set<EquipmentAllocationTime>> entity = new HttpEntity<>(equipmentAllocationTimes, headers);
        response = restTemplate.exchange(uri, HttpMethod.PATCH, entity, String.class);
        return response.getBody();
    }

    public EquipmentWeeklyProcess saveEquipmentWeeklyProcess(EquipmentWeeklyProcess equipmentWeeklyProcess) {
        URI uri = UriComponentsBuilder.fromHttpUrl(providerApiUrl)
                .path(version)
                .path("/equipment-weekly-processes")
                .build()
                .toUri();
        ResponseEntity<EquipmentWeeklyProcess> response;
        if (equipmentWeeklyProcess.getId() == null) {
            response = restTemplate.postForEntity(uri, equipmentWeeklyProcess, EquipmentWeeklyProcess.class);
        } else {
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            HttpEntity<EquipmentWeeklyProcess> entity = new HttpEntity<>(equipmentWeeklyProcess, headers);
            response = restTemplate.exchange(uri, HttpMethod.PUT, entity, EquipmentWeeklyProcess.class);
        }
        return response.getBody();
    }

    public String deleteCostCodeLogById(Long id) {
        URI uri = UriComponentsBuilder.fromHttpUrl(providerApiUrl)
                .path(version)
                .path("/cost-codes/{id}")
                .buildAndExpand(id)
                .toUri();
        ResponseEntity<String> response = restTemplate.exchange(uri, HttpMethod.DELETE, null, String.class);
        return response.getBody();
    }

    public Employee getEmployeeById(Long id) {
        URI uri = UriComponentsBuilder.fromHttpUrl(providerApiUrl)
                .path(version)
                .path("/employees/{id}")
                .buildAndExpand(id)
                .toUri();
        return restTemplate.getForObject(uri, Employee.class);
    }

    public Employee saveEmployee(Employee employee) {
        URI uri = UriComponentsBuilder.fromHttpUrl(providerApiUrl)
                .path(version)
                .path("/employees")
                .build()
                .toUri();
        ResponseEntity<Employee> response;
        if (employee.getId() == null) {
            response = restTemplate.postForEntity(uri, employee, Employee.class);
        } else {
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            HttpEntity<Employee> entity = new HttpEntity<>(employee, headers);
            response = restTemplate.exchange(uri, HttpMethod.PUT, entity, Employee.class);
        }
        return response.getBody();
    }

    public Page<EmployeeAuditDto> getEmployeeAudit(Long projectId, PagingRequest pagingRequest) {
        URI uri = UriComponentsBuilder.fromHttpUrl(providerApiUrl)
                .path(version)
                .path("/employee-audit-dtos")
                .queryParam("projectId", projectId)
                .build()
                .toUri();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<PagingRequest> entity = new HttpEntity<>(pagingRequest, headers);
        ResponseEntity<Page<EmployeeAuditDto>> response = restTemplate
                .exchange(uri, HttpMethod.POST, entity, new ParameterizedTypeReference<>() {
                });
        return response.getBody();
    }

    public Page<EmployeeDto> getEmployees(PagingRequest pagingRequest, Long projectId) {
        URI uri = UriComponentsBuilder.fromHttpUrl(providerApiUrl)
                .path(version)
                .path("/employee-dtos")
                .queryParam("projectId", projectId)
                .build()
                .toUri();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<PagingRequest> entity = new HttpEntity<>(pagingRequest, headers);
        ResponseEntity<Page<EmployeeDto>> response = restTemplate
                .exchange(uri, HttpMethod.POST, entity, new ParameterizedTypeReference<>() {
                });
        return response.getBody();
    }

    public Page<ProjectPayrollDto> getProjectPayrollPage(Long projectId, PagingRequest pagingRequest) {
        URI uri = UriComponentsBuilder.fromHttpUrl(providerApiUrl)
                .path(version)
                .path("/project-payroll-dtos")
                .queryParam("projectId", projectId)
                .build()
                .toUri();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<PagingRequest> entity = new HttpEntity<>(pagingRequest, headers);
        ResponseEntity<Page<ProjectPayrollDto>> response = restTemplate
                .exchange(uri, HttpMethod.POST, entity, new ParameterizedTypeReference<>() {
                });
        return response.getBody();
    }

    public ProjectShift getProjectShiftByProjectId(Long projectId) {
        URI uri = UriComponentsBuilder.fromHttpUrl(providerApiUrl)
                .path(version)
                .path("/project-shifts")
                .queryParam("projectId", projectId)
                .build()
                .toUri();
        return restTemplate.getForObject(uri, ProjectShift.class);
    }

    public ProjectShift saveProjectShift(ProjectShift projectShift) {
        URI uri = UriComponentsBuilder.fromHttpUrl(providerApiUrl)
                .path(version)
                .path("/project-shifts")
                .build()
                .toUri();
        ResponseEntity<ProjectShift> response = restTemplate.postForEntity(uri, projectShift, ProjectShift.class);
        return response.getBody();
    }

    public String upsertProjectShift(ProjectShift projectShift) {
        URI uri = UriComponentsBuilder.fromHttpUrl(providerApiUrl)
                .path(version)
                .path("/project-shifts")
                .build()
                .toUri();
        ResponseEntity<String> response;
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<ProjectShift> entity = new HttpEntity<>(projectShift, headers);
        response = restTemplate.exchange(uri, HttpMethod.PUT, entity, String.class);
        return response.getBody();
    }

    public Page<PurchaseOrderSummaryDto> getPurchaseOrderSummaryDtoPage(PagingRequest pagingRequest, Long projectId) {
        URI uri = UriComponentsBuilder.fromHttpUrl(providerApiUrl)
                .path(version)
                .path("/purchase-order-summary-dtos")
                .queryParam("projectId", projectId)
                .build()
                .toUri();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<PagingRequest> entity = new HttpEntity<>(pagingRequest, headers);
        ResponseEntity<Page<PurchaseOrderSummaryDto>> response = restTemplate
                .exchange(uri, HttpMethod.POST, entity, new ParameterizedTypeReference<>() {
                });
        return response.getBody();
    }

    public Page<PurchaseOrderDetailDto> getPurchaseOrderDetailDtoPage(PagingRequest pagingRequest, String purchaseOrderNumber) {
        URI uri = UriComponentsBuilder.fromHttpUrl(providerApiUrl)
                .path(version)
                .path("/purchase-order-detail-dtos")
                .queryParam("purchaseOrderNumber", purchaseOrderNumber)
                .build()
                .toUri();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<PagingRequest> entity = new HttpEntity<>(pagingRequest, headers);
        ResponseEntity<Page<PurchaseOrderDetailDto>> response = restTemplate
                .exchange(uri, HttpMethod.POST, entity, new ParameterizedTypeReference<>() {
                });
        return response.getBody();
    }

    public RequisitionDto getRequisitionDto(String requisitionNumber) {
        URI uri = UriComponentsBuilder.fromHttpUrl(providerApiUrl)
                .path(version)
                .path("/requisition-dtos")
                .queryParam("requisitionNumber", requisitionNumber)
                .build()
                .toUri();
        return restTemplate.getForObject(uri, RequisitionDto.class);
    }

    public String deleteRequisitionByRequisitionNumber(String requisitionNumber) {
        URI uri = UriComponentsBuilder.fromHttpUrl(providerApiUrl)
                .path(version)
                .path("/requisitions")
                .queryParam("requisitionNumber", requisitionNumber)
                .build()
                .toUri();
        ResponseEntity<String> response = restTemplate.exchange(uri, HttpMethod.DELETE, null, String.class);
        return response.getBody();
    }

    public String saveRequisitions(RequisitionDto requisitionDto, Long projectId) {
        URI uri = UriComponentsBuilder.fromHttpUrl(providerApiUrl)
                .path(version)
                .path("/requisitions")
                .queryParam("projectId", projectId)
                .build()
                .toUri();
        ResponseEntity<String> response;
        response = restTemplate.postForEntity(uri, requisitionDto, String.class);
        return response.getBody();
    }

    public Page<RequisitionGroup> getRequisitionGroups(PagingRequest pagingRequest, Long projectId, boolean purchaseNumberNull) {
        URI uri = UriComponentsBuilder.fromHttpUrl(providerApiUrl)
                .path(version)
                .path("/requisition-groups")
                .queryParam("projectId", projectId)
                .queryParam("purchaseNumberNull", purchaseNumberNull)
                .build()
                .toUri();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<PagingRequest> entity = new HttpEntity<>(pagingRequest, headers);
        ResponseEntity<Page<RequisitionGroup>> response = restTemplate
                .exchange(uri, HttpMethod.POST, entity, new ParameterizedTypeReference<>() {
                });
        return response.getBody();
    }

    public Set<String> getDistinctLatestPurchaseOrderNumbers() {
        URI uri = UriComponentsBuilder.fromHttpUrl(providerApiUrl)
                .path(version)
                .path("/purchase-order-number-distinct")
                .build()
                .toUri();
        ResponseEntity<Set<String>> response = restTemplate
                .exchange(uri, HttpMethod.GET, null, new ParameterizedTypeReference<>() {
                });
        return response.getBody();
    }

    public PurchaseOrderBilling savePurchaseOrderBilling(PurchaseOrderBillingDto dto) {
        URI uri = UriComponentsBuilder.fromHttpUrl(providerApiUrl)
                .path(version)
                .path("/purchase-order-billings")
                .build()
                .toUri();
        ResponseEntity<PurchaseOrderBilling> response;
        response = restTemplate.postForEntity(uri, dto, PurchaseOrderBilling.class);
        return response.getBody();
    }

    public ReconcileDto getReconcileDto(LocalDate weekEndDate) {
        URI uri = UriComponentsBuilder.fromHttpUrl(providerApiUrl)
                .path(version)
                .path("/reconcile-dtos")
                .queryParam("weekEndDate", weekEndDate)
                .build()
                .toUri();
        return restTemplate.getForObject(uri, ReconcileDto.class);
    }

    public Set<Shift> getShiftsByProjectId(Long projectId) {
        URI uri = UriComponentsBuilder.fromHttpUrl(providerApiUrl)
                .path(version)
                .path("/shifts")
                .queryParam("projectId", projectId)
                .build()
                .toUri();
        Shift[] result = restTemplate.getForObject(uri, Shift[].class);
        if (result == null) return new HashSet<>();
        return Arrays.stream(result).collect(Collectors.toCollection(TreeSet::new));
    }

    public Shift getShiftById(Long id) {
        URI uri = UriComponentsBuilder.fromHttpUrl(providerApiUrl)
                .path(version)
                .path("/shifts/{id}")
                .buildAndExpand(id)
                .toUri();
        return restTemplate.getForObject(uri, Shift.class);
    }

    public Shift saveShift(Shift shift) {
        URI uri = UriComponentsBuilder.fromHttpUrl(providerApiUrl)
                .path(version)
                .path("/shifts")
                .build()
                .toUri();
        ResponseEntity<Shift> response;
        if (shift.getId() == null) {
            response = restTemplate.postForEntity(uri, shift, Shift.class);
        } else {
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            HttpEntity<Shift> entity = new HttpEntity<>(shift, headers);
            response = restTemplate.exchange(uri, HttpMethod.PUT, entity, Shift.class);
        }
        return response.getBody();
    }

    public String deleteShiftById(Long id) {
        URI uri = UriComponentsBuilder.fromHttpUrl(providerApiUrl)
                .path(version)
                .path("/shifts/{id}")
                .buildAndExpand(id)
                .toUri();
        ResponseEntity<String> response = restTemplate.exchange(uri, HttpMethod.DELETE, null, String.class);
        return response.getBody();
    }

    public Set<String> getAllShiftNamesByProjectId(Long projectId) {
        URI uri = UriComponentsBuilder.fromHttpUrl(providerApiUrl)
                .path(version)
                .path("/shift-names")
                .queryParam("projectId", projectId)
                .build()
                .toUri();
        ResponseEntity<Set<String>> response = restTemplate
                .exchange(uri, HttpMethod.GET, null, new ParameterizedTypeReference<>() {
                });
        return response.getBody();
    }

    public WeeklyProcess findOrGenerateWeeklyProcessesByProjectIdAndDateAndType(LocalDate weekEndDate,
                                                                                WeeklyProcessType type,
                                                                                Long projectId,
                                                                                String teamName) {
        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(providerApiUrl)
                .path(version)
                .path("/weekly-processes")
                .queryParam("weekEndDate", weekEndDate)
                .queryParam("type", type)
                .queryParam("projectId", projectId);
        if (StringUtils.hasText(teamName)) builder = builder.queryParam("teamName", teamName);
        URI uri = builder.build().toUri();
        return restTemplate.getForObject(uri, WeeklyProcess.class);
    }

    public Set<WeeklyProcess> findWeeklyProcessesByProjectIdAndTeamAndDateAndType(LocalDate startDate,
                                                                                  LocalDate endDate,
                                                                                  WeeklyProcessType type,
                                                                                  Long projectId,
                                                                                  String teamName,
                                                                                  Set<String> includes) {
        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(providerApiUrl)
                .path(version)
                .path("/weekly-processes")
                .queryParam("startDate", startDate)
                .queryParam("endDate", endDate)
                .queryParam("type", type)
                .queryParam("projectId", projectId)
                .queryParam("includes", includes);
        if (StringUtils.hasText(teamName)) builder = builder.queryParam("teamName", teamName);
        URI uri = builder.build().toUri();
        WeeklyProcess[] result = restTemplate.getForObject(uri, WeeklyProcess[].class);
        if (null == result) return new HashSet<>();
        return Arrays.stream(result).collect(Collectors.toSet());
    }

    public Set<WeeklyProcess> findWeeklyProcessesByProjectIdAndDateAndType(LocalDate startDate,
                                                                           LocalDate endDate,
                                                                           WeeklyProcessType type,
                                                                           Long projectId,
                                                                           Set<String> includes) {
        return findWeeklyProcessesByProjectIdAndTeamAndDateAndType(startDate, endDate, type, projectId, "", includes);
    }

    public WeeklyProcess findWeeklyProcessById(Long id, Set<String> includes) {
        URI uri = UriComponentsBuilder.fromHttpUrl(providerApiUrl)
                .path(version)
                .path("/weekly-processes/{id}")
                .queryParam("includes", includes)
                .buildAndExpand(id)
                .toUri();
        return restTemplate.getForObject(uri, WeeklyProcess.class);
    }

    public Set<WeeklyProcess> findWeeklyProcessesById(List<Long> ids, Set<String> includes) {
        URI uri = UriComponentsBuilder.fromHttpUrl(providerApiUrl)
                .path(version)
                .path("/weekly-process-collection")
                .queryParam("includes", includes)
                .build()
                .toUri();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<Set<Long>> entity = new HttpEntity<>(new HashSet<>(ids), headers);
        ResponseEntity<WeeklyProcess[]> response = restTemplate.exchange(uri, HttpMethod.POST, entity, WeeklyProcess[].class);
        if (null == response.getBody()) return new HashSet<>();
        return Arrays.stream(response.getBody()).collect(Collectors.toSet());
    }

    public Set<EquipmentPrice> getEquipmentPricesByProjectIdAndClasses(Long projectId, Set<String> equipmentClasses) {
        URI uri = UriComponentsBuilder.fromHttpUrl(providerApiUrl)
                .path(version)
                .path("/equipment-prices-filtered")
                .queryParam("projectId", projectId)
                .build()
                .toUri();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<Set<String>> entity = new HttpEntity<>(equipmentClasses, headers);
        ResponseEntity<EquipmentPrice[]> response = restTemplate
                .exchange(uri, HttpMethod.POST, entity, new ParameterizedTypeReference<>() {
                });
        EquipmentPrice[] result = response.getBody();
        if (null == result) return new HashSet<>();
        return Arrays.stream(result).collect(Collectors.toSet());
    }

    public EquipmentWeeklyProcess getEquipmentWeeklyProcessByProjectIdAndDateAndType(Long projectId, LocalDate weekEndDate, EquipmentWeeklyProcessType type) {
        URI uri = UriComponentsBuilder.fromHttpUrl(providerApiUrl)
                .path(version)
                .path("/equipment-weekly-processes")
                .queryParam("projectId", projectId)
                .queryParam("weekEndDate", weekEndDate)
                .queryParam("type", type)
                .build()
                .toUri();
        return restTemplate.getForObject(uri, EquipmentWeeklyProcess.class);
    }

    public Page<CostCodeTagType> getCostCodeTagTypePage(Long projectId, PagingRequest pagingRequest) {
        URI uri = UriComponentsBuilder.fromHttpUrl(providerApiUrl)
                .path(version)
                .path("/cost-code-tag-type-page")
                .queryParam("projectId", projectId)
                .build()
                .toUri();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<PagingRequest> entity = new HttpEntity<>(pagingRequest, headers);
        ResponseEntity<Page<CostCodeTagType>> response = restTemplate
                .exchange(uri, HttpMethod.POST, entity, new ParameterizedTypeReference<>() {
                });
        return response.getBody();
    }

    public Page<CostCodeTag> getCostCodeTagPage(Long typeId, PagingRequest pagingRequest) {
        URI uri = UriComponentsBuilder.fromHttpUrl(providerApiUrl)
                .path(version)
                .path("/cost-code-tag-page")
                .queryParam("typeId", typeId)
                .build()
                .toUri();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<PagingRequest> entity = new HttpEntity<>(pagingRequest, headers);
        ResponseEntity<Page<CostCodeTag>> response = restTemplate
                .exchange(uri, HttpMethod.POST, entity, new ParameterizedTypeReference<>() {
                });
        return response.getBody();
    }

    public CostCodeTagType getCostCodeTagTypeById(Long id) {
        URI uri = UriComponentsBuilder.fromHttpUrl(providerApiUrl)
                .path(version)
                .path("/cost-code-tag-types/{id}")
                .buildAndExpand(id)
                .toUri();
        return restTemplate.getForObject(uri, CostCodeTagType.class);
    }

    public CostCodeTag getCostCodeTagById(Long id) {
        URI uri = UriComponentsBuilder.fromHttpUrl(providerApiUrl)
                .path(version)
                .path("/cost-code-tags/{id}")
                .buildAndExpand(id)
                .toUri();
        return restTemplate.getForObject(uri, CostCodeTag.class);
    }

    public String deleteCostCodeTagTypeById(Long id) {
        URI uri = UriComponentsBuilder.fromHttpUrl(providerApiUrl)
                .path(version)
                .path("/cost-code-tag-types/{id}")
                .buildAndExpand(id)
                .toUri();
        ResponseEntity<String> response = restTemplate.exchange(uri, HttpMethod.DELETE, null, String.class);
        return response.getBody();
    }

    public String deleteCostCodeTagById(Long id) {
        URI uri = UriComponentsBuilder.fromHttpUrl(providerApiUrl)
                .path(version)
                .path("/cost-code-tags/{id}")
                .buildAndExpand(id)
                .toUri();
        ResponseEntity<String> response = restTemplate.exchange(uri, HttpMethod.DELETE, null, String.class);
        return response.getBody();
    }

    public CostCodeTag saveTagCode(CostCodeTag costCodeTag) {
        URI uri = UriComponentsBuilder.fromHttpUrl(providerApiUrl)
                .path(version)
                .path("/cost-code-tags")
                .build()
                .toUri();
        ResponseEntity<CostCodeTag> response;
        if (costCodeTag.getId() == null) {
            response = restTemplate.postForEntity(uri, costCodeTag, CostCodeTag.class);
        } else {
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            HttpEntity<CostCodeTag> entity = new HttpEntity<>(costCodeTag, headers);
            response = restTemplate.exchange(uri, HttpMethod.PUT, entity, CostCodeTag.class);
        }
        return response.getBody();
    }

    public CostCodeTagType saveTagType(CostCodeTagType costCodeTagType) {
        URI uri = UriComponentsBuilder.fromHttpUrl(providerApiUrl)
                .path(version)
                .path("/cost-code-tag-types")
                .build()
                .toUri();
        ResponseEntity<CostCodeTagType> response;
        if (costCodeTagType.getId() == null) {
            response = restTemplate.postForEntity(uri, costCodeTagType, CostCodeTagType.class);
        } else {
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            HttpEntity<CostCodeTagType> entity = new HttpEntity<>(costCodeTagType, headers);
            response = restTemplate.exchange(uri, HttpMethod.PUT, entity, CostCodeTagType.class);
        }
        return response.getBody();
    }

    public List<CostCodeTagType> getAllCostCodeTagsByProjectId(Long projectId) {
        URI uri = UriComponentsBuilder.fromHttpUrl(providerApiUrl)
                .path(version)
                .path("/cost-code-tag-types")
                .queryParam("projectId", projectId)
                .build()
                .toUri();
        CostCodeTagType[] result = restTemplate.getForObject(uri, CostCodeTagType[].class);
        if (null == result) return new ArrayList<>();
        return Arrays.stream(result).sorted().collect(Collectors.toList());
    }

    public List<CostCodeTag> findCostCodeTagsByTypeId(Long typeId) {
        URI uri = UriComponentsBuilder.fromHttpUrl(providerApiUrl)
                .path(version)
                .path("/cost-code-tags")
                .queryParam("typeId", typeId)
                .build()
                .toUri();
        CostCodeTag[] result = restTemplate.getForObject(uri, CostCodeTag[].class);
        if (null == result) return new ArrayList<>();
        return Arrays.stream(result).sorted().collect(Collectors.toList());

    }

    public String deleteCostCodeTagAssociationsByKeyInAndCostCodeType(Set<Long> keys, CostCodeType type) {
        URI uri = UriComponentsBuilder.fromHttpUrl(providerApiUrl)
                .path(version)
                .path("/cost-code-tag-associations")
                .queryParam("costCodeType", type)
                .build()
                .toUri();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<Set<Long>> entity = new HttpEntity<>(keys, headers);
        ResponseEntity<String> response = restTemplate.exchange(uri, HttpMethod.DELETE, entity, String.class);
        return response.getBody();
    }

    public String saveCostCodeTagAssociations(Set<CostCodeTagAssociation> costCodeTagAssociations) {
        URI uri = UriComponentsBuilder.fromHttpUrl(providerApiUrl)
                .path(version)
                .path("/cost-code-tag-association-collection")
                .build()
                .toUri();
        ResponseEntity<String> response;
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<Set<CostCodeTagAssociation>> entity = new HttpEntity<>(costCodeTagAssociations, headers);
        response = restTemplate.exchange(uri, HttpMethod.PATCH, entity, String.class);
        return response.getBody();
    }

    public List<CostCodeTagAssociation> getCostCodeTagAssociationsByKeysAndType(Set<Long> keys, CostCodeType type) {
        URI uri = UriComponentsBuilder.fromHttpUrl(providerApiUrl)
                .path(version)
                .path("/cost-code-tag-association-collection")
                .queryParam("costCodeType", type)
                .build()
                .toUri();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<Set<Long>> entity = new HttpEntity<>(keys, headers);
        ResponseEntity<CostCodeTagAssociation[]> response = restTemplate
                .exchange(uri, HttpMethod.POST, entity, new ParameterizedTypeReference<>() {
                });
        CostCodeTagAssociation[] result = response.getBody();
        if (null == result) return new ArrayList<>();
        return Arrays.stream(result).collect(Collectors.toList());
    }

    public Page<CraftDto> getCraftPage(PagingRequest pagingRequest, Long projectId) {
        URI uri = UriComponentsBuilder.fromHttpUrl(providerApiUrl)
                .path(version)
                .path("/craft-dtos")
                .queryParam("projectId", projectId)
                .build()
                .toUri();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<PagingRequest> entity = new HttpEntity<>(pagingRequest, headers);
        ResponseEntity<Page<CraftDto>> response = restTemplate
                .exchange(uri, HttpMethod.POST, entity, new ParameterizedTypeReference<>() {
                });
        return response.getBody();
    }

    public Craft getCraftById(Long id) {
        URI uri = UriComponentsBuilder.fromHttpUrl(providerApiUrl)
                .path(version)
                .path("/crafts/{id}")
                .buildAndExpand(id)
                .toUri();
        return restTemplate.getForObject(uri, Craft.class);
    }

    public Craft saveCraft(Craft craft) {
        URI uri = UriComponentsBuilder.fromHttpUrl(providerApiUrl)
                .path(version)
                .path("/crafts")
                .build()
                .toUri();
        ResponseEntity<Craft> response;
        if (craft.getId() == null) {
            response = restTemplate.postForEntity(uri, craft, Craft.class);
        } else {
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            HttpEntity<Craft> entity = new HttpEntity<>(craft, headers);
            response = restTemplate.exchange(uri, HttpMethod.PUT, entity, Craft.class);
        }
        return response.getBody();
    }

    public AllocationSubmissionDto getLastAllocationSubmissionDto(Long projectId, String teamName, LocalDate dateOfService) {
        URI uri = UriComponentsBuilder.fromHttpUrl(providerApiUrl)
                .path(version)
                .path("/last-allocation-submission-dtos")
                .queryParam("projectId", projectId)
                .queryParam("teamName", teamName)
                .queryParam("dateOfService", dateOfService)
                .build()
                .toUri();
        return restTemplate.getForObject(uri, AllocationSubmissionDto.class);
    }

    public String saveEmployeeTag(Long projectId, String empId, String tag) {
        URI uri = UriComponentsBuilder.fromHttpUrl(providerApiUrl)
                .path(version)
                .path("/employee-tags")
                .queryParam("projectId", projectId)
                .queryParam("empId", empId)
                .queryParam("tag", tag)
                .build()
                .toUri();
        ResponseEntity<String> response = restTemplate.postForEntity(uri, null, String.class);
        return response.getBody();
    }

    public Set<String> getEmployeeTagsByProjectId(Long projectId) {
        URI uri = UriComponentsBuilder.fromHttpUrl(providerApiUrl)
                .path(version)
                .path("/employee-tag-string-collection")
                .queryParam("projectId", projectId)
                .build()
                .toUri();
        String[] result = restTemplate.getForObject(uri, String[].class);
        if (result == null || result.length == 0) return new HashSet<>();
        return Arrays.stream(result).collect(Collectors.toSet());
    }

    public String deleteEmployeeTag(Long projectId, String empId, String tag) {
        URI uri = UriComponentsBuilder.fromHttpUrl(providerApiUrl)
                .path(version)
                .path("/employee-tags")
                .queryParam("projectId", projectId)
                .queryParam("empId", empId)
                .queryParam("tag", tag)
                .build()
                .toUri();
        ResponseEntity<String> response = restTemplate.exchange(uri, HttpMethod.DELETE, null, String.class);
        return response.getBody();
    }

    public String saveCostCodeLogTag(Long projectId, String costCode, String tag) {
        URI uri = UriComponentsBuilder.fromHttpUrl(providerApiUrl)
                .path(version)
                .path("/cost-code-log-tags")
                .queryParam("projectId", projectId)
                .queryParam("costCode", costCode)
                .queryParam("tag", tag)
                .build()
                .toUri();
        ResponseEntity<String> response = restTemplate.postForEntity(uri, null, String.class);
        return response.getBody();
    }

    public Set<String> getCostCodeLogTagStringsByProjectId(Long projectId) {
        URI uri = UriComponentsBuilder.fromHttpUrl(providerApiUrl)
                .path(version)
                .path("/cost-code-log-tag-string-collection")
                .queryParam("projectId", projectId)
                .build()
                .toUri();
        String[] result = restTemplate.getForObject(uri, String[].class);
        if (result == null || result.length == 0) return new HashSet<>();
        return Arrays.stream(result).collect(Collectors.toSet());
    }

    public List<CostCodeLogTag> getCostCodeLogTagsByProjectId(Long projectId) {
        URI uri = UriComponentsBuilder.fromHttpUrl(providerApiUrl)
                .path(version)
                .path("/cost-code-log-tag-collection")
                .queryParam("projectId", projectId)
                .build()
                .toUri();
        CostCodeLogTag[] result = restTemplate.getForObject(uri, CostCodeLogTag[].class);
        if (result == null || result.length == 0) return new ArrayList<>();
        return Arrays.stream(result).collect(Collectors.toList());
    }

    public String deleteCostCodeLogTag(Long projectId, String costCode, String tag) {
        URI uri = UriComponentsBuilder.fromHttpUrl(providerApiUrl)
                .path(version)
                .path("/cost-code-log-tags")
                .queryParam("projectId", projectId)
                .queryParam("costCode", costCode)
                .queryParam("tag", tag)
                .build()
                .toUri();
        ResponseEntity<String> response = restTemplate.exchange(uri, HttpMethod.DELETE, null, String.class);
        return response.getBody();
    }

    public List<EquipmentTag> findEquipmentTags() {
        URI uri = UriComponentsBuilder.fromHttpUrl(providerApiUrl)
                .path(version)
                .path("/equipment-tags")
                .build()
                .toUri();
        EquipmentTag[] result = restTemplate.getForObject(uri, EquipmentTag[].class);
        if (result == null || result.length == 0) return new ArrayList<>();
        return Arrays.stream(result).collect(Collectors.toList());
    }

    public String deleteEquipmentTag(String equipmentId, String tag) {
        URI uri = UriComponentsBuilder.fromHttpUrl(providerApiUrl)
                .path(version)
                .path("/equipment-tags")
                .queryParam("equipmentId", equipmentId)
                .queryParam("tag", tag)
                .build()
                .toUri();
        ResponseEntity<String> response = restTemplate.exchange(uri, HttpMethod.DELETE, null, String.class);
        return response.getBody();
    }

    public String saveEquipmentTag(String equipmentId, String tag) {
        URI uri = UriComponentsBuilder.fromHttpUrl(providerApiUrl)
                .path(version)
                .path("/equipment-tags")
                .queryParam("equipmentId", equipmentId)
                .queryParam("tag", tag)
                .build()
                .toUri();
        ResponseEntity<String> response = restTemplate.postForEntity(uri, null, String.class);
        return response.getBody();
    }

    // TM-275, total_hour is not function now in table time.equipment_cost_code_perc
    public void deleteEquipmentCostCodePercBatch(Set<Long> ids) {
        URI uri = UriComponentsBuilder.fromHttpUrl(providerApiUrl)
                .path(version)
                .path("/equipment-cost-code-perc-collection")
                .build()
                .toUri();
        List<EquipmentCostCodePerc> list = ids.stream().map(id -> {
            EquipmentCostCodePerc cc = new EquipmentCostCodePerc();
            cc.setId(id);
            return cc;
        }).collect(Collectors.toList());
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<List<EquipmentCostCodePerc>> entity = new HttpEntity<>(list, headers);
        restTemplate.exchange(uri, HttpMethod.DELETE, entity, Void.class);
    }

    public Shift getDefaultShiftByProjectId(Long projectId) {
        URI uri = UriComponentsBuilder.fromHttpUrl(providerApiUrl)
                .path(version)
                .path("/default-shifts")
                .queryParam("projectId", projectId)
                .build()
                .toUri();
        return restTemplate.getForObject(uri, Shift.class);
    }

    public Page<PurchaseOrderBillingDetailViewDto> getPurchaseOrderDetailHistory(PagingRequest pagingRequest, String purchaseOrderNumber) {
        URI uri = UriComponentsBuilder.fromHttpUrl(providerApiUrl)
                .path(version)
                .path("/purchase-order-billing-detail-view-dtos")
                .queryParam("purchaseOrderNumber", purchaseOrderNumber)
                .build()
                .toUri();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<PagingRequest> entity = new HttpEntity<>(pagingRequest, headers);
        ResponseEntity<Page<PurchaseOrderBillingDetailViewDto>> response = restTemplate
                .exchange(uri, HttpMethod.POST, entity, new ParameterizedTypeReference<>() {
                });
        return response.getBody();
    }

    public String deletePurchaseOrderBillingById(Long id) {
        URI uri = UriComponentsBuilder.fromHttpUrl(providerApiUrl)
                .path(version)
                .path("/purchase-order-billings/{id}")
                .buildAndExpand(id)
                .toUri();
        ResponseEntity<String> response = restTemplate.exchange(uri, HttpMethod.DELETE, null, String.class);
        return response.getBody();
    }

    public Set<Requisition> getRequisitions(String jobNumber, String subJob) {
        URI uri = UriComponentsBuilder.fromHttpUrl(providerApiUrl)
                .path(version)
                .path("/requisitions")
                .queryParam("jobNumber", jobNumber)
                .queryParam("subJob", subJob)
                .build()
                .toUri();
        Requisition[] result = restTemplate.getForObject(uri, Requisition[].class);
        if (null == result) return new HashSet<>();
        return Arrays.stream(result).collect(Collectors.toSet());
    }

    public Page<PerDiem> getPerDiemPage(PagingRequest pagingRequest, Long projectId) {
        URI uri = UriComponentsBuilder.fromHttpUrl(providerApiUrl)
                .path(version)
                .path("/per-diem-list")
                .queryParam("projectId", projectId)
                .build()
                .toUri();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<PagingRequest> entity = new HttpEntity<>(pagingRequest, headers);
        ResponseEntity<Page<PerDiem>> response = restTemplate
                .exchange(uri, HttpMethod.POST, entity, new ParameterizedTypeReference<>() {
                });
        return response.getBody();
    }

    public PerDiem findPerDiemById(Long id) {
        URI uri = UriComponentsBuilder.fromHttpUrl(providerApiUrl)
                .path(version)
                .path("/per-diems/{id}")
                .buildAndExpand(id)
                .toUri();
        return restTemplate.getForObject(uri, PerDiem.class);
    }

    public PerDiem savePerDiem(PerDiem perDiem) {
        URI uri = UriComponentsBuilder.fromHttpUrl(providerApiUrl)
                .path(version)
                .path("/per-diems")
                .build()
                .toUri();
        ResponseEntity<PerDiem> response;
        if (perDiem.getId() == null) {
            response = restTemplate.postForEntity(uri, perDiem, PerDiem.class);
        } else {
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            HttpEntity<PerDiem> entity = new HttpEntity<>(perDiem, headers);
            response = restTemplate.exchange(uri, HttpMethod.PUT, entity, PerDiem.class);
        }
        return response.getBody();
    }

    public String deletePerDiemById(Long id) {
        URI uri = UriComponentsBuilder.fromHttpUrl(providerApiUrl)
                .path(version)
                .path("/per-diems/{id}")
                .buildAndExpand(id)
                .toUri();
        ResponseEntity<String> response = restTemplate.exchange(uri, HttpMethod.DELETE, null, String.class);
        return response.getBody();
    }

    public List<PerDiem> getPerDiemByProjectId(Long projectId) {
        URI uri = UriComponentsBuilder.fromHttpUrl(providerApiUrl)
                .path(version)
                .path("/per-diems")
                .queryParam("projectId", projectId)
                .build()
                .toUri();
        PerDiem[] perDiems = restTemplate.getForObject(uri, PerDiem[].class);
        if (perDiems == null) return new ArrayList<>();
        return Arrays.stream(perDiems).collect(Collectors.toList());
    }

    public Rule getRuleById(Long id) {
        URI uri = UriComponentsBuilder.fromHttpUrl(providerApiUrl)
                .path(version)
                .path("/rules/{id}")
                .buildAndExpand(id)
                .toUri();
        return restTemplate.getForObject(uri, Rule.class);
    }

    public PurchaseOrderGroup savePurchaseOrderGroupComment(String purchaseOrderNumber, String comment) {
        URI uri = UriComponentsBuilder.fromHttpUrl(providerApiUrl)
                .path(version)
                .path("/purchase-order-groups/{po}/comment")
                .queryParam("comment", comment)
                .buildAndExpand(purchaseOrderNumber)
                .toUri();
        ResponseEntity<PurchaseOrderGroup> response = restTemplate.postForEntity(uri, null, PurchaseOrderGroup.class);
        return response.getBody();
    }

    public PurchaseOrderGroup savePurchaseOrderGroupFlag(String purchaseOrderNumber, boolean flag) {
        URI uri = UriComponentsBuilder.fromHttpUrl(providerApiUrl)
                .path(version)
                .path("/purchase-order-groups/{po}/flag")
                .queryParam("flag", flag)
                .buildAndExpand(purchaseOrderNumber)
                .toUri();
        ResponseEntity<PurchaseOrderGroup> response = restTemplate.postForEntity(uri, null, PurchaseOrderGroup.class);
        return response.getBody();
    }

    public List<Craft> getCraftsByProjectIdAndDateRange(Long projectId, LocalDate startDate, LocalDate endDate) {
        URI uri = UriComponentsBuilder.fromHttpUrl(providerApiUrl)
                .path(version)
                .path("/crafts")
                .queryParam("projectId", projectId)
                .queryParam("startDate", startDate)
                .queryParam("endDate", endDate)
                .build()
                .toUri();
        Craft[] result = restTemplate.getForObject(uri, Craft[].class);
        if (null == result) return new ArrayList<>();
        return Arrays.stream(result).collect(Collectors.toList());

    }

    public List<ActivityCode> getActivityCode() {
        URI uri = UriComponentsBuilder.fromHttpUrl(providerApiUrl)
                .path(version)
                .path("/activity-codes")
                .build()
                .toUri();
        ActivityCode[] result = restTemplate.getForObject(uri, ActivityCode[].class);
        if (null == result) return new ArrayList<>();
        return Arrays.stream(result).collect(Collectors.toList());
    }

    public ActivityCode saveActivityCode(ActivityCode activateCode) {
        URI uri = UriComponentsBuilder.fromHttpUrl(providerApiUrl)
                .path(version)
                .path("/activity-codes")
                .build()
                .toUri();
        ResponseEntity<ActivityCode> response;
        if (activateCode.getId() == null) {
            response = restTemplate.postForEntity(uri, activateCode, ActivityCode.class);
        } else {
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            HttpEntity<ActivityCode> entity = new HttpEntity<>(activateCode, headers);
            response = restTemplate.exchange(uri, HttpMethod.PUT, entity, ActivityCode.class);
        }
        return response.getBody();
    }

    // TM-412
    public int countEmployeeByProjectId(Long projectId) {
        URI uri = UriComponentsBuilder.fromHttpUrl(providerApiUrl)
                .path(version)
                .path("/employees/count")
                .queryParam("projectId", projectId)
                .build()
                .toUri();
        return Optional.ofNullable(restTemplate.getForObject(uri, Integer.class)).orElse(0);
    }

    public Page<PurchaseVendorRoster> getPurchaseVendorRoster(PagingRequest pagingRequest) {
        URI uri = UriComponentsBuilder.fromHttpUrl(providerApiUrl)
                .path(version)
                .path("/purchase-vendor-roster-list")
                .build()
                .toUri();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<PagingRequest> entity = new HttpEntity<>(pagingRequest, headers);
        ResponseEntity<Page<PurchaseVendorRoster>> response = restTemplate
                .exchange(uri, HttpMethod.POST, entity, new ParameterizedTypeReference<>() {
                });
        return response.getBody();
    }

    public List<RPCUserPool> getUserPools() {
        URI uri = UriComponentsBuilder.fromHttpUrl(providerApiUrl)
                .path(version)
                .path("/userPools")
                .build()
                .toUri();
        RPCUserPool[] result = restTemplate.getForObject(uri, RPCUserPool[].class);
        if (null == result) { return new ArrayList<>(); }
        return Arrays.stream(result).collect(Collectors.toList());
    }
}

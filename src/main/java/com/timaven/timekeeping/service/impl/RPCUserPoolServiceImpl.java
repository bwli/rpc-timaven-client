package com.timaven.timekeeping.service.impl;

import com.timaven.timekeeping.api.provider.ProviderAPI;
import com.timaven.timekeeping.model.RPCUserPool;
import com.timaven.timekeeping.service.RPCUserPoolService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RPCUserPoolServiceImpl implements RPCUserPoolService {
    private final ProviderAPI providerAPI;

    @Autowired
    public RPCUserPoolServiceImpl(ProviderAPI providerAPI) {
        this.providerAPI = providerAPI;
    }

    @Override
    public List<RPCUserPool> getUserPools() {
        return providerAPI.getUserPools();
    }
}

package com.timaven.timekeeping.service.impl;

import com.timaven.timekeeping.api.provider.ProviderAPI;
import com.timaven.timekeeping.model.Craft;
import com.timaven.timekeeping.model.dto.CraftDto;
import com.timaven.timekeeping.model.paging.Page;
import com.timaven.timekeeping.model.paging.PagingRequest;
import com.timaven.timekeeping.service.CraftService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CraftServiceImpl implements CraftService {

    private final ProviderAPI providerAPI;

    @Autowired
    public CraftServiceImpl(ProviderAPI providerAPI) {
        this.providerAPI = providerAPI;
    }

    @Override
    public Page<CraftDto> getCraftPage(PagingRequest pagingRequest, Long projectId) {
        return providerAPI.getCraftPage(pagingRequest, projectId);
    }

    @Override
    public Craft findById(Long id) {
        return providerAPI.getCraftById(id);
    }

    @Override
    public void saveCraft(Craft craft) {
        providerAPI.saveCraft(craft);
    }

    @Override
    public void updateCraftPerDiem(Long craftId, Long perDiemId) {
        Craft craft = providerAPI.getCraftById(craftId);
        craft.setPerDiemId(perDiemId);
        providerAPI.saveCraft(craft);
    }
}

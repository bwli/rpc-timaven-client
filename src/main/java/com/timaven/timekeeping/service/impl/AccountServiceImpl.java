package com.timaven.timekeeping.service.impl;

import com.timaven.timekeeping.api.provider.ProviderAPI;
import com.timaven.timekeeping.model.Role;
import com.timaven.timekeeping.model.User;
import com.timaven.timekeeping.model.UserRole;
import com.timaven.timekeeping.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;
import java.util.Set;

@Service
public class AccountServiceImpl implements AccountService {

    private final ProviderAPI providerAPI;

    @Autowired
    public AccountServiceImpl(ProviderAPI providerAPI) {
        this.providerAPI = providerAPI;
    }

    @Override
    public Set<User> getAdminUsers() {
        return providerAPI.getUsersByProjectIdAndRoles(null, Set.of("ROLE_admin", "ROLE_it_admin"));
    }

    @Override
    public Role getAdminRole() {
        return providerAPI.getRoleByName("ROLE_admin");
    }

    @Override
    public User saveUser(User user) {
        return providerAPI.saveUser(user);
    }

    @Override
    public User getUserById(Long userId) {
        return providerAPI.getUserById(userId);
    }

    @Override
    public UserRole saveUserRole(UserRole userRole) {
        return providerAPI.saveUserRole(userRole);
    }

    @Override
    public void changeUserValidity(Long id, boolean active) {
        providerAPI.changeUserValidity(id, active);
    }

    @Override
    public User loadUserByUsername(String username) {
        return providerAPI.getUserByUsername(username);
    }

    @Override
    public User loadUserByUsername(String username, Collection<String> includes) {
        return providerAPI.getUserByUsername(username, includes);
    }

    @Override
    public Set<User> getProjectManagerUsers() {
        return providerAPI.getUsersByProjectIdAndRoles(null, Set.of("ROLE_project_manager"));
    }

    @Override
    public Set<User> getProjectManagerUsersWithRoles(String currentUsername, Long projectId) {
        return providerAPI.getUsersByProjectIdAndRoles(projectId,
                Set.of("ROLE_project_manager",
                        "ROLE_accountant_foreman",
                        "ROLE_accountant",
                        "ROLE_timekeeper",
                        "ROLE_purchasing",
                        "ROLE_foreman"), false, true);
    }

    @Override
    public Set<Role> getRolesForManager() {
        return providerAPI.getRolesByNames(Set.of("ROLE_accountant_foreman", "ROLE_timekeeper",
                "ROLE_purchasing", "ROLE_foreman"));
    }

    @Override
    public User addUser(User user, Long projectId) {
        return providerAPI.addUser(user, projectId);
    }

    @Override
    public Set<Role> getRolesForAdmin() {
        return providerAPI.getRolesByNames(Set.of("ROLE_project_manager", "ROLE_corporate_accountant", "ROLE_corporate_timekeeper"));
    }

    @Override
    public Set<User> getUsersForAdmin() {
        return providerAPI.getUsersByProjectIdAndRoles(null, Set.of("ROLE_project_manager", "ROLE_corporate_accountant", "ROLE_corporate_timekeeper"));
    }

    @Override
    public Set<User> getAdminManagerUsersByProjectId(Long projectId) {
        return providerAPI.getUsersByProjectIdAndRoles(projectId, Set.of("ROLE_admin", "ROLE_project_manager"));
    }

    @Override
    public List<User> getUsersForProject() {
        return providerAPI.getUsers(null, Set.of("ROLE_super_admin", "ROLE_admin"), Set.of("roles"));
    }

    @Override
    public Set<Role> getRolesForSuperAdmin() {
        return providerAPI.getRolesByNames(Set.of("ROLE_admin", "ROLE_it_admin"));
    }
}

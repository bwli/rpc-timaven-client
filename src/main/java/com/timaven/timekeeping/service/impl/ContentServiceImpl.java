package com.timaven.timekeeping.service.impl;

import com.timaven.timekeeping.api.provider.ProviderAPI;
import com.timaven.timekeeping.api.provider.WebClientAPI;
import com.timaven.timekeeping.model.*;
import com.timaven.timekeeping.model.dto.*;
import com.timaven.timekeeping.model.enums.AllocationSubmissionStatus;
import com.timaven.timekeeping.service.ContentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class ContentServiceImpl implements ContentService {

    private final ProviderAPI providerAPI;
    private final WebClientAPI webClientAPI;

    @Autowired
    public ContentServiceImpl(ProviderAPI providerAPI, WebClientAPI webClientAPI) {
        this.providerAPI = providerAPI;
        this.webClientAPI = webClientAPI;
    }

    @Override
    public TransSubmission getTodayTransSubmission(Long projectId) {
        List<TransSubmission> transSubmissionList = providerAPI.getTransSubmissions(projectId, LocalDate.now(), null, null);
        return CollectionUtils.isEmpty(transSubmissionList) ? null : transSubmissionList.get(0);
    }

    @Override
    public void saveAllocationSubmission(AllocationSubmission allocationSubmission) {
        providerAPI.saveAllocationSubmission(allocationSubmission);
    }

    @Override
    public AllocationSubmissionDto getAllocationSubmissionDtoById(Long allocationSubmissionId) {
        return providerAPI.getAllocationSubmissionDtoById(allocationSubmissionId);
    }

    @Override
    public Set<String> getTeamNames(Long projectId, LocalDate startDate) {
        return getTeamNames(projectId, startDate, null);
    }

    @Override
    public Set<String> getTeamNames(Long projectId, LocalDate startDate, LocalDate endDate) {
        EmployeeFilter employeeFilter = new EmployeeFilter(projectId, null, null, null, null, startDate, endDate);
        return webClientAPI.getTeamNames(employeeFilter).defaultIfEmpty(new HashSet<>()).block();
    }

    @Override
    public void saveReports(String teamName, List<ReportRecord> reportRecords, Long projectId) {
        providerAPI.saveReports(reportRecords, teamName, projectId);
    }

    @Override
    public void saveTransSubmission(TransSubmission transSubmission) {
        providerAPI.saveTransSubmission(transSubmission);
    }

    @Override
    public Set<String> getTeamNamesByUserIdAndProjectId(Long userId, Long projectId) {
        return providerAPI.getUserTeams(userId, projectId).stream().map(UserTeam::getTeam).collect(Collectors.toSet());
    }

    @Override
    public void saveUserTeam(User user, Long projectId) {
        providerAPI.saveUserTeams(user.getId(), projectId, user.getTeams());
    }

    @Override
    public Set<AllocationTime> getAllocationTimesForThisWeek(Long projectId, LocalDate startOfWeek,
                                                             LocalDate endOfWeek, Set<String> employeeIds) {
        return providerAPI.getAllocationTimes(startOfWeek, endOfWeek, null, null, projectId, AllocationSubmissionStatus.APPROVED, null, null, employeeIds);
    }

    @Override
    public void saveAllocationTimes(Set<AllocationTime> allocationTimes) {
        providerAPI.saveAllocationTimes(allocationTimes);
    }

    @Override
    public List<AllocationTime> getAllocationTimesById(Long projectId, Set<Long> allocationTimeIds, Set<String> includes) {
        return providerAPI.getAllocationTimesById(projectId, allocationTimeIds, includes);
    }

    @Override
    public Set<AllocationTime> getAllocationTimesByProjectIdAndDates(Long projectId, LocalDate weekStartDate,
                                                                     LocalDate weekEndDate) {
        return providerAPI.getAllocationTimes(weekStartDate, weekEndDate, weekEndDate, weekEndDate, projectId, AllocationSubmissionStatus.APPROVED, null, List.of("crafts", "perDiemCostCodeTagAssociations"), null);
    }

    @Override
    public Set<AllocationTime> getTimeLeftOffAllocationTimesByProjectIdAndDates(Long projectId,
                                                                                LocalDate weekStartDate,
                                                                                LocalDate weekEndDate) {
        return providerAPI.getAllocationTimes(null, weekStartDate.minusDays(1), weekEndDate, weekEndDate, projectId, AllocationSubmissionStatus.APPROVED, null, List.of("crafts", "perDiemCostCodeTagAssociations"), null);
    }

    @Override
    public void saveAllocationSubmissions(Set<AllocationSubmission> allocationSubmissions) {
        providerAPI.saveAllocationSubmissions(allocationSubmissions);
    }

    @Override
    public AllocationSubmission getAllocationSubmissionById(Long allocationSubmissionId) {
        return providerAPI.getAllocationSubmissionById(allocationSubmissionId);
    }

    @Override
    public void deleteAllocationSubmissionById(Long allocationSubmissionId) {
        providerAPI.deleteAllocationSubmissionById(allocationSubmissionId);
    }

    @Override
    public void deleteStageTransactionsByTeamAndDateOfService(String team, LocalDate dateOfService, Long projectId) {
        providerAPI.deleteStageTransactionsByTeamAndDateOfService(team, dateOfService, projectId);
    }

    @Override
    public AllocationSubmissionDto getAllocationSubmissionDtoByDateOfService(Long projectId, String teamName, LocalDate dateOfService) {
        return providerAPI.getAllocationSubmissionDtoByDateOfService(projectId, teamName, dateOfService);
    }

    @Override
    public Set<CostCodeTagDto> getDefaultCostCodeTagDtos(AllocationSubmissionDto allocationSubmissionDto) {
        Set<CostCodeTagDto> result = new TreeSet<>();
        if (allocationSubmissionDto == null || CollectionUtils.isEmpty(allocationSubmissionDto.getAllocationTimes())) return result;
        for (AllocationTimeDto allocationTimeDto : allocationSubmissionDto.getAllocationTimes()) {
            Set<CostCodePercDto> costCodePercDtos = allocationTimeDto.getCostCodePercs();
            if (!CollectionUtils.isEmpty(costCodePercDtos)) {
                for (CostCodePercDto costCodePercDto : costCodePercDtos) {
                    CostCodeTagDto costCodeTagDto = new CostCodeTagDto();
                    costCodeTagDto.setCostCodeFull(costCodePercDto.getCostCodeFull());
                    costCodeTagDto.setDisplayOrder(costCodePercDto.getDisplayOrder());
                    costCodeTagDto.setCostCodeTagAssociationDtos(costCodePercDto.getCostCodeTagAssociationDtos());
                    result.add(costCodeTagDto);
                }
            }
        }
        return result;
    }

    @Override
    public AllocationSubmissionDto getLastAllocationSubmissionDto(Long projectId, String teamName, LocalDate dateOfService) {
        return providerAPI.getLastAllocationSubmissionDto(projectId, teamName, dateOfService);
    }

    @Override
    public List<StageTransaction> getStageTransactions(Long projectId, String teamName, LocalDate startDate, LocalDate endDate, List<String> includes) {
        return providerAPI.getStageTransactions(projectId, teamName, startDate, endDate, includes);
    }

    @Override
    public Set<AllocationSubmission> getAllocationSubmissions(LocalDate startDate, LocalDate endDate, Long projectId, String teamName, AllocationSubmissionStatus status, AllocationSubmissionStatus notStatus, List<String> includes) {
        return providerAPI.getAllocationSubmissions(startDate, endDate, projectId, teamName, status, notStatus, includes);
    }

    @Override
    public Set<String> getSignInSheets(Long projectId, LocalDate dateOfService) {
        EmployeeFilter filter = new EmployeeFilter(projectId, null, null, null, null, dateOfService, null);
        return webClientAPI.getSignInSheets(filter).defaultIfEmpty(new HashSet<>()).block();
    }

    @Override
    public void deleteAllocationTimeById(Long id) {
        webClientAPI.deleteAllocationTimeById(id);
    }

    @Override
    public void deleteStageTransactionsByProjectIdEmpIdAndTeamAndDateOfService(Long projectId, String empId, String team, LocalDate dateOfService) {
        webClientAPI.deleteStageTransactionsByProjectIdAndEmpIdAndTeamAndDateOfService(projectId, empId, team, dateOfService);
    }

    @Override
    public Set<AllocationTime> getAllocationTimesByEmpIdsAndProjectIdAndDates(Set<String> empIds, Long projectId,
                                                                              Set<LocalDate> dates,
                                                                              LocalDate weekEndDate) {
        if (CollectionUtils.isEmpty(empIds) || CollectionUtils.isEmpty(dates)) return new HashSet<>();
        AllocationTimeFilter filter = new AllocationTimeFilter(dates, null, null, weekEndDate, weekEndDate, false, projectId, null, AllocationSubmissionStatus.APPROVED, null, List.of("crafts"), empIds);
        return providerAPI.getAllocationTimes(filter);
    }

    @Override
    public List<AllocationSubmission> getApprovedAllocationSubmissionsBetween(LocalDate startOfWeek,
                                                                              LocalDate endOfWeek, Long projectId) {
        return new ArrayList<>(providerAPI.getAllocationSubmissions(startOfWeek, endOfWeek, projectId,
                AllocationSubmissionStatus.APPROVED, null, List.of("allocationTimes", "crafts")));
    }

    @Override
    public Map<String, BigDecimal> getToDateHours(Long projectId) {
        Set<AllocationTime> allocationTimes = providerAPI.getAllocationTimes(null, null, null, null, projectId, AllocationSubmissionStatus.APPROVED, null, null, null);

        Map<String, BigDecimal> ccHoursMap = allocationTimes.stream()
                .map(AllocationTime::getCostCodePercs)
                .filter(ccp -> !CollectionUtils.isEmpty(ccp))
                .flatMap(Collection::stream)
                .collect(Collectors.groupingBy(CostCodePerc::getCostCodeFull,
                        Collectors.reducing(BigDecimal.ZERO, CostCodePerc::getTotalHour, BigDecimal::add)));

        Set<String> perDiemCostCodes = allocationTimes.stream()
                .filter(AllocationTime::isHasPerDiem)
                .map(AllocationTime::getPerDiemCostCode)
                .filter(StringUtils::hasText)
                .collect(Collectors.toSet());

        Set<String> mobCostCodes = allocationTimes.stream()
                .map(AllocationTime::getMobCostCode)
                .filter(StringUtils::hasText)
                .collect(Collectors.toSet());
        for (String perDiemCostCode : perDiemCostCodes) {
            ccHoursMap.putIfAbsent(perDiemCostCode, BigDecimal.ZERO);
        }
        for (String mobCostCode : mobCostCodes) {
            ccHoursMap.putIfAbsent(mobCostCode, BigDecimal.ZERO);
        }

        return ccHoursMap;
    }
}

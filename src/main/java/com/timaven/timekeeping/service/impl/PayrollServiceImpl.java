package com.timaven.timekeeping.service.impl;

import com.timaven.timekeeping.api.provider.ProviderAPI;
import com.timaven.timekeeping.model.dto.ProjectPayrollDto;
import com.timaven.timekeeping.model.paging.Page;
import com.timaven.timekeeping.model.paging.PagingRequest;
import com.timaven.timekeeping.service.PayrollService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PayrollServiceImpl implements PayrollService {

    private final ProviderAPI providerAPI;

    @Autowired
    public PayrollServiceImpl(ProviderAPI providerAPI) {
        this.providerAPI = providerAPI;
    }

    @Override
    public Page<ProjectPayrollDto> getProjectPayrollPage(Long projectId, PagingRequest pagingRequest) {
        return providerAPI.getProjectPayrollPage(projectId, pagingRequest);
    }
}

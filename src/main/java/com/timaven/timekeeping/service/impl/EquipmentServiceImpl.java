package com.timaven.timekeeping.service.impl;

import com.timaven.timekeeping.api.provider.ProviderAPI;
import com.timaven.timekeeping.model.*;
import com.timaven.timekeeping.model.dto.EquipmentAllocationTimeFilter;
import com.timaven.timekeeping.model.dto.EquipmentPriceDto;
import com.timaven.timekeeping.model.enums.EquipmentOwnershipType;
import com.timaven.timekeeping.model.enums.EquipmentWeeklyProcessType;
import com.timaven.timekeeping.model.paging.Page;
import com.timaven.timekeeping.model.paging.PagingRequest;
import com.timaven.timekeeping.service.EquipmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;
import java.util.Set;

@Service
public class EquipmentServiceImpl implements EquipmentService {

    private final ProviderAPI providerAPI;

    @Autowired
    public EquipmentServiceImpl(ProviderAPI providerAPI) {
        this.providerAPI = providerAPI;
    }

    @Override
    public List<EquipmentUsage> getCurrentEquipmentUsages() {
        return providerAPI.getEquipmentUsageByIdsAndDates(null, null, LocalDate.now(), null);
    }

    @Override
    public Equipment getEquipmentById(Long id) {
        return providerAPI.getEquipmentById(id);
    }

    @Override
    public Equipment getEquipmentByEquipmentId(String equipmentId) {
        return providerAPI.getEquipmentByEquipmentId(equipmentId);
    }

    @Override
    public Equipment saveEquipment(Equipment equipment) {
        return providerAPI.saveEquipment(equipment);
    }

    @Override
    public void changeEquipmentValidity(Long id, boolean active) {
        providerAPI.changeEquipmentValidity(id, active);
    }

    @Override
    public void saveEquipmentUsage(Long equipmentId, Long userId, Long projectId, Long id, LocalDate date,
                                   String type) {
        providerAPI.saveEquipmentUsage(equipmentId, projectId, id, date, type);
    }

    @Override
    public void deactivateEquipmentUsage(Long id) {
        providerAPI.deactivateEquipmentUsage(id);
    }

    @Override
    public List<EquipmentUsage> getEquipmentUsagesByProjectIdAndDates(Long projectId, LocalDate weekStartDate,
                                                                      LocalDate weekEndDate) {
        return providerAPI.getEquipmentUsageByIdsAndDates(null, projectId, weekStartDate, weekEndDate);
    }

    @Override
    public Set<EquipmentAllocationTime> getEquipmentAllocationTimesByProjectIdAndDates(Long projectId,
                                                                                       LocalDate weekStartDate,
                                                                                       LocalDate weekEndDate) {
        EquipmentAllocationTimeFilter filter = new EquipmentAllocationTimeFilter.EquipmentAllocationTimeFilterBuilder()
                .startDate(weekStartDate)
                .endDate(weekEndDate)
                .payrollStartDate(weekEndDate)
                .payrollEndDate(weekEndDate)
                .projectId(projectId)
                .build();
        return providerAPI.getEquipmentAllocationTimes(filter);
    }

    @Override
    public Set<EquipmentAllocationTime> getEquipmentAllocationTimesByProjectIdAndDates(Long projectId, LocalDate startDate, LocalDate endDate, LocalDate payrollStartDate, LocalDate payrollEndDate) {
        EquipmentAllocationTimeFilter filter = new EquipmentAllocationTimeFilter.EquipmentAllocationTimeFilterBuilder()
                .startDate(startDate)
                .endDate(endDate)
                .payrollStartDate(payrollStartDate)
                .payrollEndDate(payrollEndDate)
                .projectId(projectId)
                .build();
        return providerAPI.getEquipmentAllocationTimes(filter);
    }

    @Override
    public Set<EquipmentAllocationTime> getTimeLeftOffEquipmentAllocationTimesByProjectIdAndDates(Long projectId,
                                                                                                  LocalDate weekStartDate,
                                                                                                  LocalDate weekEndDate) {
        EquipmentAllocationTimeFilter filter = new EquipmentAllocationTimeFilter.EquipmentAllocationTimeFilterBuilder()
                .endDate(weekStartDate.minusDays(1))
                .payrollStartDate(weekEndDate)
                .payrollEndDate(weekEndDate)
                .projectId(projectId)
                .build();
        return providerAPI.getEquipmentAllocationTimes(filter);
    }

    @Override
    public Set<EquipmentAllocationTime> getEquipmentAllocationTimesByIds(Set<Long> ids) {
        return providerAPI.getEquipmentAllocationTimesById(ids);
    }

    @Override
    public void saveEquipmentAllocationSubmissions(Set<EquipmentAllocationSubmission> submissions) {
        providerAPI.saveEquipmentAllocationSubmissions(submissions);
    }

    @Override
    public EquipmentWeeklyProcess saveEquipmentWeeklyProcess(EquipmentWeeklyProcess weeklyProcess) {
        return providerAPI.saveEquipmentWeeklyProcess(weeklyProcess);
    }

    @Override
    public List<Equipment> getAllEquipmentByOwnershipType(EquipmentOwnershipType ownershipType) {
        return providerAPI.getEquipmentByOwnershipType(ownershipType);
    }

    @Override
    public Page<EquipmentPriceDto> getEquipmentPricesByProjectId(Long projectId, PagingRequest pagingRequest) {
        return providerAPI.getEquipmentPricesByProjectId(projectId, pagingRequest);
    }

    @Override
    public EquipmentPrice getEquipmentPriceById(Long id) {
        return providerAPI.getEquipmentPriceById(id);
    }

    @Override
    public void saveEquipmentPrice(EquipmentPrice equipmentPrice) {
        providerAPI.saveEquipmentPrice(equipmentPrice);
    }

    @Override
    public void deleteEquipmentPriceById(Long id) {
        providerAPI.deleteEquipmentPriceById(id);
    }

    @Override
    public Set<Equipment> getRentalAndBeingUsedEquipments(Long projectId) {
        return providerAPI.getEquipmentLog(projectId);
    }

    @Override
    public Set<EquipmentPrice> getEquipmentPricesByProjectIdAndClasses(Long projectId, Set<String> equipmentClasses) {
        return providerAPI.getEquipmentPricesByProjectIdAndClasses(projectId, equipmentClasses);
    }

    @Override
    public EquipmentWeeklyProcess getEquipmentWeeklyProcessByProjectIdAndDateAndType(Long projectId, LocalDate weekEndDate, EquipmentWeeklyProcessType type) {
        return providerAPI.getEquipmentWeeklyProcessByProjectIdAndDateAndType(projectId, weekEndDate, type);
    }

    @Override
    public List<EquipmentTag> findEquipmentTags() {
        return providerAPI.findEquipmentTags();
    }

    @Override
    public void deleteEquipmentTag(String equipmentId, String tag) {
        providerAPI.deleteEquipmentTag(equipmentId, tag);
    }

    @Override
    public void saveEquipmentTag(String equipmentId, String tag) {
        providerAPI.saveEquipmentTag(equipmentId, tag);
    }

    // TM-275, total_hour is not function now in table time.equipment_cost_code_perc
    @Override
    public void deleteEquipmentCostCodePercBatch(Set<Long> ids) {
        providerAPI.deleteEquipmentCostCodePercBatch(ids);
    }
}

package com.timaven.timekeeping.service.impl;

import com.timaven.timekeeping.api.provider.ProviderAPI;
import com.timaven.timekeeping.model.*;
import com.timaven.timekeeping.model.dto.CostCodeBillingCodeFilter;
import com.timaven.timekeeping.model.dto.SaveCostCodeAssociationDto;
import com.timaven.timekeeping.service.ProjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class ProjectServiceImpl implements ProjectService {

    private final ProviderAPI providerAPI;

    @Autowired
    public ProjectServiceImpl(ProviderAPI providerAPI) {
        this.providerAPI = providerAPI;
    }

    @Override
    public List<Project> getAllProjectsFetchUserProjects() {
        return providerAPI.getAllProjects(List.of("userProjects"));
    }

    @Override
    public Project getProjectById(Long projectId) {
        return providerAPI.getProjectById(projectId);
    }

    @Override
    public void saveProject(Project project) {
        Long projectIdToBeDuplicated = project.getProjectId();
        Long projectId = project.getId();
        project = providerAPI.saveProject(project, project.getUserIds());
        if (projectIdToBeDuplicated != null) {
            // Duplicate project settings from other project
            Optional<Rule> ruleOpt = providerAPI.getRuleByProjectId(projectIdToBeDuplicated);
            Set<Shift> shifts = providerAPI.getShiftsByProjectId(projectIdToBeDuplicated);
            ProjectShift projectShift = providerAPI.getProjectShiftByProjectId(projectIdToBeDuplicated);
            Set<RainOut> rainOuts = providerAPI.getRainOuts(projectIdToBeDuplicated);
            List<CostCodeLog> costCodeLogs = providerAPI.getCostCodesByProjectIdAndDateOfService(projectIdToBeDuplicated, LocalDate.now(), null, false);
            Set<Long> costCodeLogIds = costCodeLogs.stream().map(CostCodeLog::getId).collect(Collectors.toSet());
            CostCodeBillingCodeFilter filter = new CostCodeBillingCodeFilter(costCodeLogIds, Set.of("costCodeLog"));
            List<CostCodeBillingCode> costCodeBillingCodes = providerAPI.getCostCodeBillingCodesByCostCodeIds(filter);
            Map<CostCodeLog, List<CostCodeBillingCode>> ccbcMap = costCodeBillingCodes.stream()
                    .collect(Collectors.groupingBy(CostCodeBillingCode::getCostCodeLog));
            List<BillingCodeOverride> overrides = providerAPI.findBillingCodeOverridesByProjectId(projectIdToBeDuplicated);
            List<CostCodeLogTag> costCodeLogTags = providerAPI.getCostCodeLogTagsByProjectId(projectIdToBeDuplicated);
            List<PerDiem> perDiems = providerAPI.getPerDiemByProjectId(projectIdToBeDuplicated);

            String defaultShiftName = null;
            if (projectShift != null) {
                Long defaultShiftId = projectShift.getShiftId();
                defaultShiftName = shifts.stream().filter(s -> s.getId().equals(defaultShiftId)).map(Shift::getName).findFirst().orElse(null);
            }

            Long finalProjectId = project.getId();
            ruleOpt.ifPresent(rule -> {
                rule.setId(null);
                rule.setProjectId(finalProjectId);
            });

            for (PerDiem perDiem : perDiems) {
                Long perDiemId = perDiem.getId();
                perDiem.setId(null);
                perDiem.setProjectId(finalProjectId);
                perDiem = providerAPI.savePerDiem(perDiem);
                if (ruleOpt.isPresent() && ruleOpt.get().getPerDiemId() != null && ruleOpt.get().getPerDiemId().equals(perDiemId)) {
                    ruleOpt.get().setPerDiemId(perDiem.getId());
                }
            }

            ruleOpt.ifPresent(providerAPI::saveRule);

            shifts.forEach(s -> {
                s.setId(null);
                s.setProjectId(finalProjectId);
            });

            for (Shift shift : shifts) {
                shift = providerAPI.saveShift(shift);
                if (defaultShiftName != null && defaultShiftName.equalsIgnoreCase(shift.getName())) {
                    projectShift = new ProjectShift();
                    projectShift.setProjectId(finalProjectId);
                    projectShift.setShiftId(shift.getId());
                    providerAPI.saveProjectShift(projectShift);
                }
            }
            rainOuts.forEach(r -> {
                if (r.getProjectId() == null) return;
                r.setId(null);
                r.setProjectId(finalProjectId);
                providerAPI.saveRainOut(r);
            });

            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM/dd/yyyy");

            for (CostCodeLog costCodeLog : costCodeLogs) {

                List<CostCodeBillingCode> costCodeBillingCodeList = ccbcMap.getOrDefault(costCodeLog, new ArrayList<>());

                SaveCostCodeAssociationDto dto = new SaveCostCodeAssociationDto();

                dto.setCostCode(costCodeLog.getCostCodeFull());
                dto.setDescription(costCodeLog.getDescription());
                if (costCodeLog.getStartDate() != null) {
                    dto.setStartDate(formatter.format(costCodeLog.getStartDate()));
                }
                if (costCodeLog.getEndDate() != null) {
                    dto.setEndDate(formatter.format(costCodeLog.getEndDate()));
                }

                List<Long> billingCodeIds = costCodeBillingCodeList.stream()
                        .map(CostCodeBillingCode::getBillingCodeId)
                        .collect(Collectors.toList());
                dto.setBillingCodeIds(billingCodeIds);

                providerAPI.saveCostCodeBillingCode(finalProjectId, dto);
            }

            overrides.forEach(o -> {
                o.setId(null);
                o.setProjectId(finalProjectId);
                providerAPI.saveBillingCodeOverride(o);
            });

            costCodeLogTags.forEach(t -> {
                providerAPI.saveCostCodeLogTag(finalProjectId, t.getCostCodeFull(), t.getTag());
            });
        } else if (projectId == null) {
            // Generate default rule and shift
            projectId = project.getId();
            Rule rule = new Rule();
            rule.setDescription("Default rule");
            rule.setRigPayThreshold(999);
            rule.setProjectId(projectId);
            providerAPI.saveRule(rule);
            Shift shift = new Shift();
            shift.setProjectId(projectId);
            shift.setName("DAY");
            shift.setScheduleStart(LocalTime.of(6, 0));
            shift.setScheduleEnd(LocalTime.of(17, 0));
            shift.setBreakStart(LocalTime.of(12, 0));
            shift.setBreakEnd(LocalTime.of(12, 30));
            shift.setPaidBreakDuration(0);
            shift = providerAPI.saveShift(shift);
            ProjectShift projectShift = new ProjectShift();
            projectShift.setProjectId(projectId);
            projectShift.setShiftId(shift.getId());
            providerAPI.saveProjectShift(projectShift);
        }
    }

    @Override
    public void deleteProject(Long id) {
        providerAPI.deleteProject(id);
    }

    @Override
    public Project getProjectByJobNumberAndSubJob(String jobNumber, String subJob) {
        return providerAPI.getProjectBySubJob(jobNumber, subJob);
    }

    @Override
    public List<Project> getAllProjects() {
        return providerAPI.getAllProjects();
    }
}

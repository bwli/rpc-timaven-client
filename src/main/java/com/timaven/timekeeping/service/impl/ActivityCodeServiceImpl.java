package com.timaven.timekeeping.service.impl;

import com.timaven.timekeeping.api.provider.ProviderAPI;
import com.timaven.timekeeping.model.ActivityCode;
import com.timaven.timekeeping.service.ActivityCodeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ActivityCodeServiceImpl implements ActivityCodeService {

    private final ProviderAPI providerAPI;

    @Autowired
    public ActivityCodeServiceImpl(ProviderAPI providerAPI) {
        this.providerAPI = providerAPI;
    }


    @Override
    public List<ActivityCode> getActivity() {
        return providerAPI.getActivityCode();
    }

    @Override
    public ActivityCode saveActivityCode(ActivityCode activateCode) {
        return providerAPI.saveActivityCode(activateCode);
    }
}

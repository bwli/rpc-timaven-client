package com.timaven.timekeeping.service.impl;

import com.timaven.timekeeping.api.provider.ProviderAPI;
import com.timaven.timekeeping.model.PerDiem;
import com.timaven.timekeeping.model.paging.Page;
import com.timaven.timekeeping.model.paging.PagingRequest;
import com.timaven.timekeeping.service.PerDiemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class PerDiemServiceImpl implements PerDiemService {

    private final ProviderAPI providerAPI;

    @Autowired
    public PerDiemServiceImpl(ProviderAPI providerAPI) {
        this.providerAPI = providerAPI;
    }

    @Override
    public Page<PerDiem> getPerDiemPage(PagingRequest pagingRequest, Long projectId) {
        return providerAPI.getPerDiemPage(pagingRequest, projectId);
    }

    @Override
    public PerDiem findById(Long id) {
        return providerAPI.findPerDiemById(id);
    }

    @Override
    public void savePerDiem(PerDiem perDiem) {
        providerAPI.savePerDiem(perDiem);
    }

    @Override
    public void deleteById(Long id) {
        providerAPI.deletePerDiemById(id);
    }

    @Override
    public List<PerDiem> getPerDiems(Long projectId) {
        return providerAPI.getPerDiemByProjectId(projectId);
    }
}

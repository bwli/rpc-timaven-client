package com.timaven.timekeeping.service.impl;

import com.timaven.timekeeping.api.provider.ProviderAPI;
import com.timaven.timekeeping.model.Notification;
import com.timaven.timekeeping.service.NotificationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;

@Service
public class NotificationServiceImpl implements NotificationService {

    private final ProviderAPI providerAPI;

    @Autowired
    public NotificationServiceImpl(ProviderAPI providerAPI) {
        this.providerAPI = providerAPI;
    }

    @Override
    public List<Notification> getNotificationsByUserId() {
        return providerAPI.getNotificationsByUserId();
    }

    @Override
    public void updateNotificationRead(Long id, boolean read) {
        providerAPI.updateNotificationRead(id, read);
    }

    @Override
    public int getUnreadNotificationCountByUserId(Long id) {
        return providerAPI.getNotificationCountByUserIdAndRead(false);
    }

    @Override
    public List<Notification> saveNotifications(List<Notification> notifications) {
        if (CollectionUtils.isEmpty(notifications)) return new ArrayList<>();
        return providerAPI.saveNotifications(notifications);
    }
}

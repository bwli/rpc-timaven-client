package com.timaven.timekeeping.service.impl;

import com.timaven.timekeeping.api.provider.ProviderAPI;
import com.timaven.timekeeping.model.BillingCode;
import com.timaven.timekeeping.model.BillingCodeOverride;
import com.timaven.timekeeping.model.BillingCodeType;
import com.timaven.timekeeping.model.dto.BillingCodeDto;
import com.timaven.timekeeping.model.dto.SaveCostCodeAssociationDto;
import com.timaven.timekeeping.model.paging.Page;
import com.timaven.timekeeping.model.paging.PagingRequest;
import com.timaven.timekeeping.service.BillingCodeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BillingCodeServiceImpl implements BillingCodeService {

    private final ProviderAPI providerAPI;

    @Autowired
    public BillingCodeServiceImpl(ProviderAPI providerAPI) {
        this.providerAPI = providerAPI;
    }

    @Override
    public List<BillingCodeType> findAllBillingCodeTypes() {
        return providerAPI.getBillingCodeTypes(false);
    }

    @Override
    public BillingCodeType findBillingCodeTypeById(Long id) {
        return providerAPI.findBillingCodeTypeById(id);
    }

    @Override
    public Page<BillingCodeDto> findBillingCodesByTypeId(Long typeId, PagingRequest pagingRequest, Long projectId) {
        return providerAPI.findBillingCodesByTypeId(typeId, pagingRequest, projectId);
    }

    @Override
    public BillingCode findBillingCodeById(Long billingCodeId) {
        return providerAPI.findBillingCodeById(billingCodeId);
    }

    @Override
    public void deleteBillingCodeTypeById(Long id) {
        providerAPI.deleteBillingCodeTypeById(id);
    }

    @Override
    public void deleteBillingCodeById(Long id) {
        providerAPI.deleteBillingCodeById(id);
    }

    @Override
    public void saveBillingCodeType(BillingCodeType billingCodeType) {
        providerAPI.saveBillingCodeType(billingCodeType);
    }

    public List<BillingCodeDto> findBillingCodeByCostCodeIdAndProjectId(Long id, Long projectId) {
        return providerAPI.getBillingCodesByCostCodeIdAndProjectId(id, projectId);
    }

    @Override
    public List<BillingCodeDto> findBillingCodesByTypeId(Long id, Long projectId) {
        return providerAPI.getBillingCodesByTypeIdAndProjectId(id, projectId);
    }

    @Override
    public void saveCostCodeBillingCode(Long projectId, SaveCostCodeAssociationDto dto) {
        providerAPI.saveCostCodeBillingCode(projectId, dto);
    }

    @Override
    public List<BillingCodeType> findBillingCodeTypesSortByLevel() {
        return providerAPI.getBillingCodeTypes(true);
    }

    @Override
    public void saveCostCodeStructure(List<Long> ids) {
        providerAPI.saveCostCodeStructure(ids);
    }

    @Override
    public BillingCodeOverride findBillingCodeOverride(Long billingCodeId, Long projectId) {
        return providerAPI.findBillingCodeOverrideByBillingCodeIdAndProjectId(billingCodeId, projectId);
    }

    @Override
    public void saveBillingCodeOverride(BillingCodeOverride billingCodeOverride) {
        providerAPI.saveBillingCodeOverride(billingCodeOverride);
    }

    @Override
    public void deleteBillingCodeOverrideByBillingCodeIdAndProjectId(Long id, Long projectId) {
        providerAPI.deleteBillingCodeOverrideByBillingCodeIdAndProjectId(id, projectId);
    }

    @Override
    public void saveBillingCode(BillingCode billingCode) {
        providerAPI.saveBillingCode(billingCode);
    }

    @Override
    public BillingCode findBillingCodeByTypeIdAndCodeName(Long typeId, String codeName) {
        return providerAPI.findBillingCodeByTypeIdAndCodeName(typeId, codeName);
    }
}

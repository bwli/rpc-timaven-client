package com.timaven.timekeeping.service.impl;

import com.timaven.timekeeping.api.provider.ProviderAPI;
import com.timaven.timekeeping.model.Project;
import com.timaven.timekeeping.model.paging.Page;
import com.timaven.timekeeping.model.paging.PagingRequest;
import com.timaven.timekeeping.service.UserProjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Set;


@Service
public class UserProjectServiceImpl implements UserProjectService {

    private final ProviderAPI providerAPI;

    @Autowired
    public UserProjectServiceImpl(ProviderAPI providerAPI) {
        this.providerAPI = providerAPI;
    }

    @Override
    public Set<Project> getProjectsByUsername(String principal) {
        return providerAPI.getProjectsByUsername(principal);
    }

    @Override
    public Page<Project> getProjectsByUsername(String principal, PagingRequest pagingRequest, boolean isAll) {
        return providerAPI.getProjectsByUsername(pagingRequest, principal, isAll);
    }
}

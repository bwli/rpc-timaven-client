package com.timaven.timekeeping.service.impl;

import com.timaven.timekeeping.api.provider.ProviderAPI;
import com.timaven.timekeeping.model.Rule;
import com.timaven.timekeeping.service.RuleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;


@Service
public class RuleServiceImpl implements RuleService {

    private final ProviderAPI providerAPI;

    @Autowired
    public RuleServiceImpl(ProviderAPI providerAPI) {
        this.providerAPI = providerAPI;
    }

    @Override
    public void saveRule(Rule rule) {
        providerAPI.saveRule(rule);
    }

    @Override
    public Optional<Rule> getRule(Long projectId) {
        return providerAPI.getRuleByProjectId(projectId);
    }

    @Override
    public Rule updateRulePerDiem(Long ruleId, Long perDiemId) {
        Rule rule = providerAPI.getRuleById(ruleId);
        rule.setPerDiemId(perDiemId);
        return providerAPI.saveRule(rule);
    }
}

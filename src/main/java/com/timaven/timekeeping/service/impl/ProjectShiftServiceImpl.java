package com.timaven.timekeeping.service.impl;

import com.timaven.timekeeping.api.provider.ProviderAPI;
import com.timaven.timekeeping.model.ProjectShift;
import com.timaven.timekeeping.service.ProjectShiftService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class ProjectShiftServiceImpl implements ProjectShiftService {

    private final ProviderAPI providerAPI;

    @Autowired
    public ProjectShiftServiceImpl(ProviderAPI providerAPI) {
        this.providerAPI = providerAPI;
    }

    @Override
    public void saveProjectShift(ProjectShift projectShift) {
        providerAPI.saveProjectShift(projectShift);
    }

    @Override
    public void upsertProjectShift(ProjectShift projectShift) {
        providerAPI.upsertProjectShift(projectShift);
    }

    @Override
    public Long getShiftIdByProjectId(Long projectId) {
        ProjectShift projectShift = providerAPI.getProjectShiftByProjectId(projectId);
        return projectShift == null ? null : projectShift.getShiftId();
    }
}

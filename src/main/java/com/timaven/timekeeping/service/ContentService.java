package com.timaven.timekeeping.service;

import com.timaven.timekeeping.model.*;
import com.timaven.timekeeping.model.dto.AllocationSubmissionDto;
import com.timaven.timekeeping.model.dto.CostCodeTagDto;
import com.timaven.timekeeping.model.enums.AllocationSubmissionStatus;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;
import java.util.Map;
import java.util.Set;

public interface ContentService {

    TransSubmission getTodayTransSubmission(Long projectId);

    void saveAllocationSubmission(AllocationSubmission allocationSubmission);

    AllocationSubmissionDto getAllocationSubmissionDtoById(Long allocationSubmissionId);

    Set<String> getTeamNames(Long projectId, LocalDate startDate);

    Set<String> getTeamNames(Long projectId, LocalDate startDate, LocalDate endDate);

    void saveReports(String teamName, List<ReportRecord> reportRecords, Long projectId);

    void saveTransSubmission(TransSubmission transSubmission);

    Set<String> getTeamNamesByUserIdAndProjectId(Long userId, Long projectId);

    void saveUserTeam(User user, Long projectId);

    Set<AllocationTime> getAllocationTimesForThisWeek(Long projectId, LocalDate startOfWeek, LocalDate endOfWeek,
                                                      Set<String> employeeIds);

    void saveAllocationTimes(Set<AllocationTime> allocationTimes);

    List<AllocationTime> getAllocationTimesById(Long projectId, Set<Long> allocationTimeIds, Set<String> includes);


    Set<AllocationTime> getAllocationTimesByProjectIdAndDates(Long projectId, LocalDate weekStartDate,
                                                              LocalDate weekEndDate);

    Set<AllocationTime> getTimeLeftOffAllocationTimesByProjectIdAndDates(Long projectId, LocalDate weekStartDate,
                                                                         LocalDate weekEndDate);

    Set<AllocationTime> getAllocationTimesByEmpIdsAndProjectIdAndDates(Set<String> empIds, Long projectId, Set<LocalDate> dates, LocalDate weekEndDate);

    List<AllocationSubmission> getApprovedAllocationSubmissionsBetween(LocalDate startOfWeek, LocalDate endOfWeek, Long projectId);

    Map<String, BigDecimal> getToDateHours(Long projectId);

    void saveAllocationSubmissions(Set<AllocationSubmission> allocationSubmissions);

    AllocationSubmission getAllocationSubmissionById(Long allocationSubmissionId);

    void deleteAllocationSubmissionById(Long allocationSubmissionId);

    void deleteStageTransactionsByTeamAndDateOfService(String team, LocalDate dateOfService, Long projectId);

    AllocationSubmissionDto getAllocationSubmissionDtoByDateOfService(Long projectId, String teamName, LocalDate dateOfService);

    Set<CostCodeTagDto> getDefaultCostCodeTagDtos(AllocationSubmissionDto allocationSubmissionDto);

    AllocationSubmissionDto getLastAllocationSubmissionDto(Long projectId, String teamName, LocalDate dateOfService);

    List<StageTransaction> getStageTransactions(Long projectId, String teamName, LocalDate startDate, LocalDate endDate, List<String> includes);

    Set<AllocationSubmission> getAllocationSubmissions(LocalDate startDate, LocalDate endDate, Long projectId, String teamName, AllocationSubmissionStatus status, AllocationSubmissionStatus notStatus, List<String> includes);

    Set<String> getSignInSheets(Long projectId, LocalDate dateOfService);

    void deleteAllocationTimeById(Long id);

    void deleteStageTransactionsByProjectIdEmpIdAndTeamAndDateOfService(Long projectId, String empId, String team, LocalDate dateOfService);
}

package com.timaven.timekeeping.service;

import com.timaven.timekeeping.model.Project;
import com.timaven.timekeeping.model.PurchaseOrderBilling;
import com.timaven.timekeeping.model.PurchaseVendorRoster;
import com.timaven.timekeeping.model.RequisitionGroup;
import com.timaven.timekeeping.model.dto.*;
import com.timaven.timekeeping.model.paging.Page;
import com.timaven.timekeeping.model.paging.PagingRequest;

import java.util.Set;

public interface PurchaseOrderService {
    Page<PurchaseOrderSummaryDto> getLatestPurchaseOrderSummaries(PagingRequest pagingRequest, Long projectId);

    Page<PurchaseOrderDetailDto> getLatestPurchaseOrderDetails(PagingRequest pagingRequest, String purchaseOrderNumber);

    RequisitionDto getRequisitionDto(String requisitionNumber);

    void deleteByRequisitionNumber(String requisitionNumber);

    void saveRequisition(RequisitionDto requisitionDto, Long projectId);

    Page<RequisitionGroup> getRequisitionGroups(PagingRequest pagingRequest, Long projectId);

    Set<String> getDistinctLatestPurchaseOrderNumbers();

    PurchaseOrderBilling savePurchaseOrderBilling(PurchaseOrderBillingDto dto);

    Page<PurchaseOrderBillingDetailViewDto> getPurchaseOrderDetailHistory(PagingRequest pagingRequest, String purchaseOrderNumber);

    void deletePurchaseOrderBillingById(Long id);

    String generateRequisitionNumber(Project project);

    void updatePurchaseOrderGroupComment(String purchaseOrderNumber, String comment);

    void updatePurchaseOrderGroupFlag(String purchaseOrderNumber, boolean flag);

    Page<PurchaseVendorRoster> getPurchaseVendorRoster(PagingRequest pagingRequest);
}

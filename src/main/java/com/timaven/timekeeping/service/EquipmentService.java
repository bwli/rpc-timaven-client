package com.timaven.timekeeping.service;


import com.timaven.timekeeping.model.*;
import com.timaven.timekeeping.model.dto.EquipmentPriceDto;
import com.timaven.timekeeping.model.enums.EquipmentOwnershipType;
import com.timaven.timekeeping.model.enums.EquipmentWeeklyProcessType;
import com.timaven.timekeeping.model.paging.Page;
import com.timaven.timekeeping.model.paging.PagingRequest;

import java.time.LocalDate;
import java.util.List;
import java.util.Set;

public interface EquipmentService {
    List<EquipmentUsage> getCurrentEquipmentUsages();

    Equipment getEquipmentById(Long id);

    Equipment getEquipmentByEquipmentId(String equipmentId);

    Equipment saveEquipment(Equipment equipment);

    void changeEquipmentValidity(Long id, boolean active);

    void saveEquipmentUsage(Long equipmentId, Long userId, Long projectId, Long id, LocalDate date, String type);

    void deactivateEquipmentUsage(Long id);

    List<EquipmentUsage> getEquipmentUsagesByProjectIdAndDates(Long projectId, LocalDate weekStartDate, LocalDate weekEndDate);

    Set<EquipmentAllocationTime> getEquipmentAllocationTimesByProjectIdAndDates(Long projectId, LocalDate weekStartDate, LocalDate weekEndDate);

    Set<EquipmentAllocationTime> getEquipmentAllocationTimesByProjectIdAndDates(Long projectId, LocalDate startDate, LocalDate endDate, LocalDate payrollStartDate, LocalDate payrollEndDate);

    Set<EquipmentAllocationTime> getTimeLeftOffEquipmentAllocationTimesByProjectIdAndDates(Long projectId, LocalDate weekStartDate, LocalDate weekEndDate);

    Set<EquipmentAllocationTime> getEquipmentAllocationTimesByIds(Set<Long> ids);

    void saveEquipmentAllocationSubmissions(Set<EquipmentAllocationSubmission> submissions);

    EquipmentWeeklyProcess saveEquipmentWeeklyProcess(EquipmentWeeklyProcess weeklyProcess);

    List<Equipment> getAllEquipmentByOwnershipType(EquipmentOwnershipType ownershipType);

    Page<EquipmentPriceDto> getEquipmentPricesByProjectId(Long projectId, PagingRequest pagingRequest);

    EquipmentPrice getEquipmentPriceById(Long id);

    void saveEquipmentPrice(EquipmentPrice equipmentPrice);

    void deleteEquipmentPriceById(Long id);

    Set<Equipment> getRentalAndBeingUsedEquipments(Long projectId);

    Set<EquipmentPrice> getEquipmentPricesByProjectIdAndClasses(Long projectId, Set<String> equipmentClasses);

    EquipmentWeeklyProcess getEquipmentWeeklyProcessByProjectIdAndDateAndType(Long projectId, LocalDate weekEndDate, EquipmentWeeklyProcessType type);

    List<EquipmentTag> findEquipmentTags();

    void deleteEquipmentTag(String equipmentId, String tag);

    void saveEquipmentTag(String equipmentId, String tag);

    // TM-275, total_hour is not function now in table time.equipment_cost_code_perc
    void deleteEquipmentCostCodePercBatch(Set<Long> ids);
}

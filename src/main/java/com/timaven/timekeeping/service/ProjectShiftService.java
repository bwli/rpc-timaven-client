package com.timaven.timekeeping.service;

import com.timaven.timekeeping.model.ProjectShift;


public interface ProjectShiftService {
    void saveProjectShift(ProjectShift projectShift);

    void upsertProjectShift(ProjectShift projectShift);

    Long getShiftIdByProjectId(Long projectId);
}

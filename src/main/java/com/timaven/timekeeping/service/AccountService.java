package com.timaven.timekeeping.service;


import com.timaven.timekeeping.model.Role;
import com.timaven.timekeeping.model.User;
import com.timaven.timekeeping.model.UserRole;

import java.util.Collection;
import java.util.List;
import java.util.Set;

public interface AccountService {

    Set<User> getAdminUsers();

    Role getAdminRole();

    User saveUser(User user);

    User getUserById(Long userId);

    UserRole saveUserRole(UserRole userRole);

    void changeUserValidity(Long id, boolean active);

    User loadUserByUsername(String username);

    User loadUserByUsername(String username, Collection<String> includes);

    Set<User> getProjectManagerUsers();

    Set<User> getProjectManagerUsersWithRoles(String currentUsername, Long projectId);

    Set<Role> getRolesForManager();

    User addUser(User user, Long projectId);

    Set<Role> getRolesForAdmin();

    Set<User> getUsersForAdmin();

    Set<User> getAdminManagerUsersByProjectId(Long projectId);

    List<User> getUsersForProject();

    Set<Role> getRolesForSuperAdmin();
}

package com.timaven.timekeeping.service;

import com.timaven.timekeeping.model.Rule;

import java.util.Optional;

public interface RuleService {
    Optional<Rule> getRule(Long projectId);

    void saveRule(Rule rule);

    Rule updateRulePerDiem(Long ruleId, Long perDiemId);
}

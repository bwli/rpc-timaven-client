package com.timaven.timekeeping.service;

import com.timaven.timekeeping.model.*;
import com.timaven.timekeeping.model.dto.CostCodeAssociationDto;
import com.timaven.timekeeping.model.enums.CostCodeType;
import com.timaven.timekeeping.model.paging.Page;
import com.timaven.timekeeping.model.paging.PagingRequest;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;
import java.util.Set;

public interface CostCodeService {

    void deleteById(Long id);

    Page<CostCodeAssociationDto> findCostCodeAssociations(PagingRequest pagingRequest, Long projectId);

    Set<CostCodeLog> getCostCodeLogsByTeamNameAndDateOfService(String teamName, Long projectId,
                                                               LocalDate dateOfService);

    List<CostCodeLog> getAllCostCodeLogsByProjectIdAndDateOfService(Long projectId, LocalDate dateOfService);

    List<CostCodeLog> getAllCostCodeLogsByProjectIdAndDateRange(Long projectId, List<LocalDate> days);

    List<CostCodeLog> getCostCodesByCodesAndProjectIdAndDate(Long projectId, List<String> costCodes,
                                                             LocalDate dateOfService);

    CostCodeLog getCostCodeById(Long id);

    List<CostCodeBillingCode> getCostCodeBillingCodes(Set<Long> costCodeIds);

    Map<String, String> findCostCodeColumns(Long projectId);

    void lockCostCodes(Set<String> costCodes, Long projectId);

    Page<CostCodeTagType> getCostCodeTagTypePage(Long projectId, PagingRequest pagingRequest);

    CostCodeTagType getCostCodeTagTypeById(Long id);

    CostCodeTag getCostCodeTagById(Long id);

    Page<CostCodeTag> getCostCodeTagPage(Long typeId, PagingRequest pagingRequest);

    void deleteCostCodeTagTypeById(Long id);

    void deleteCostCodeTagById(Long id);

    void saveTagCode(CostCodeTag costCodeTag);

    void saveTagType(CostCodeTagType costCodeTagType);

    List<CostCodeTagType> getAllCostCodeTagTypesByProjectId(Long projectId);

    List<CostCodeTag> findCostCodeTagsByTypeId(Long typeId);

    void deleteCostCodeTagAssociationsByKeyInAndCostCodeType(Set<Long> keys, CostCodeType type);

    void saveCostCodeTagAssociations(Set<CostCodeTagAssociation> costCodeTagAssociations);

    List<CostCodeTagAssociation> getCostCodeTagAssociationsByKeysAndType(Set<Long> keys, CostCodeType type);

    void saveCostCodeLogTag(Long projectId, String costCode, String tag);

    void deleteCostCodeLogTag(Long projectId, String costCode, String tag);

    Set<String> getTagsByProjectId(Long projectId);

    List<CostCodeTagType> findAllCostCodeTagTypes();
}

package com.timaven.timekeeping.service;

import com.timaven.timekeeping.model.PerDiem;
import com.timaven.timekeeping.model.paging.Page;
import com.timaven.timekeeping.model.paging.PagingRequest;

import java.util.List;

public interface PerDiemService {
    Page<PerDiem> getPerDiemPage(PagingRequest pagingRequest, Long projectId);

    PerDiem findById(Long id);

    void savePerDiem(PerDiem perDiem);

    void deleteById(Long id);

    List<PerDiem> getPerDiems(Long projectId);
}

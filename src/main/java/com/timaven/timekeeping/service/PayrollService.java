package com.timaven.timekeeping.service;

import com.timaven.timekeeping.model.dto.ProjectPayrollDto;
import com.timaven.timekeeping.model.paging.Page;
import com.timaven.timekeeping.model.paging.PagingRequest;

public interface PayrollService {
    Page<ProjectPayrollDto> getProjectPayrollPage(Long projectId, PagingRequest pagingRequest);
}

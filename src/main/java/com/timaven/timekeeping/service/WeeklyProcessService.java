package com.timaven.timekeeping.service;


import com.timaven.timekeeping.model.AllocationTime;
import com.timaven.timekeeping.model.Project;
import com.timaven.timekeeping.model.Rule;
import com.timaven.timekeeping.model.WeeklyProcess;
import com.timaven.timekeeping.model.dto.ProjectsDashboardDto;
import com.timaven.timekeeping.model.dto.ProjectsDashboardProfitDto;
import com.timaven.timekeeping.model.dto.WeeklyPayrollDto;
import com.timaven.timekeeping.model.dto.WeeklyPayrollStatusDto;
import com.timaven.timekeeping.model.enums.WeeklyProcessStatus;
import com.timaven.timekeeping.model.enums.WeeklyProcessType;

import java.time.LocalDate;
import java.util.Collection;
import java.util.List;
import java.util.Set;

public interface WeeklyProcessService {
    WeeklyProcess findByProjectIdAndDateAndType(Long projectId, LocalDate weekEndDate, WeeklyProcessType type);

    WeeklyProcess save(WeeklyProcess weeklyProcess);

    List<WeeklyPayrollDto> getWeeklyPayrollDtos(LocalDate weekEndDate);

    void approveWeeklyTimesheets(Long userId, LocalDate weekEndDate, Long projectId, int type);

    void calculateCost(Long projectId, String teamName, LocalDate weekEndDate, WeeklyProcessType type);

    void getCorporateDashboard(List<ProjectsDashboardDto> projectsDashboardDtos,
                               List<ProjectsDashboardProfitDto> projectsDashboardProfitDtos);

    void getCorporateDashboard(List<ProjectsDashboardDto> projectsDashboardDtos,
                               List<ProjectsDashboardProfitDto> projectsDashboardProfitDtos,
                               List<Project> projectList);

    void getProjectsDashboard(List<ProjectsDashboardDto> projectsDashboardDtos, Set<Project> projects);

    WeeklyProcess findByDateAndTypeFetching(LocalDate weekEndDate, String teamName, WeeklyProcessType type);

    WeeklyProcess findWeeklyProcessByIdFetching(Long id);

    void calculateCost(WeeklyProcess weeklyProcess, Set<AllocationTime> allocationTimes);

    void updatePerDiemTaxable(Long weeklyProcessId, boolean taxablePerDiem);

    WeeklyProcess findOrGenerateWeeklyProcess(Long projectId, LocalDate weekEndDate, WeeklyProcessType type);

    WeeklyProcess findOrGenerateWeeklyProcess(Long projectId, LocalDate weekEndDate, WeeklyProcessType type, String teamName);

    WeeklyProcess findOrGenerateWeeklyProcess(Long projectId, String teamName, LocalDate weekEndDate, WeeklyProcessType type);

    void updateWeeklyPayrollComment(Long id, String comment);

    void addPerDiemAllocationTimes(Long projectId, LocalDate weekEndDate, Rule rule);

    void updateWeeklyProcesses(Collection<Long> ids, WeeklyProcessStatus status);
}

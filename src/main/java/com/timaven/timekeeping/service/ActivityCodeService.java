package com.timaven.timekeeping.service;



import com.timaven.timekeeping.model.ActivityCode;

import java.util.List;

public interface ActivityCodeService {

    List<ActivityCode> getActivity();

    ActivityCode saveActivityCode(ActivityCode activateCode);

}

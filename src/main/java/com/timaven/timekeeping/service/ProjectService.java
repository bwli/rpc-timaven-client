package com.timaven.timekeeping.service;

import com.timaven.timekeeping.model.Project;

import java.util.List;

public interface ProjectService {
    List<Project> getAllProjectsFetchUserProjects();

    Project getProjectById(Long projectId);

    void saveProject(Project project);

    void deleteProject(Long id);

    Project getProjectByJobNumberAndSubJob(String jobNumber, String subJob);

    List<Project> getAllProjects();
}

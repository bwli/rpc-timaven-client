package com.timaven.timekeeping.service;


import com.timaven.timekeeping.model.dto.ReconcileDto;

import java.time.LocalDate;

public interface ReconcileService {
    ReconcileDto getReconcileData(LocalDate weekEndDate);
}

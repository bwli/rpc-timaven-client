package com.timaven.timekeeping.service;

import com.timaven.timekeeping.model.Shift;

import java.util.Set;

public interface ShiftService {
    Set<Shift> getShiftsByProjectId(Long projectId);

    Shift getShiftById(Long shiftId);

    void saveShift(Shift shift);

    void deleteShiftById(Long id);

    Set<String> getAllShiftNamesByProjectId(Long projectId);

    Shift getDefaultShiftByProjectId(Long projectId);

    Shift getShiftByProjectIdAndName(Long projectId, String name);
}

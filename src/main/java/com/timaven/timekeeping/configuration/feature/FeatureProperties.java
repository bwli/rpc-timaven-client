package com.timaven.timekeeping.configuration.feature;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Getter
@Setter
@Component
@ConfigurationProperties(prefix = "features")
public class FeatureProperties {
    private Map<String, Boolean> flags = new HashMap<>();


}

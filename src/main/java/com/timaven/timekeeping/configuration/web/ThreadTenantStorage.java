package com.timaven.timekeeping.configuration.web;

import com.timaven.timekeeping.configuration.feature.FeatureProperties;
import org.springframework.util.CollectionUtils;

public class ThreadTenantStorage {

    private static ThreadLocal<String> currentTenant = new ThreadLocal<>();

    private static ThreadLocal<FeatureProperties> featureTenant = new ThreadLocal<>();

    public static void setTenantId(String tenantId) {
        currentTenant.set(tenantId);
    }

    public static String getTenantId() {
        return currentTenant.get();
    }

    public static void clear(){
        currentTenant.remove();
        featureTenant.remove();
    }

    public static void setFeature(FeatureProperties featureProperties) {
        featureTenant.set(featureProperties);
    }

    public static FeatureProperties getFeature(FeatureProperties defaultFeature) {
        if (featureTenant.get() == null) {
            return defaultFeature;
        }
        FeatureProperties properties = featureTenant.get();
        if (defaultFeature != null && !CollectionUtils.isEmpty(defaultFeature.getFlags())) {
            defaultFeature.getFlags().forEach((key, value) -> properties.getFlags().putIfAbsent(key, value));
        }
        return properties;
    }
}

package com.timaven.timekeeping.configuration.web;

import com.timaven.timekeeping.configuration.tenant.TenantProperties;
import org.springframework.stereotype.Component;
import org.springframework.ui.ModelMap;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.context.request.WebRequestInterceptor;

@Component
public class HeaderTenantInterceptor implements WebRequestInterceptor {

    private final TenantProperties tenantProperties;

    public HeaderTenantInterceptor(TenantProperties tenantProperties) {
        this.tenantProperties = tenantProperties;
    }

    @Override
    public void preHandle(WebRequest request) throws Exception {
        String tenantId = tenantProperties.getHosts().get(request.getHeader("Host"));
        ThreadTenantStorage.setTenantId(tenantId);
        ThreadTenantStorage.setFeature(tenantProperties.getFeatures().get(tenantId));
    }

    @Override
    public void postHandle(WebRequest request, ModelMap model) throws Exception {
        ThreadTenantStorage.clear();
    }

    @Override
    public void afterCompletion(WebRequest request, Exception ex) throws Exception {

    }
}

package com.timaven.timekeeping.configuration.web;


import com.timaven.timekeeping.security.TenantGuardHandlerInterceptor;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class WebConfiguration implements WebMvcConfigurer {

    private final HeaderTenantInterceptor headerTenantInterceptor;
    private final TenantGuardHandlerInterceptor tenantGuardHandlerInterceptor;

    public WebConfiguration(HeaderTenantInterceptor headerTenantInterceptor,
                            TenantGuardHandlerInterceptor tenantGuardHandlerInterceptor) {
        this.headerTenantInterceptor = headerTenantInterceptor;
        this.tenantGuardHandlerInterceptor = tenantGuardHandlerInterceptor;
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addWebRequestInterceptor(headerTenantInterceptor);
        registry.addInterceptor(tenantGuardHandlerInterceptor);
    }
}

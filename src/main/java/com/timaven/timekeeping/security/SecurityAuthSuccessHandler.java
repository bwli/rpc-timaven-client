package com.timaven.timekeeping.security;

import com.timaven.timekeeping.model.dto.Principal;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
public class SecurityAuthSuccessHandler implements AuthenticationSuccessHandler {

    @Override
    public void onAuthenticationSuccess(HttpServletRequest httpServletRequest,
                                        HttpServletResponse httpServletResponse, Authentication authentication) throws IOException, ServletException {
        Principal principal = (Principal) authentication.getPrincipal();
        httpServletRequest.getSession().setAttribute("tenant_id", principal.getTenantId());
        httpServletResponse.sendRedirect(httpServletRequest.getContextPath().concat("/home"));
    }
}

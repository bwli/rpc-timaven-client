package com.timaven.timekeeping.security.endpoint;

import com.timaven.timekeeping.security.common.WebUtil;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.LoginUrlAuthenticationEntryPoint;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class AjaxAwareAuthenticationEntryPoint extends
        LoginUrlAuthenticationEntryPoint {

    public AjaxAwareAuthenticationEntryPoint(String loginFormUrl) {
        super(loginFormUrl);
    }

    @Override
    public void commence(HttpServletRequest request, HttpServletResponse
            response, AuthenticationException authException) throws
            IOException, ServletException {
        if (WebUtil.isAjax(request)) {
            response.sendError(HttpStatus.UNAUTHORIZED.value(), "Unauthorized");
        } else {
            super.commence(request, response, authException);
        }
    }
}

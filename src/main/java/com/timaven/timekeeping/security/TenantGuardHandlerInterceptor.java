package com.timaven.timekeeping.security;

import com.timaven.timekeeping.configuration.tenant.TenantProperties;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@Component
public class TenantGuardHandlerInterceptor implements HandlerInterceptor {

    private final TenantProperties tenantProperties;
    private final Environment env;

    public TenantGuardHandlerInterceptor(TenantProperties tenantProperties, Environment env) {
        this.tenantProperties = tenantProperties;
        this.env = env;
    }

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        String tenantId = tenantProperties.getHosts().get(request.getHeader("Host"));
        HttpSession session = request.getSession();
        Object sessionTenantIdObj = session.getAttribute("tenant_id");
        if (sessionTenantIdObj != null) {
            String sessionTenantId = (String) sessionTenantIdObj;
            if (!tenantId.equalsIgnoreCase(sessionTenantId)) {
//                session.invalidate();
//                String xcrfToken = "";
//                for (Cookie cookie : request.getCookies()) {
//                    String cookieName = cookie.getName();
//                    Cookie cookieToDelete = new Cookie(cookieName, null);
//                    if ("XSRF-TOKEN".equals(cookieName)) xcrfToken = cookie.getValue();
//                    if ("access_token".equals(cookieName)) {
//                        cookieToDelete.setMaxAge(0);
//                        response.addCookie(cookieToDelete);
//                    }
//                }
//                request.getHeaders("cookie");
//                String logoutUri = env.getProperty("timaven-client.client.logoutUri");
//                assert logoutUri != null;
//                HttpHeaders headers = new HttpHeaders();
//                RestTemplate restTemplate = new RestTemplate();
//                headers.set("Cookie", session.getId());
//                HttpEntity<String> entity = new HttpEntity<>("parameters", headers);
//                restTemplate.exchange(logoutUri, HttpMethod.POST, entity, String.class);
//                headers.add("X-XSRF-TOKEN", xcrfToken);
//                headers.set("Cookie", String.join(";", Collections.list(request.getHeaders("cookie"))));
//                entity = new HttpEntity<>("parameters", headers);
//                restTemplate.exchange(absoluteContextRootURL + "/logout", HttpMethod.POST, entity, String.class);
//                response.setStatus(HttpServletResponse.SC_SEE_OTHER);
                String absoluteContextRootURL = request.getRequestURL().toString().replace(request.getRequestURI().substring(1), request.getContextPath());
                UriComponents uriComponents = UriComponentsBuilder.fromHttpUrl(absoluteContextRootURL + "/silence-logout").build();
                session.removeAttribute("tenant_id");
//                response.setHeader(HttpHeaders.LOCATION, uriComponents.toUriString());
//                throw new AccessDeniedException("Cross tenant");
                response.sendRedirect(uriComponents.toUriString());
                return false;
            }
        }
        return true;
    }
}

package com.timaven.timekeeping.controller;

import com.timaven.timekeeping.model.Project;
import com.timaven.timekeeping.model.dto.ProjectsDashboardDto;
import com.timaven.timekeeping.model.dto.ProjectsDashboardProfitDto;
import com.timaven.timekeeping.model.dto.WeeklyPayrollDto;
import com.timaven.timekeeping.model.enums.WeeklyProcessStatus;
import com.timaven.timekeeping.model.paging.Page;
import com.timaven.timekeeping.model.paging.PagingRequest;
import com.timaven.timekeeping.service.UserProjectService;
import com.timaven.timekeeping.service.WeeklyProcessService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@RestController
@PreAuthorize("hasAnyRole('ROLE_admin', 'ROLE_corporate_accountant', 'ROLE_corporate_timekeeper')")
public class AdminCorporateAccountantRestController extends BaseController {

    private final UserProjectService userProjectService;
    private final WeeklyProcessService weeklyProcessService;

    @Autowired
    public AdminCorporateAccountantRestController(UserProjectService userProjectService,
                                                  WeeklyProcessService weeklyProcessService) {
        this.userProjectService = userProjectService;
        this.weeklyProcessService = weeklyProcessService;
    }

    @PostMapping(value = {"/projects-dashboard-profit-dtos"})
    public Page<ProjectsDashboardProfitDto> projectsDashboardProfitDtoPage(@RequestBody PagingRequest pagingRequest) {
        Page<Project> projectsByUsername = userProjectService.getProjectsByUsername(getUsername(), pagingRequest, true);
        List<ProjectsDashboardDto> dtos = new ArrayList<>();
        List<ProjectsDashboardProfitDto> profitDtos = new ArrayList<>();
        weeklyProcessService.getCorporateDashboard(dtos, profitDtos, projectsByUsername.getData());
        Page<ProjectsDashboardProfitDto> result = new Page<>();
        result.setData(profitDtos);
        result.setDraw(projectsByUsername.getDraw());
        result.setRecordsTotal(projectsByUsername.getRecordsTotal());
        result.setRecordsFiltered(projectsByUsername.getRecordsFiltered());
        return result;
    }

    @GetMapping(value = "/weekly-payroll-data")
    public List<WeeklyPayrollDto> getWeeklyPayrollData(@RequestParam LocalDate weekEndDate) {
        return weeklyProcessService.getWeeklyPayrollDtos(weekEndDate);
    }

    @PutMapping(value = "/weekly-payroll")
    public String updateWeeklyPayrollsStatus(@RequestBody Collection<Long> ids, @RequestParam WeeklyProcessStatus status) {
        weeklyProcessService.updateWeeklyProcesses(ids, status);
        return RESPONSE_SUCCESS;
    }
}

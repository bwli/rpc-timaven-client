package com.timaven.timekeeping.controller;

import com.timaven.timekeeping.model.CostCodeTag;
import com.timaven.timekeeping.model.CostCodeTagType;
import com.timaven.timekeeping.model.PerDiem;
import com.timaven.timekeeping.model.Rule;
import com.timaven.timekeeping.model.dto.EquipmentPriceDto;
import com.timaven.timekeeping.model.paging.Page;
import com.timaven.timekeeping.model.paging.PagingRequest;
import com.timaven.timekeeping.service.CostCodeService;
import com.timaven.timekeeping.service.EquipmentService;
import com.timaven.timekeeping.service.PerDiemService;
import com.timaven.timekeeping.service.RuleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;

@RestController
@SessionAttributes({"sessionProjectId", "allowedDayOfWeek", "rule", "sessionTeamName", "sessionTimesheetsMode", "sessionTimesheetsDate"})
@PreAuthorize("hasRole('ROLE_project_manager')")
public class ProjectManagerRestController extends BaseController {

    private final EquipmentService equipmentService;
    private final CostCodeService costCodeService;
    private final PerDiemService perDiemService;
    private final RuleService ruleService;

    @Autowired
    public ProjectManagerRestController(EquipmentService equipmentService, CostCodeService costCodeService,
                                        PerDiemService perDiemService, RuleService ruleService) {
        this.equipmentService = equipmentService;
        this.costCodeService = costCodeService;
        this.perDiemService = perDiemService;
        this.ruleService = ruleService;
    }

    @RequestMapping(value = "/equipment-usage", method = {RequestMethod.POST})
    public String addOrUpdateEquipmentUsage(@ModelAttribute("sessionProjectId") Long projectId,
                                            @RequestParam Long equipmentId,
                                            @RequestParam(required = false) Long id,
                                            @RequestParam LocalDate date,
                                            @RequestParam String type) {
        equipmentService.saveEquipmentUsage(equipmentId, getUserId(), projectId, id, date, type);
        return RESPONSE_SUCCESS;
    }

    @PostMapping(value = "/equipment-price-list")
    public Page<EquipmentPriceDto> equipmentPriceListTable(@RequestBody PagingRequest pagingRequest,
                                                           @ModelAttribute("sessionProjectId") Long projectId) {
        return equipmentService.getEquipmentPricesByProjectId(projectId, pagingRequest);
    }

    @PostMapping(value = "/tag-type-list")
    public Page<CostCodeTagType> costCodeTagTypePage(@RequestBody PagingRequest pagingRequest,
                                                     @ModelAttribute("sessionProjectId") Long projectId) {
        return costCodeService.getCostCodeTagTypePage(projectId, pagingRequest);
    }

    @PostMapping(value = "/tag-code-list")
    public Page<CostCodeTag> costCodeTagPage(@RequestBody PagingRequest pagingRequest,
                                             @RequestParam Long typeId) {
        return costCodeService.getCostCodeTagPage(typeId, pagingRequest);
    }

    @PostMapping(value = "/per-diem-list")
    public Page<PerDiem> perDiemPage(@RequestBody PagingRequest pagingRequest,
                                     @ModelAttribute("sessionProjectId") Long projectId) {
        return perDiemService.getPerDiemPage(pagingRequest, projectId);
    }

    @PutMapping(value = "/rules/{ruleId}/per-diem-id/{perDiemId}")
    public String changeRulePerDiem(ModelMap model, @PathVariable("ruleId") Long ruleId, @PathVariable("perDiemId") Long perDiemId) {
        Rule rule = ruleService.updateRulePerDiem(ruleId, perDiemId);
        // Refresh session rule
        model.put("rule", rule);
        return RESPONSE_SUCCESS;
    }
}

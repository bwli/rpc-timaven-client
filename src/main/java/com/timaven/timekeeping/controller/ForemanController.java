package com.timaven.timekeeping.controller;

import com.timaven.timekeeping.model.Project;
import com.timaven.timekeeping.model.ReportRecord;
import com.timaven.timekeeping.model.Rule;
import com.timaven.timekeeping.service.ContentService;
import com.timaven.timekeeping.service.CostCodeService;
import com.timaven.timekeeping.service.RuleService;
import com.timaven.timekeeping.service.UserProjectService;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;

import java.time.LocalDate;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

@Controller
@SessionAttributes({"sessionProjectId", "allowedDayOfWeek", "rule", "sessionTeamName", "sessionTimesheetsMode", "sessionTimesheetsDate", "idProjectMap"})
@PreAuthorize("hasRole('ROLE_foreman')")
public class ForemanController extends BaseController {

    private final static String KEY_CC = "costCode";
    private final static String KEY_PERC = "percentage";

    private final ContentService mContentService;
    private final UserProjectService mUserProjectService;
    private final RuleService ruleService;
    private final CostCodeService costCodeService;

    @Autowired
    public ForemanController(ContentService contentService, UserProjectService userProjectService,
                             RuleService ruleService, CostCodeService costCodeService) {
        mContentService = contentService;
        mUserProjectService = userProjectService;
        this.ruleService = ruleService;
        this.costCodeService = costCodeService;
    }

    /**
     * Show home page
     */
    @GetMapping(value = "/home-foreman")
    public ModelAndView foremanHome(ModelMap model, @RequestParam(required = false) Long projectId) {
        ModelAndView modelAndView = new ModelAndView("foreman-home");
        Set<Project> projects = mUserProjectService.getProjectsByUsername(getUsername());
        if (projectId != null) {
            model.addAttribute("sessionProjectId", projectId);
        } else {
            if (!model.containsAttribute("sessionProjectId")) {
                if (CollectionUtils.isEmpty(projects)) {
                    modelAndView = new ModelAndView("operation-error");
                    modelAndView.addObject("errorMsg", "This user does not belong to any project");
                    modelAndView.addObject("solution", "Please ask the admin to help");
                    return modelAndView;
                }
                projectId = projects.iterator().next().getId();
                model.addAttribute("sessionProjectId", projectId);
            } else {
                projectId = (Long) model.get("sessionProjectId");
            }
        }
        if (!model.containsAttribute("idProjectMap")) {
            Map<Long, Project> idProjectMap = projects.stream()
                    .collect(Collectors.toMap(Project::getId, Function.identity()));
            modelAndView.addObject("idProjectMap", idProjectMap);
        }
        Optional<Rule> ruleOpt = ruleService.getRule(projectId);
        if (ruleOpt.isEmpty()) {
            modelAndView = new ModelAndView("operation-error");
            modelAndView.addObject("errorMsg", "Rule or default shift has not been set.");
            modelAndView.addObject("solution", "Please ask your project manager to set up the rule and default shift");
            return modelAndView;
        } else {
            modelAndView.addObject("rule", ruleOpt.orElse(null));
        }
        modelAndView.addObject("teams", mContentService.getTeamNamesByUserIdAndProjectId(getUserId(), projectId));
        return modelAndView;
    }

    /**
     * Show report page
     */
    @GetMapping(value = "/report")
    public ModelAndView report(ModelMap model, @RequestParam String teamName,
                               @ModelAttribute("sessionProjectId") Long projectId) {
        ModelAndView modelAndView = new ModelAndView("report");
        model.addAttribute("sessionTeamName", teamName);
        modelAndView.addObject("costCodeLogs", costCodeService.getCostCodeLogsByTeamNameAndDateOfService(teamName,
                projectId,
                LocalDate.now().minusDays(1)));
        return modelAndView;
    }

    /**
     * Save report
     */
    @PostMapping(value = "/report")
    public @ResponseBody
    String report(MultipartHttpServletRequest request, @ModelAttribute("sessionTeamName") String teamName,
                  @ModelAttribute("sessionProjectId") Long projectId) {
        JSONObject jsonObject = requestParamsToJSON(request);
        Iterator<String> keys = jsonObject.keys();
        TreeMap<Integer, String> orderCostCodeMap = new TreeMap<>();
        TreeMap<Integer, Integer> orderPercMap = new TreeMap<>();
        // Collecting data
        while (keys.hasNext()) {
            String key = keys.next();
            int order = Integer.parseInt(key.substring(key.lastIndexOf("-") + 1));
            if (key.startsWith(KEY_CC)) {
                String costCode = jsonObject.getString(key);
                orderCostCodeMap.put(order, costCode);
            } else if (key.startsWith(KEY_PERC)) {
                int perc = Integer.parseInt(jsonObject.getString(key));
                orderPercMap.put(order, perc);
            }
        }
        List<ReportRecord> reportRecords = new ArrayList<>();
        for (Map.Entry<Integer, String> entry : orderCostCodeMap.entrySet()) {
            int key = entry.getKey();
            String costCode = entry.getValue();
            int percentage = orderPercMap.get(key);
            ReportRecord reportRecord = new ReportRecord(costCode, percentage);
            reportRecords.add(reportRecord);
        }
        mContentService.saveReports(teamName, reportRecords, projectId);
        return RESPONSE_SUCCESS;
    }
}

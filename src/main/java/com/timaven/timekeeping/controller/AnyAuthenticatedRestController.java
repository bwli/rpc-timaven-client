package com.timaven.timekeeping.controller;

import com.timaven.timekeeping.configuration.feature.FeatureProperties;
import com.timaven.timekeeping.configuration.web.ThreadTenantStorage;
import com.timaven.timekeeping.model.*;
import com.timaven.timekeeping.model.dto.*;
import com.timaven.timekeeping.model.paging.Page;
import com.timaven.timekeeping.model.paging.PagingRequest;
import com.timaven.timekeeping.service.*;
import com.timaven.timekeeping.util.AuthenticationHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

@RestController
@SessionAttributes({"sessionProjectId", "allowedDayOfWeek", "rule", "sessionTeamName", "sessionTimesheetsMode", "sessionTimesheetsDate"})
@PreAuthorize("isAuthenticated()")
public class AnyAuthenticatedRestController extends BaseController {

    private final NotificationService notificationService;
    private final ContentService contentService;
    private final EmployeeService employeeService;
    private final RuleService ruleService;
    private final ShiftService shiftService;
    private final CostCodeService costCodeService;
    private final BillingCodeService billingCodeService;
    private final FeatureProperties featureProperties;
    private final UserProjectService userProjectService;
    private final WeeklyProcessService weeklyProcessService;

    @Autowired
    public AnyAuthenticatedRestController(NotificationService notificationService, ContentService contentService,
                                          EmployeeService employeeService, RuleService ruleService, ShiftService shiftService, CostCodeService costCodeService, BillingCodeService billingCodeService, FeatureProperties featureProperties, UserProjectService userProjectService, WeeklyProcessService weeklyProcessService) {
        this.notificationService = notificationService;
        this.contentService = contentService;
        this.employeeService = employeeService;
        this.ruleService = ruleService;
        this.shiftService = shiftService;
        this.costCodeService = costCodeService;
        this.billingCodeService = billingCodeService;
        this.featureProperties = featureProperties;
        this.userProjectService = userProjectService;
        this.weeklyProcessService = weeklyProcessService;
    }

    @PutMapping(value = "/notification", params = {"id", "read"})
    public String updateNotificationRead(@RequestParam Long id, @RequestParam boolean read) {
        notificationService.updateNotificationRead(id, read);
        return RESPONSE_SUCCESS;
    }

    @GetMapping(value = "/notification-count")
    public String getNotificationCount() {
        return String.valueOf(notificationService.getUnreadNotificationCountByUserId(getUserId()));
    }

    @PreAuthorize("hasAnyRole('ROLE_project_manager','ROLE_accountant', 'ROLE_accountant_foreman', 'ROLE_corporate_accountant', 'ROLE_corporate_timekeeper')")
    @PostMapping(value = "/employees")
    public Page<EmployeeDto> getEmployees(@RequestBody PagingRequest pagingRequest, ModelMap model) {
        Long projectId = (Long) model.get("sessionProjectId");
        return employeeService.getEmployees(pagingRequest, projectId);
    }

    @PreAuthorize("hasAnyRole('ROLE_project_manager','ROLE_accountant', 'ROLE_accountant_foreman', 'ROLE_corporate_accountant', 'ROLE_corporate_timekeeper')")
    @PostMapping(value = "/employee-audit")
    public Page<EmployeeAuditDto> getEmployeeAudit(@RequestBody PagingRequest pagingRequest,
                                                   ModelMap model) {
        Long projectId = (Long) model.get("sessionProjectId");
        return employeeService.getEmployeeAudit(pagingRequest, projectId);
    }

    @PreAuthorize("hasAnyRole('ROLE_project_manager','ROLE_accountant', 'ROLE_accountant_foreman', 'ROLE_corporate_accountant', 'ROLE_corporate_timekeeper')")
    @PostMapping(value = "/employee-tags")
    public String saveEmployeeTag(ModelMap model,
                                  @RequestParam String empId,
                                  @RequestParam String tag) {
        Long projectId = (Long) model.get("sessionProjectId");
        employeeService.saveEmployeeTag(projectId, empId, tag);
        return RESPONSE_SUCCESS;
    }

    @PreAuthorize("hasAnyRole('ROLE_project_manager','ROLE_accountant', 'ROLE_accountant_foreman', 'ROLE_corporate_accountant', 'ROLE_corporate_timekeeper')")
    @DeleteMapping(value = "/employee-tags")
    public String deleteEmployeeTag(ModelMap model,
                                    @RequestParam String empId,
                                    @RequestParam String tag) {
        Long projectId = (Long) model.get("sessionProjectId");
        employeeService.deleteEmployeeTag(projectId, empId, tag);
        return RESPONSE_SUCCESS;
    }

    @PreAuthorize("hasAnyRole('ROLE_project_manager','ROLE_accountant', 'ROLE_accountant_foreman', 'ROLE_corporate_accountant')")
    @PutMapping(value = {"/employees/{employeeId}/per-diem-id", "/employees/{employeeId}/per-diem-id/{perDiemId}"})
    public String updateCraftPerDiem(@PathVariable("employeeId") Long employeeId,
                                     @PathVariable(value = "perDiemId", required = false) Long perDiemId) {
        employeeService.updateEmployeePerDiem(employeeId, perDiemId);
        return RESPONSE_SUCCESS;
    }

    // TM-307
    @PreAuthorize("hasAnyRole('ROLE_project_manager','ROLE_accountant', 'ROLE_accountant_foreman', 'ROLE_corporate_accountant')")
    @PutMapping(value = "/employees/{employeeId}/is-offsite/{isOffsite}")
    public String updateIsOffset(@PathVariable("employeeId") Long employeeId,
                                 @PathVariable("isOffsite") Boolean isOffsite) {
        employeeService.updateEmployeeIsOffsite(employeeId, isOffsite);
        return RESPONSE_SUCCESS;
    }

    @PreAuthorize("hasAnyRole('ROLE_admin', 'ROLE_corporate_accountant', 'ROLE_accountant', 'ROLE_accountant_foreman')")
    @GetMapping(value = "/employees/id/name")
    public List<EmployeeIdNameDto> getEmployeeIdNames(@RequestParam Long projectId,
                                                      @RequestParam LocalDate dateOfService) {
        return employeeService.getEmployeeIdNames(projectId, dateOfService).stream().sorted().collect(Collectors.toList());
    }

    @PreAuthorize("hasAnyRole('ROLE_admin', 'ROLE_corporate_accountant', 'ROLE_accountant', 'ROLE_accountant_foreman')")
    @GetMapping(value = "/teams")
    public Set<String> getTeamNames(ModelMap model,
                                    @RequestParam LocalDate startDate,
                                    @RequestParam(required = false) LocalDate endDate,
                                    @RequestParam(required = false) Long projectId) {
        if (null == projectId) projectId = (Long) model.getAttribute("sessionProjectId");
        return contentService.getTeamNames(projectId, startDate, endDate);
    }

    @PreAuthorize("hasAnyRole('ROLE_admin', 'ROLE_corporate_accountant', 'ROLE_accountant', 'ROLE_accountant_foreman')")
    @GetMapping(value = "/previous-payroll-date")
    public ResponseEntity<PayrollDateDto> getPreviousPayrollDate(@RequestParam(required = false) LocalDate dateOfService,
                                                                 @RequestParam Long projectId) {
        if (dateOfService == null) dateOfService = LocalDate.now();
        Optional<Rule> ruleOpt = ruleService.getRule(projectId);
        LocalDate finalDateOfService = dateOfService;
        return ruleOpt.map(r -> ResponseEntity.ok()
                        .body(new PayrollDateDto(r.getPreviousEndOfWeek(finalDateOfService).format(DateTimeFormatter.ofPattern("MM/dd/yyyy")),
                                r.getWeekEndDay().getValue() % 7)))
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

    @PreAuthorize("hasAnyRole('ROLE_admin', 'ROLE_corporate_accountant', 'ROLE_accountant', 'ROLE_accountant_foreman')")
    @GetMapping(value = "/shift-names")
    public List<String> getShiftNames(@RequestParam Long projectId) {
        Set<String> shifts = shiftService.getAllShiftNamesByProjectId(projectId);
        return shifts.stream().sorted().collect(Collectors.toList());
    }

    @PreAuthorize("hasAnyRole('ROLE_admin', 'ROLE_corporate_accountant', 'ROLE_accountant', 'ROLE_accountant_foreman')")
    @GetMapping(value = "/cost-code-tag-types")
    public List<CostCodeTagType> getCostCodeTagTypes(@RequestParam Long projectId) {
        return costCodeService.getAllCostCodeTagTypesByProjectId(projectId);
    }

    @PreAuthorize("hasAnyRole('ROLE_admin', 'ROLE_corporate_accountant', 'ROLE_accountant', 'ROLE_accountant_foreman')")
    @GetMapping(value = "/billing-code-types")
    public List<BillingCodeType> getBillingCodeTypes(@RequestParam Long projectId) {
        return billingCodeService.findBillingCodeTypesSortByLevel();
    }

    @PreAuthorize("hasAnyRole('ROLE_admin', 'ROLE_corporate_accountant', 'ROLE_accountant', 'ROLE_accountant_foreman')")
    @GetMapping(value = "/feature-flags")
    public Map<String, Boolean> getFeatureProperties() {
        FeatureProperties properties = ThreadTenantStorage.getFeature(featureProperties);
        return properties.getFlags();
    }

    @GetMapping(value = "/shift", params = {"name"})
    public Shift shift(ModelMap model, @RequestParam String name) {
        Long projectId = (Long) model.get("sessionProjectId");
        return shiftService.getShiftByProjectIdAndName(projectId, name);
    }

    @PreAuthorize("hasAnyRole('ROLE_admin', 'ROLE_corporate_accountant', 'ROLE_corporate_timekeeper', 'ROLE_project_manager', 'ROLE_accountant', 'ROLE_accountant_foreman', 'ROLE_timekeeper', 'ROLE_purchasing')")
    @PostMapping(value = "/projects-dashboard-dtos")
    public Page<ProjectsDashboardDto> projectsDashboardDtoPage(@RequestBody PagingRequest pagingRequest) {
        boolean isAll = AuthenticationHelper.hasAnyRole(List.of("ROLE_admin", "ROLE_corporate_accountant", "ROLE_corporate_timekeeper"));
        Page<Project> projectsByUsername = userProjectService.getProjectsByUsername(getUsername(), pagingRequest, isAll);
        List<ProjectsDashboardDto> dtos = new ArrayList<>();
        Set<Project> projects = new HashSet<>(projectsByUsername.getData());
        weeklyProcessService.getProjectsDashboard(dtos, projects);
        Page<ProjectsDashboardDto> result = new Page<>();
        result.setData(dtos);
        result.setDraw(projectsByUsername.getDraw());
        result.setRecordsTotal(projectsByUsername.getRecordsTotal());
        result.setRecordsFiltered(projectsByUsername.getRecordsFiltered());
        return result;
    }
}

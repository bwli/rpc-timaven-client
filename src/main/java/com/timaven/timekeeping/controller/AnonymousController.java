package com.timaven.timekeeping.controller;

import com.timaven.timekeeping.model.User;
import com.timaven.timekeeping.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class AnonymousController extends BaseController {

    private final AccountService accountService;

    @Autowired
    public AnonymousController(AccountService accountService) {
        this.accountService = accountService;
    }

    /**
     * This method handles Access-Denied redirect.
     */
    @RequestMapping(value = "Access_Denied")
    public ModelAndView accessDeniedPage() {
        User user = accountService.loadUserByUsername(getUsername());
        ModelAndView modelAndView = new ModelAndView("accessDenied");
        modelAndView.addObject("user", user);
        return modelAndView;
    }

    @RequestMapping(value = "/silence-logout")
    public String silenceLogout() {
        return "silence-logout";
    }

    /**
     * Index page
     */
    @GetMapping(value = "index")
    public String accessIndexPage() {
        return "index";
    }
}

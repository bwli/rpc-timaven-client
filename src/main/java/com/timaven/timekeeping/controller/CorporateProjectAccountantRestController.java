package com.timaven.timekeeping.controller;

import com.timaven.timekeeping.configuration.feature.FeatureProperties;
import com.timaven.timekeeping.configuration.web.ThreadTenantStorage;
import com.timaven.timekeeping.model.PurchaseOrderBilling;
import com.timaven.timekeeping.model.PurchaseVendorRoster;
import com.timaven.timekeeping.model.RequisitionGroup;
import com.timaven.timekeeping.model.dto.*;
import com.timaven.timekeeping.model.paging.Page;
import com.timaven.timekeeping.model.paging.PagingRequest;
import com.timaven.timekeeping.service.CraftService;
import com.timaven.timekeeping.service.PurchaseOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@SessionAttributes({"sessionProjectId", "allowedDayOfWeek", "rule", "sessionTeamName", "sessionTimesheetsMode", "sessionTimesheetsDate"})
@PreAuthorize("hasAnyRole('ROLE_accountant', 'ROLE_accountant_foreman', 'ROLE_corporate_accountant', 'ROLE_timekeeper', 'ROLE_purchasing')")
public class CorporateProjectAccountantRestController extends BaseController {

    private final PurchaseOrderService purchaseOrderService;
    private final CraftService craftService;

    @Autowired
    public CorporateProjectAccountantRestController(PurchaseOrderService purchaseOrderService, CraftService craftService) {
        this.purchaseOrderService = purchaseOrderService;
        this.craftService = craftService;
    }

    @PreAuthorize("hasAnyRole('ROLE_accountant', 'ROLE_accountant_foreman', 'ROLE_corporate_accountant', 'ROLE_purchasing')")
    @PostMapping(value = "/purchase-order-details")
    public Page<PurchaseOrderDetailDto> getPurchaseOrderDetails(@RequestBody PagingRequest pagingRequest,
                                                                @RequestParam String purchaseOrderNumber) {
        return purchaseOrderService.getLatestPurchaseOrderDetails(pagingRequest, purchaseOrderNumber);
    }

    @PreAuthorize("hasAnyRole('ROLE_accountant', 'ROLE_accountant_foreman', 'ROLE_corporate_accountant', 'ROLE_purchasing')")
    @PostMapping(value = "purchase-order-billing-detail-history")
    public Page<PurchaseOrderBillingDetailViewDto> getPurchaseOrderDetailHistory(@RequestBody PagingRequest pagingRequest,
                                                                                 @RequestParam String purchaseOrderNumber) {
        return purchaseOrderService.getPurchaseOrderDetailHistory(pagingRequest, purchaseOrderNumber);
    }

    @PreAuthorize("hasAnyRole('ROLE_accountant', 'ROLE_accountant_foreman', 'ROLE_corporate_accountant', 'ROLE_purchasing')")
    @PostMapping(value = "/purchase-order-summaries")
    public Page<PurchaseOrderSummaryDto> getPurchaseOrderSummaries(ModelMap model,
                                                                   @RequestBody PagingRequest pagingRequest) {
        Long projectId = (Long) model.get("sessionProjectId");
        return purchaseOrderService.getLatestPurchaseOrderSummaries(pagingRequest, projectId);
    }

    @PutMapping(value = "/purchase-order-group")
    public String updatePurchaseOrderGroup(@RequestParam String purchaseOrderNumber,
                                           @RequestParam(required = false) String comment,
                                           @RequestParam(required = false) Boolean flag) {
        if (comment != null) {
            purchaseOrderService.updatePurchaseOrderGroupComment(purchaseOrderNumber, comment);
        }
        if (flag != null) {
            purchaseOrderService.updatePurchaseOrderGroupFlag(purchaseOrderNumber, flag);
        }
        return RESPONSE_SUCCESS;
    }

    @PreAuthorize("hasAnyRole('ROLE_accountant', 'ROLE_accountant_foreman', 'ROLE_corporate_accountant', 'ROLE_purchasing')")
    @PostMapping(value = "/requisition-group")
    public Page<RequisitionGroup> getRequisitionGroup(ModelMap model, @RequestBody PagingRequest pagingRequest) {
        Long projectId = (Long) model.get("sessionProjectId");
        return purchaseOrderService.getRequisitionGroups(pagingRequest, projectId);
    }

    @PostMapping(value = "/purchase-order-billings")
    @ResponseBody
    public PurchaseOrderBilling addPurchaseOrderBilling(@RequestBody PurchaseOrderBillingDto dto) {
        return purchaseOrderService.savePurchaseOrderBilling(dto);
    }

    @PreAuthorize("hasAnyRole('ROLE_accountant', 'ROLE_accountant_foreman', 'ROLE_corporate_accountant')")
    @PostMapping(value = "/craft-list")
    public Page<CraftDto> getCraftPage(ModelMap model, @RequestBody PagingRequest pagingRequest) {
        Long projectId = (Long) model.get("sessionProjectId");
        return craftService.getCraftPage(pagingRequest, projectId);
    }

    @PreAuthorize("hasAnyRole('ROLE_accountant', 'ROLE_accountant_foreman', 'ROLE_corporate_accountant')")
    @PutMapping(value = "/crafts/{craftId}/per-diem-id/{perDiemId}")
    public String updateCraftPerDiem(@PathVariable("craftId") Long craftId,
                                     @PathVariable("perDiemId") Long perDiemId) {
        craftService.updateCraftPerDiem(craftId, perDiemId);
        return RESPONSE_SUCCESS;
    }

    @DeleteMapping(value = "/purchase-order-billings/{id}")
    public String deletePurchaseOrderBilling(@PathVariable Long id) {
        purchaseOrderService.deletePurchaseOrderBillingById(id);
        return RESPONSE_SUCCESS;
    }

    @PostMapping(value = "/purchase-vendor-roster-list")
    public Page<PurchaseVendorRoster> getPurchaseVendorRoster(@RequestBody PagingRequest pagingRequest){
        return purchaseOrderService.getPurchaseVendorRoster(pagingRequest);
    }
}

package com.timaven.timekeeping.controller;

import com.timaven.timekeeping.api.ExportServer.ExportAPI;
import com.timaven.timekeeping.common.RuleUtils;
import com.timaven.timekeeping.model.*;
import com.timaven.timekeeping.model.dto.AllocationSubmissionDto;
import com.timaven.timekeeping.model.dto.AllocationTimeDto;
import com.timaven.timekeeping.model.dto.CostCodeTagDto;
import com.timaven.timekeeping.model.enums.AllocationSubmissionStatus;
import com.timaven.timekeeping.model.enums.WeeklyProcessType;
import com.timaven.timekeeping.service.*;
import com.timaven.timekeeping.util.AuthenticationHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.temporal.TemporalAdjusters;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toMap;
import static java.util.stream.Collectors.toSet;

@Controller
@SessionAttributes({"sessionProjectId", "allowedDayOfWeek", "rule", "sessionTeamName", "sessionTimesheetsMode", "sessionTimesheetsDate"})
@PreAuthorize("hasAnyRole('ROLE_foreman', 'ROLE_accountant', 'ROLE_accountant_foreman', 'ROLE_corporate_accountant'," +
        " 'ROLE_timekeeper')")
public class AccountantForemanController extends BaseController {

    private final ContentService mContentService;
    private final ExportAPI mExportAPI;
    private final RainOutService rainOutService;
    private final WeeklyProcessService weeklyProcessService;
    private final CostCodeService costCodeService;
    private final PerDiemService perDiemService;

    @Autowired
    public AccountantForemanController(ContentService contentService, ExportAPI exportAPI, RainOutService rainOutService, WeeklyProcessService weeklyProcessService, CostCodeService costCodeService, PerDiemService perDiemService) {
        mContentService = contentService;
        mExportAPI = exportAPI;
        this.rainOutService = rainOutService;
        this.weeklyProcessService = weeklyProcessService;
        this.costCodeService = costCodeService;
        this.perDiemService = perDiemService;
    }

    @GetMapping(value = "/timesheets")
    public ModelAndView timesheets(ModelMap model,
                                   @RequestParam(required = false) String mode,
                                   @RequestParam(required = false) String teamName,
                                   @ModelAttribute("sessionProjectId") Long projectId,
                                   @RequestParam(required = false) LocalDate date) {
        Rule rule;
        ModelAndView modelAndView;
        if (!model.containsAttribute("rule")) {
            modelAndView = new ModelAndView("operation-error");
            modelAndView.addObject("errorMsg", "Rule has not been set.");
            modelAndView.addObject("solution", "Please set the rule first.");
            return modelAndView;
        }
        rule = (Rule) model.get("rule");

        modelAndView = new ModelAndView("timesheets");
        if (StringUtils.hasText(teamName)) {
            model.addAttribute("sessionTeamName", teamName);
        }
        if (!StringUtils.hasText(teamName) && model.containsKey("sessionTeamName")) {
            teamName = (String) model.get("sessionTeamName");
        }
        if (!StringUtils.hasText(mode)) {
            mode = (String) model.getOrDefault("sessionTimesheetsMode", "1days");
        }
        if (null == date) {
            date = (LocalDate) model.getOrDefault("sessionTimesheetsDate", null);
        }
        int days = switch (mode) {
            case "1days" -> 1;
            case "2days" -> 2;
            case "3days" -> 3;
            case "4days" -> 4;
            case "5days" -> 5;
            case "6days" -> 6;
            case "7days" -> 7;
            case "30days" -> 30;
            default -> -1;
        };

        LocalDate startDate;
        LocalDate endDate = null;
        if (days != -1) {
            startDate = LocalDate.now().minusDays(days);
        } else {
            if (mode.equals("lastPayrollWeek")) {
                endDate = rule.getPreviousEndOfWeek(LocalDate.now());
                startDate = endDate.minusDays(6);
            } else if (mode.equals("specificDay")) {
                endDate = date;
                startDate = date;
            } else {
                endDate = date;
                startDate = endDate.minusDays(6);
            }
        }
        if (AuthenticationHelper.hasRole("ROLE_accountant_foreman") || AuthenticationHelper.hasRole("ROLE_foreman")
                || AuthenticationHelper.hasRole("ROLE_timekeeper")) {

            List<StageTransaction> stageTransactions = mContentService.getStageTransactions(projectId, teamName, startDate, endDate, null);
            Set<AllocationSubmission> allocationSubmissions = mContentService.getAllocationSubmissions(startDate, endDate, projectId, teamName, null, null, List.of("users"));
            allocationSubmissions.forEach(s -> {
                s.setTlo(!s.getPayrollDate().isEqual(rule.getEndOfWeek(s.getDateOfService())));
            });

            Set<TeamServiceDate> teamServiceDates = null;
            if (!CollectionUtils.isEmpty(stageTransactions)) {
                Set<TeamServiceDate> teamServiceDateSet = allocationSubmissions.stream()
                        .filter(s -> !s.getStatus().equals(AllocationSubmissionStatus.DENIED))
                        .map(e -> new TeamServiceDate(e.getTeamName(), e.getDateOfService(), e.getPayrollDate())).collect(toSet());
                teamServiceDates = stageTransactions.stream()
                        .map(st -> new TeamServiceDate(st.getTeamName(), st.getDateOfService(), rule.getEndOfWeek(st.getDateOfService())))
                        .collect(Collectors.toCollection(TreeSet::new));
                teamServiceDates.removeIf(teamServiceDateSet::contains);
            }
            if (AuthenticationHelper.hasRole("ROLE_foreman")) {
                Long userId = getUserId();
                if (!CollectionUtils.isEmpty(teamServiceDates)) {
                    Set<TeamServiceDate> teamServiceDateSet = allocationSubmissions.stream()
                            .filter(s -> !s.getStatus().equals(AllocationSubmissionStatus.DENIED))
                            .filter(s -> !userId.equals(s.getSubmitUserId()))
                            .map(e -> new TeamServiceDate(e.getTeamName(), e.getDateOfService(), e.getPayrollDate())).collect(toSet());
                    teamServiceDates.removeIf(teamServiceDateSet::contains);
                }
                allocationSubmissions.removeIf(s -> !userId.equals(s.getSubmitUserId()));
            }
            modelAndView.addObject("teamServiceDates", teamServiceDates);
            modelAndView.addObject("allocationSubmissions", allocationSubmissions);
        } else {
            TransSubmission transSubmission = mContentService.getTodayTransSubmission(projectId);
            if (null != transSubmission) {
                modelAndView.addObject("lastUploaded", transSubmission.getCreatedAt());
            }

            Set<AllocationSubmission> allocationSubmissions = mContentService.getAllocationSubmissions(startDate, endDate, projectId, teamName, AllocationSubmissionStatus.RELEASED, null, List.of("users"));
            allocationSubmissions.forEach(s -> {
                s.setTlo(!s.getPayrollDate().isEqual(rule.getEndOfWeek(s.getDateOfService())));
            });
            modelAndView.addObject("allocationSubmissions", allocationSubmissions);
        }
        modelAndView.addObject("sessionTimesheetsMode", mode);
        modelAndView.addObject("sessionTimesheetsDate", date);
        return modelAndView;
    }

    @GetMapping(value = "/time-manage", params = {"allocationSubmissionId"})
    public ModelAndView timeManage(ModelMap model,
                                   @ModelAttribute("rule") Rule rule,
                                   @RequestParam Long allocationSubmissionId) {
        ModelAndView modelAndView = new ModelAndView("time-manage");
        Long projectId = (Long) model.get("sessionProjectId");

        AllocationSubmissionDto allocationSubmissionDto =
                mContentService.getAllocationSubmissionDtoById(allocationSubmissionId);
        Set<CostCodeTagDto> costCodeTagDtos = mContentService.getDefaultCostCodeTagDtos(allocationSubmissionDto);

        Set<CostCodeLog> costCodeLogSet =
                costCodeService.getCostCodeLogsByTeamNameAndDateOfService(allocationSubmissionDto.getTeamName(),
                        projectId, allocationSubmissionDto.getDateOfService());
        List<CostCodeLog> allCostCodeLogSet =
                costCodeService.getAllCostCodeLogsByProjectIdAndDateOfService(projectId, allocationSubmissionDto.getDateOfService());
        Set<CostCodeLog> validCostCodeLogSet = new TreeSet<>();
        Set<CostCodeLog> otherCostCodeLogSet = new TreeSet<>();
        Set<String> crewSet = new TreeSet<>();
        if (!CollectionUtils.isEmpty(costCodeLogSet)) {
            validCostCodeLogSet = new TreeSet<>(costCodeLogSet);
        }
        if (!CollectionUtils.isEmpty(allCostCodeLogSet)) {
            Set<String> finalValidCostCodeSet = validCostCodeLogSet.stream()
                    .map(CostCodeLog::getCostCodeFull).collect(Collectors.toSet());
            otherCostCodeLogSet = allCostCodeLogSet.stream()
                    .filter(e -> !finalValidCostCodeSet.contains(e.getCostCodeFull()))
                    .collect(Collectors.toCollection(TreeSet::new));
        }
        for (AllocationTimeDto allocationTimeDto : allocationSubmissionDto.getAllocationTimes()) {
            crewSet.add(Optional.ofNullable(allocationTimeDto.getEmployeeCrew()).orElse(""));
        }
        if (AuthenticationHelper.hasRole("ROLE_corporate_accountant")
                || AuthenticationHelper.hasRole("ROLE_accountant_foreman")
                || AuthenticationHelper.hasRole("ROLE_accountant")
                || AuthenticationHelper.hasRole("ROLE_timekeeper")) {
            modelAndView.addObject("rainOuts", rainOutService.getAllRainOuts(projectId));
        }

        List<CostCodeTagType> costCodeTagTypes = costCodeService.getAllCostCodeTagTypesByProjectId(projectId);
        PerDiem projectPerDiem = null;
        if (rule.getPerDiemId() != null) {
            projectPerDiem = perDiemService.findById(rule.getPerDiemId());
        }
        if (projectPerDiem == null) {
            projectPerDiem = new PerDiem();
            projectPerDiem.setDailyThreshold(1000);
            projectPerDiem.setWeeklyThreshold(1000);
        }
        if (!CollectionUtils.isEmpty(allocationSubmissionDto.getAllocationTimes())) {
            PerDiem finalProjectPerDiem = projectPerDiem;
            allocationSubmissionDto.getAllocationTimes().stream()
                    .filter(t -> t.getPerDiem() == null)
                    .forEach(t -> t.setPerDiem(finalProjectPerDiem));
        }
        modelAndView.addObject("costCodeTagTypes", costCodeTagTypes);
        modelAndView.addObject("crewSet", crewSet);
        modelAndView.addObject("teamName", allocationSubmissionDto.getTeamName());
        modelAndView.addObject("allocationSubmission", allocationSubmissionDto);
        modelAndView.addObject("costCodeTagDtos", costCodeTagDtos);
        modelAndView.addObject("validCostCodeLogSet", validCostCodeLogSet);
        modelAndView.addObject("otherCostCodeLogSet", otherCostCodeLogSet);
        LocalDate dateOfService = allocationSubmissionDto.getDateOfService();
        modelAndView.addObject("isTlo", allocationSubmissionDto.getPayrollDate() != null
                && allocationSubmissionDto.getDateOfService() != null
                && !rule.getEndOfWeek(allocationSubmissionDto.getDateOfService()).isEqual(allocationSubmissionDto.getPayrollDate()));
        modelAndView.addObject("dateOfService", dateOfService);
        // TM-327 add exported authority
        modelAndView.addObject("exported", allocationSubmissionDto.getExported());
        return modelAndView;
    }

    /**
     * Show time manage page
     */
    @GetMapping(value = "/time-manage", params = {"dateOfService"})
    public ModelAndView timeManage(ModelMap model,
                                   @ModelAttribute("rule") Rule rule,
                                   @RequestParam(required = false) String teamName,
                                   @RequestParam LocalDate dateOfService) {
        ModelAndView modelAndView = new ModelAndView("time-manage");
        Long projectId = (Long) model.get("sessionProjectId");

        teamName = StringUtils.hasText(teamName) ? teamName : (String) model.get("sessionTeamName");

        AllocationSubmissionDto allocationSubmissionDto = mContentService.getAllocationSubmissionDtoByDateOfService(projectId, teamName, dateOfService);

        Set<CostCodeTagDto> costCodeTagDtos = new TreeSet<>();
        Set<String> crewSet = new TreeSet<>();
        if (allocationSubmissionDto != null && !CollectionUtils.isEmpty(allocationSubmissionDto.getAllocationTimes())) {
            costCodeTagDtos = allocationSubmissionDto.getAllocationTimes().stream()
                    .map(AllocationTimeDto::getCostCodePercs)
                    .filter(ccp -> !CollectionUtils.isEmpty(ccp))
                    .flatMap(Collection::stream)
                    .map(costCodePercDto -> {
                        CostCodeTagDto costCodeTagDto = new CostCodeTagDto();
                        costCodeTagDto.setCostCodeFull(costCodePercDto.getCostCodeFull());
                        costCodeTagDto.setDisplayOrder(costCodePercDto.getDisplayOrder());
                        costCodeTagDto.setCostCodeTagAssociationDtos(costCodePercDto.getCostCodeTagAssociationDtos());
                        return costCodeTagDto;
                    })
                    .collect(Collectors.toCollection(TreeSet::new));

            crewSet = allocationSubmissionDto.getAllocationTimes().stream()
                    .map(AllocationTimeDto::getEmployeeCrew)
                    .filter(Objects::nonNull)
                    .collect(Collectors.toCollection(TreeSet::new));
        }

        Set<CostCodeLog> validCostCodeLogSet = new TreeSet<>(costCodeService
                .getCostCodeLogsByTeamNameAndDateOfService(teamName, projectId, dateOfService));
        Set<String> validCostCodeSet = validCostCodeLogSet.stream()
                .map(CostCodeLog::getCostCodeFull).collect(Collectors.toSet());
        Set<CostCodeLog> otherCostCodeLogSet =
                costCodeService.getAllCostCodeLogsByProjectIdAndDateOfService(projectId, dateOfService).stream()
                        .filter(e -> !validCostCodeSet.contains(e.getCostCodeFull()))
                        .collect(Collectors.toCollection(TreeSet::new));

        if (AuthenticationHelper.hasRole("ROLE_corporate_accountant")
                || AuthenticationHelper.hasRole("ROLE_accountant_foreman")
                || AuthenticationHelper.hasRole("ROLE_accountant")
                || AuthenticationHelper.hasRole("ROLE_timekeeper")) {
            modelAndView.addObject("rainOuts", rainOutService.getAllRainOuts(projectId));
        }

        List<CostCodeTagType> costCodeTagTypes = costCodeService.getAllCostCodeTagTypesByProjectId(projectId);
        PerDiem projectPerDiem = null;
        if (rule.getPerDiemId() != null) {
            projectPerDiem = perDiemService.findById(rule.getPerDiemId());
        }
        if (projectPerDiem == null) {
            projectPerDiem = new PerDiem();
            projectPerDiem.setDailyThreshold(1000);
            projectPerDiem.setWeeklyThreshold(1000);
        }
        if (allocationSubmissionDto != null && !CollectionUtils.isEmpty(allocationSubmissionDto.getAllocationTimes())) {
            PerDiem finalProjectPerDiem = projectPerDiem;
            allocationSubmissionDto.getAllocationTimes().stream()
                    .filter(t -> t.getPerDiem() == null)
                    .forEach(t -> t.setPerDiem(finalProjectPerDiem));
        }
        modelAndView.addObject("costCodeTagTypes", costCodeTagTypes);
        modelAndView.addObject("crewSet", crewSet);
        modelAndView.addObject("allocationSubmission", allocationSubmissionDto);
        modelAndView.addObject("teamName", teamName);
        modelAndView.addObject("costCodeTagDtos", costCodeTagDtos);
        modelAndView.addObject("dateOfService", dateOfService);
        modelAndView.addObject("isTlo", allocationSubmissionDto != null
                && allocationSubmissionDto.getPayrollDate() != null && allocationSubmissionDto.getDateOfService() != null
                && !rule.getEndOfWeek(allocationSubmissionDto.getDateOfService()).isEqual(allocationSubmissionDto.getPayrollDate()));
        modelAndView.addObject("validCostCodeLogSet", validCostCodeLogSet);
        modelAndView.addObject("otherCostCodeLogSet", otherCostCodeLogSet);
        return modelAndView;
    }

    /**
     * Save time allocations
     */
    @PreAuthorize("hasAnyRole('ROLE_foreman', 'ROLE_accountant', 'ROLE_accountant_foreman', 'ROLE_timekeeper')")
    @PostMapping(value = "/save-time")
    public @ResponseBody
    String saveTime(@RequestBody AllocationSubmissionDto allocationSubmissionDto,
                    @ModelAttribute("rule") Rule rule) {
        AllocationSubmission allocationSubmission = new AllocationSubmission(allocationSubmissionDto);
        AllocationSubmissionStatus status = allocationSubmission.getStatus();
        Set<AllocationTime> allocationTimeSet = allocationSubmission.getAllocationTimes().stream()
                .filter(t -> StringUtils.hasText(t.getEmpId())
                        || StringUtils.hasText(t.getBadge())
                        || StringUtils.hasText(t.getClientEmpId()))
                .collect(Collectors.toSet());
        if (AllocationSubmissionStatus.APPROVED == status) {
            allocationSubmission.setApproveUserId(getUserId());
            allocationSubmission.setApprovedAt(LocalDateTime.now());
            allocationSubmission.getAllocationTimes().forEach(at -> {
                if (!CollectionUtils.isEmpty(at.getCostCodePercs())) {
                    at.getCostCodePercs().stream()
                            .max(Comparator
                                    .comparing(CostCodePerc::getTotalHour)
                                    .thenComparing(CostCodePerc::getCostCodeFull))
                            .ifPresent(p -> at.setMaxTimeCostCode(p.getCostCodeFull()));
                }
            });
            Set<String> costCodes = allocationSubmission.getAllocationTimes().stream()
                    .map(AllocationTime::getCostCodePercs)
                    .filter(s -> !CollectionUtils.isEmpty(s))
                    .flatMap(Collection::stream)
                    .map(CostCodePerc::getCostCodeFull)
                    .collect(Collectors.toSet());

            costCodes.addAll(allocationSubmission.getAllocationTimes().stream()
                    .map(AllocationTime::getMobCostCode)
                    .filter(StringUtils::hasText)
                    .collect(Collectors.toSet()));

            costCodeService.lockCostCodes(costCodes, allocationSubmission.getProjectId());

            if (!CollectionUtils.isEmpty(allocationTimeSet)) {
                Set<String> employeeIds = allocationTimeSet.stream()
                        .map(AllocationTime::getEmpId).collect(Collectors.toSet());
                LocalDate dateOfService = allocationSubmission.getDateOfService();

                LocalDate startOfWeek = rule.getStartOfWeek(dateOfService);
                LocalDate endOfWeek = rule.getEndOfWeek(dateOfService);

                int roundingMinutes = rule.getRoundingMinutes();
                String roundingMode = rule.getRoundingMode();
                BigDecimal hoursToBePlus = roundingMode.equals("ROUNDING_TO_NEAREST_NTH_MINUTE") ?
                        BigDecimal.valueOf(roundingMinutes).divide(BigDecimal.valueOf(120), 3, RoundingMode.DOWN) :
                        roundingMode.equals("ROUNDING_UP") ?
                                BigDecimal.valueOf(roundingMinutes).divide(BigDecimal.valueOf(60), 3, RoundingMode.DOWN) :
                                BigDecimal.ZERO;
                BigDecimal roundingHours = BigDecimal.valueOf(roundingMinutes)
                        .divide(BigDecimal.valueOf(60), 2, RoundingMode.DOWN);

                Set<AllocationTime> oldAllocationTimeList = mContentService
                        .getAllocationTimesForThisWeek(allocationSubmission.getProjectId(), startOfWeek, endOfWeek,
                                employeeIds);
                Map<String, BigDecimal> empStHourMap = oldAllocationTimeList.stream()
                        .collect(toMap(AllocationTime::getEmpId, AllocationTime::getStHour, BigDecimal::add));

                allocationTimeSet.forEach(
                        at -> {
                            Set<CostCodePerc> costCodePercSet = at.getCostCodePercs();
                            if (!CollectionUtils.isEmpty(costCodePercSet)) {
                                costCodePercSet.forEach(ccp -> {
                                    BigDecimal totalHour = BigDecimal.ZERO;
                                    ccp.setStHour(RuleUtils.roundTime(ccp.getStHour(), roundingHours, hoursToBePlus));
                                    totalHour = totalHour.add(ccp.getStHour());
                                    if (ccp.getOtHour() != null) {
                                        ccp.setOtHour(RuleUtils.roundTime(ccp.getOtHour(), roundingHours, hoursToBePlus));
                                        totalHour = totalHour.add(ccp.getOtHour());
                                    }
                                    if (ccp.getDtHour() != null) {
                                        ccp.setDtHour(RuleUtils.roundTime(ccp.getDtHour(), roundingHours, hoursToBePlus));
                                        totalHour = totalHour.add(ccp.getDtHour());
                                    }
                                    ccp.setTotalHour(totalHour);
                                });
                                BigDecimal allocatedHour = costCodePercSet.stream()
                                        .map(ccp -> {
                                            BigDecimal stHour = ccp.getStHour() == null ? BigDecimal.ZERO : ccp.getStHour();
                                            BigDecimal otHour = ccp.getOtHour() == null ? BigDecimal.ZERO : ccp.getOtHour();
                                            BigDecimal dtHour = ccp.getDtHour() == null ? BigDecimal.ZERO : ccp.getDtHour();
                                            return stHour.add(otHour).add(dtHour);
                                        }).reduce(BigDecimal.ZERO, BigDecimal::add);
                                at.setAllocatedHour(allocatedHour);
                                allocatedHour = RuleUtils.roundTime(allocatedHour, roundingHours, hoursToBePlus);
                                BigDecimal previousAccumulatedStHours = empStHourMap.getOrDefault(at.getEmpId(), BigDecimal.ZERO);
                                BigDecimal totalAllocated = allocatedHour.add(previousAccumulatedStHours);
                                BigDecimal overTime = totalAllocated.compareTo(BigDecimal.valueOf(40)) <= 0 ?
                                        BigDecimal.ZERO : totalAllocated.subtract(BigDecimal.valueOf(40));
                                if (overTime.compareTo(BigDecimal.ZERO) > 0) {
                                    // has overtime
//                            overTime = RuleUtils.roundTime(overTime, roundingHours, hoursToBePlus);
                                    at.setOtHour(overTime);
                                    List<CostCodePerc> costCodePercList = new ArrayList<>(costCodePercSet);
                                    BigDecimal remainingOTHour = overTime;
                                    for (int i = 0; i < costCodePercList.size() - 1; i++) {
                                        CostCodePerc ccp = costCodePercList.get(i);
                                        BigDecimal ccpOt = overTime.multiply(BigDecimal.valueOf(ccp.getPercentage()))
                                                .divide(BigDecimal.valueOf(100), 2, RoundingMode.DOWN);
                                        ccpOt = RuleUtils.roundTime(ccpOt, roundingHours, hoursToBePlus);
                                        ccp.setOtHour(ccpOt);
                                        ccp.setStHour(BigDecimal.ZERO.max(ccp.getStHour().subtract(ccpOt)));
                                        remainingOTHour = BigDecimal.ZERO.max(remainingOTHour.subtract(ccpOt));
                                    }
                                    CostCodePerc lastCostCodePerc = costCodePercList.get(costCodePercList.size() - 1);
                                    lastCostCodePerc.setOtHour(remainingOTHour);
                                    lastCostCodePerc.setStHour(BigDecimal.ZERO.max(lastCostCodePerc.getStHour()
                                            .subtract(remainingOTHour)));
                                }
                                BigDecimal standardTime = BigDecimal.ZERO.max(allocatedHour.subtract(overTime));
                                // standardTime could be negative here. If many overtime was not calculated before
                                at.setStHour(standardTime);
                            }
                        }
                );
            }
        } else if (AllocationSubmissionStatus.PENDING == status) {
            allocationSubmission.setApproveUserId(null);
            allocationSubmission.setApprovedAt(null);
        }
        allocationSubmission.setAllocationTimes(allocationTimeSet);
        if (!CollectionUtils.isEmpty(allocationTimeSet)) {
            // Ignore empty time allocation
            LocalDate payrollDate = allocationSubmission.getPayrollDate();
            LocalDate previousPayrollDate = payrollDate.minusDays(7);
            WeeklyProcessType weeklyProcessType;
            if (allocationSubmission.getDateOfService().isAfter(previousPayrollDate)) {
                weeklyProcessType = WeeklyProcessType.NORMAL;
            } else {
                weeklyProcessType = WeeklyProcessType.TLO;
            }
            if (AllocationSubmissionStatus.APPROVED == status) {
                WeeklyProcess weeklyProcess = weeklyProcessService.findOrGenerateWeeklyProcess(allocationSubmission.getProjectId(), payrollDate, weeklyProcessType);
                allocationTimeSet.forEach(t -> t.setWeeklyProcessId(weeklyProcess.getId()));
            }
            if (allocationSubmissionDto.getPreviousStatus() == AllocationSubmissionStatus.APPROVED
                    && AllocationSubmissionStatus.PENDING == status) {
                allocationTimeSet.forEach(t -> t.setWeeklyProcessId(null));
            }
            mContentService.saveAllocationSubmission(allocationSubmission);
            if (AllocationSubmissionStatus.APPROVED == status
                    || (allocationSubmissionDto.getPreviousStatus() == AllocationSubmissionStatus.APPROVED
                    && AllocationSubmissionStatus.PENDING == status)) {
                weeklyProcessService.calculateCost(allocationSubmission.getProjectId(), null, payrollDate, weeklyProcessType);
            }
        }
        return RESPONSE_SUCCESS;
    }

    @DeleteMapping(value = "/allocation-submission", params = "allocationSubmissionId")
    public String deleteAllocationSubmission(ModelMap model,
                                             HttpServletRequest request,
                                             @RequestParam Long allocationSubmissionId) {
        Long projectId = (Long) model.get("sessionProjectId");

        AllocationSubmission allocationSubmission = mContentService.getAllocationSubmissionById(allocationSubmissionId);
        String team = allocationSubmission.getTeamName();
        LocalDate dateOfService = allocationSubmission.getDateOfService();
        mContentService.deleteStageTransactionsByTeamAndDateOfService(team, dateOfService, projectId);
        mContentService.deleteAllocationSubmissionById(allocationSubmissionId);
        String referer = request.getHeader("Referer");
        return "redirect:" + referer;
    }

    @DeleteMapping(value = "/allocation-submission", params = {"dateOfService"})
    public String deleteStageTransactions(ModelMap model,
                                          HttpServletRequest request,
                                          @RequestParam(required = false) String teamName,
                                          @RequestParam LocalDate dateOfService) {
        Long projectId = (Long) model.get("sessionProjectId");
        mContentService.deleteStageTransactionsByTeamAndDateOfService(teamName, dateOfService, projectId);
        String referer = request.getHeader("Referer");
        return "redirect:" + referer;
    }

    @PreAuthorize("hasRole('ROLE_foreman') or hasRole('ROLE_accountant_foreman')")
    @GetMapping(value = "/export")
    public void export(@RequestParam Long allocationSubmissionId, HttpServletResponse response) {
        mExportAPI.exportByAllocationSubmissionId(allocationSubmissionId, response);
    }
}

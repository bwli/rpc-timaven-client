package com.timaven.timekeeping.controller;

import com.timaven.timekeeping.model.User;
import com.timaven.timekeeping.model.dto.Principal;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import java.util.Collections;
import java.util.Map;

class BaseController {
    final static String RESPONSE_SUCCESS = "success";
    final static String RESPONSE_FAILED = "failed";

    Long getUserId() {
        Object principal = SecurityContextHolder.getContext()
                .getAuthentication().getPrincipal();
        return ((Principal) principal).getId();
    }

    String getUsername() {
        Object principal = SecurityContextHolder.getContext()
                .getAuthentication().getPrincipal();
        return ((Principal) principal).getUsername();
    }

    /**
     * Convert request parameters to hash map
     */
    JSONObject requestParamsToJSON(MultipartHttpServletRequest req) {
        JSONObject jsonObj = new JSONObject();

        Map<String, String[]> params = req.getParameterMap();
        for (Map.Entry<String, String[]> entry : params.entrySet()) {
            String[] v = entry.getValue();
            for (int i = 0; i < v.length; i++) {
                v[i] = v[i].replaceAll("<br />", "\n");
            }
            if (v.length == 1) {
                jsonObj.put(entry.getKey(), v[0]);
            } else {
                jsonObj.put(entry.getKey(), new JSONArray(Collections.singletonList(v)));
            }
        }
        return jsonObj;
    }
}

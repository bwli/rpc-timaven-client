package com.timaven.timekeeping.controller;

import com.timaven.timekeeping.model.*;
import com.timaven.timekeeping.service.AccountService;
import com.timaven.timekeeping.service.BillingCodeService;
import com.timaven.timekeeping.service.CostCodeService;
import com.timaven.timekeeping.service.ProjectService;
import com.timaven.timekeeping.validator.ProjectValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.*;

@Controller
@PreAuthorize("hasRole('ROLE_admin')")
public class AdminController extends BaseController {

    private final ProjectService mProjectService;
    private final AccountService mAccountService;
    private final BillingCodeService billingCodeService;
    private final CostCodeService costCodeService;
    private final ProjectValidator projectValidator;

    @Autowired
    public AdminController(ProjectService projectService, AccountService accountService,
                           BillingCodeService billingCodeService, CostCodeService costCodeService,
                           ProjectValidator projectValidator) {
        mProjectService = projectService;
        mAccountService = accountService;
        this.billingCodeService = billingCodeService;
        this.costCodeService = costCodeService;
        this.projectValidator = projectValidator;
    }

    @InitBinder("project")
    protected void initProjectBinder(WebDataBinder binder) {
        binder.addValidators(projectValidator);
    }

    @GetMapping(value = "/projects")
    public ModelAndView home() {
        ModelAndView modelAndView = new ModelAndView("projects");
        List<Project> projects = mProjectService.getAllProjectsFetchUserProjects().stream().sorted().collect(toList());
        Set<User> users = mAccountService.getProjectManagerUsers();
        Map<Long, User> idUserMap = users.stream().collect(Collectors.toMap(User::getId,
                Function.identity()));
        modelAndView.addObject("projects", projects);
        modelAndView.addObject("idUserMap", idUserMap);
        return modelAndView;
    }

    @GetMapping(value = {"/project"})
    public ModelAndView project(@RequestParam(value = "projectId", required = false) Long projectId,
                                ModelMap modelMap) {
        ModelAndView modelAndView = new ModelAndView("project");
        Project project;

        List<Project> allProjects = mProjectService.getAllProjectsFetchUserProjects();
        if (null == projectId) {
            project = new Project();
        } else {
            project = allProjects.stream().filter(p -> projectId.equals(p.getId())).findFirst().orElse(new Project());
        }

        modelAndView.addObject("projects", allProjects);
        modelAndView.addObject("project", project);
        bindProjectAttributes(modelMap, allProjects);
        return modelAndView;
    }

    @PostMapping(value = "/project")
    public String saveProject(@Validated @ModelAttribute("project") Project project, BindingResult bindingResult, ModelMap modelMap) {
        if (bindingResult.hasErrors()) {
            List<Project> allProjects = mProjectService.getAllProjectsFetchUserProjects();
            bindProjectAttributes(modelMap, allProjects);
            return "project";
        } else {
            mProjectService.saveProject(project);
            return rdAdminHome();
        }
    }

    private void bindProjectAttributes(ModelMap modelMap, List<Project> allProjects) {

        List<User> users = mAccountService.getUsersForProject();
        Map<Long, Project> idProjectMap = allProjects.stream().collect(Collectors.toMap(Project::getId,
                Function.identity()));

        Set<UserProject> userProjects = allProjects.stream()
                .map(Project::getUserProjects).flatMap(Collection::stream).collect(Collectors.toCollection(TreeSet::new));
        Map<Long, String> userIdProjectNameMap = userProjects.stream()
                .collect(groupingBy(UserProject::getUserId,
                        mapping(up -> idProjectMap.get(up.getProjectId()).getDescription(),
                                joining(", ", "  (assigned to [", "])"))));
        Map<String, Long> projectManagerNameIdMap = new LinkedHashMap<>();
        Map<String, Long> otherNameIdMap = new LinkedHashMap<>();
        List<User> projectManagerUsers = users.stream()
                .filter(u -> u.getUserRoles().stream()
                        .map(UserRole::getRole).anyMatch(r -> r.getName().equals("ROLE_project_manager")))
                .collect(Collectors.toList());
        List<User> otherUsers = users.stream()
                .filter(u -> !projectManagerUsers.contains(u)).collect(Collectors.toList());
        projectManagerUsers.forEach(
                l -> {
                    if (userIdProjectNameMap.containsKey(l.getId())) {
                        projectManagerNameIdMap.put(l.getUsername() + userIdProjectNameMap.get(l.getId()), l.getId());
                    } else {
                        projectManagerNameIdMap.put(l.getUsername(), l.getId());
                    }
                }
        );
        otherUsers.forEach(
                l -> {
                    if (userIdProjectNameMap.containsKey(l.getId())) {
                        otherNameIdMap.put(l.getUsername() + userIdProjectNameMap.get(l.getId()), l.getId());
                    } else {
                        otherNameIdMap.put(l.getUsername(), l.getId());
                    }
                }
        );
        modelMap.put("projectManagerNameIdMap", projectManagerNameIdMap);
        modelMap.put("otherNameIdMap", otherNameIdMap);
    }

    @DeleteMapping(value = "/project")
    public String deleteProject(@RequestParam Long id) {
        mProjectService.deleteProject(id);
        return rdAdminHome();
    }

    @DeleteMapping(value = "/billing-code-type")
    public String deleteBillingCodeType(@RequestParam Long id) {
        billingCodeService.deleteBillingCodeTypeById(id);
        return "redirect:code-segments";
    }

    @GetMapping(value = "/code-segment")
    public ModelAndView codeSegment(@RequestParam(required = false) Long id) {
        ModelAndView modelAndView = new ModelAndView("code-segment");
        BillingCodeType billingCodeType;
        if (id == null) {
            billingCodeType = new BillingCodeType();
        } else {
            billingCodeType = billingCodeService.findBillingCodeTypeById(id);
        }
        modelAndView.addObject("billingCodeType", billingCodeType);
        return modelAndView;
    }

    @RequestMapping(value = "/code-segment", method = {RequestMethod.POST, RequestMethod.PUT})
    public String addOrUpdateBillingCode(@ModelAttribute("billingCodeType") BillingCodeType billingCodeType) {
        billingCodeService.saveBillingCodeType(billingCodeType);
        return "redirect:code-segments";
    }

    @GetMapping(value = "/cost-code-structure")
    public ModelAndView getCostCodeStructure() {
        ModelAndView modelAndView = new ModelAndView("cost-code-structure");

        List<BillingCodeType> sortedTypes = billingCodeService.findBillingCodeTypesSortByLevel();
        List<BillingCodeType> allTypes = billingCodeService.findAllBillingCodeTypes();
        List<BillingCodeType> otherTypes = allTypes.stream()
                .filter(t -> !sortedTypes.contains(t)).collect(toList());
        modelAndView.addObject("sortedTypes", sortedTypes);
        modelAndView.addObject("otherTypes", otherTypes);

        return modelAndView;
    }

    private String rdAdminHome() {
        return "redirect:home-admin";
    }
}

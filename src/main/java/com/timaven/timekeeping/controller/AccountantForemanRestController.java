package com.timaven.timekeeping.controller;

import com.timaven.timekeeping.model.CostCodeTag;
import com.timaven.timekeeping.model.dto.*;
import com.timaven.timekeeping.service.ContentService;
import com.timaven.timekeeping.service.CostCodeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.ui.ModelMap;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@RestController
@SessionAttributes({"sessionProjectId", "allowedDayOfWeek", "rule", "sessionTeamName", "sessionTimesheetsMode", "sessionTimesheetsDate"})
@PreAuthorize("hasAnyRole('ROLE_foreman', 'ROLE_accountant', 'ROLE_accountant_foreman', 'ROLE_corporate_accountant'," +
        " 'ROLE_timekeeper')")
public class AccountantForemanRestController extends BaseController {

    private final CostCodeService costCodeService;
    private final ContentService contentService;

    @Autowired
    public AccountantForemanRestController(CostCodeService costCodeService, ContentService contentService) {
        this.costCodeService = costCodeService;
        this.contentService = contentService;
    }

    @PostMapping(value = "/cost-code-tags")
    public List<CostCodeTag> getCostCodeTags(@RequestParam Long typeId) {
        return costCodeService.findCostCodeTagsByTypeId(typeId);
    }

    @GetMapping(value = "/last-cost-code-tags")
    public PreviousCostCodeTagDto getPreviousCostCodeTagDto(@ModelAttribute("sessionProjectId") Long projectId,
                                                            @RequestParam String teamName,
                                                            @RequestParam LocalDate dateOfService) {
        PreviousCostCodeTagDto dto = new PreviousCostCodeTagDto();
        AllocationSubmissionDto allocationSubmissionDto = contentService.getLastAllocationSubmissionDto(projectId, teamName, dateOfService);
        if (!CollectionUtils.isEmpty(allocationSubmissionDto.getAllocationTimes())) {
            Set<CostCodeTagDto> costCodeTagDtos = contentService.getDefaultCostCodeTagDtos(allocationSubmissionDto);
            dto.setCostCodeTagDtos(costCodeTagDtos);
            Map<String, List<CostCodeTagAssociationDto>> empIdPerDiemCostCodeTagAssociationsMap = new HashMap<>();
            Map<String, List<CostCodeTagAssociationDto>> empIdMobCostCodeTagAssociationsMap = new HashMap<>();
           allocationSubmissionDto.getAllocationTimes().forEach(t -> {
                empIdPerDiemCostCodeTagAssociationsMap.put(t.getEmpId(), t.getPerDiemCostCodeTagAssociations());
            });
            allocationSubmissionDto.getAllocationTimes().forEach(t -> {
                empIdMobCostCodeTagAssociationsMap.put(t.getEmpId(), t.getMobCostCodeTagAssociations());
            });
            dto.setEmpIdPerDiemCostCodeTagAssociationsMap(empIdPerDiemCostCodeTagAssociationsMap);
            dto.setEmpIdMobCostCodeTagAssociationsMap(empIdMobCostCodeTagAssociationsMap);
            Map<String, String> empIdPerDiemCostCodeMap = new HashMap<>();
            Map<String, String> empIdMobCostCodeMap = new HashMap<>();
            allocationSubmissionDto.getAllocationTimes()
                    .stream().filter(AllocationTimeDto::isHasPerDiem)
                    .forEach(t -> {
                        empIdPerDiemCostCodeMap.put(t.getEmpId(), t.getPerDiemCostCode());
                    });
            allocationSubmissionDto.getAllocationTimes()
                    .stream().filter(AllocationTimeDto::isHasMob)
                    .forEach(t -> {
                        empIdMobCostCodeMap.put(t.getEmpId(), t.getMobCostCode());
                    });
            dto.setEmpIdPerDiemCostCodeMap(empIdPerDiemCostCodeMap);
            dto.setEmpIdMobCostCodeMap(empIdMobCostCodeMap);
        }
        return dto;
    }

    @DeleteMapping(value = "/allocation-times")
    public void deleteAllocationTime(ModelMap model,
                                     @RequestParam(required = false) Long id,
                                     @RequestParam(required = false) String empId,
                                     @RequestParam(required = false) LocalDate dateOfService,
                                     @RequestParam(required = false) String teamName) {
        if (id != null) {
            contentService.deleteAllocationTimeById(id);
        } else {
            Long projectId = (Long) model.get("sessionProjectId");
            contentService.deleteStageTransactionsByProjectIdEmpIdAndTeamAndDateOfService(projectId, empId, teamName, dateOfService);
        }
    }
}

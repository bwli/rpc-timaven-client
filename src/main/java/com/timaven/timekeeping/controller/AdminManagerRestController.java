package com.timaven.timekeeping.controller;

import com.timaven.timekeeping.model.dto.BillingCodeDto;
import com.timaven.timekeeping.model.dto.CostCodeAssociationDto;
import com.timaven.timekeeping.model.dto.SaveCostCodeAssociationDto;
import com.timaven.timekeeping.model.paging.Page;
import com.timaven.timekeeping.model.paging.PagingRequest;
import com.timaven.timekeeping.service.BillingCodeService;
import com.timaven.timekeeping.service.CostCodeService;
import com.timaven.timekeeping.service.EmployeeService;
import com.timaven.timekeeping.service.EquipmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@SessionAttributes({"sessionProjectId"})
@PreAuthorize("hasAnyRole('ROLE_admin', 'ROLE_project_manager')")
public class AdminManagerRestController extends BaseController {

    private final BillingCodeService billingCodeService;
    private final CostCodeService costCodeService;
    private final EmployeeService employeeService;
    private final EquipmentService equipmentService;

    @Autowired
    public AdminManagerRestController(BillingCodeService billingCodeService, CostCodeService costCodeService, EmployeeService employeeService, EquipmentService equipmentService) {
        this.billingCodeService = billingCodeService;
        this.costCodeService = costCodeService;
        this.employeeService = employeeService;
        this.equipmentService = equipmentService;
    }

    @PreAuthorize("hasAnyRole('ROLE_admin', 'ROLE_project_manager')")
    @PostMapping(value = "/billing-codes", params = "billingCodeTypeId")
    public Page<BillingCodeDto> getBillingCodes(ModelMap model,
                                                @RequestBody PagingRequest pagingRequest,
                                                @RequestParam Long billingCodeTypeId) {
        Long projectId = (Long) model.get("sessionProjectId");
        return billingCodeService.findBillingCodesByTypeId(billingCodeTypeId, pagingRequest, projectId);
    }

    @PreAuthorize("hasAnyRole('ROLE_admin', 'ROLE_project_manager')")
    @PostMapping(value = "/billing-codes", params = "id")
    public List<BillingCodeDto> getBillingCodes(ModelMap model, @RequestParam Long id) {
        Long projectId = (Long) model.get("sessionProjectId");
        return billingCodeService.findBillingCodesByTypeId(id, projectId);
    }

    @PreAuthorize("hasAnyRole('ROLE_admin', 'ROLE_project_manager', 'ROLE_timekeeper')")
    @PostMapping(value = "/cost-code-columns")
    public Map<String, String> getCostCodeColumns(ModelMap model) {
        Long projectId = (Long) model.get("sessionProjectId");
        return costCodeService.findCostCodeColumns(projectId);
    }

    @PreAuthorize("hasAnyRole('ROLE_admin', 'ROLE_project_manager', 'ROLE_timekeeper')")
    @PostMapping(value = "/cost-codes")
    public Page<CostCodeAssociationDto> getCostCodes(ModelMap model, @RequestBody PagingRequest pagingRequest) {
        Long projectId = (Long) model.get("sessionProjectId");
        return costCodeService.findCostCodeAssociations(pagingRequest, projectId);
    }

    @PreAuthorize("hasAnyRole('ROLE_admin', 'ROLE_project_manager')")
    @PostMapping(value = "/cost-code-association")
    public String saveBillingCode(ModelMap model,
                                  @RequestBody SaveCostCodeAssociationDto dto) {
        Long projectId = (Long) model.get("sessionProjectId");
        billingCodeService.saveCostCodeBillingCode(projectId, dto);
        return "success";
    }

    @PreAuthorize("hasAnyRole('ROLE_admin', 'ROLE_project_manager')")
    @PostMapping(value = "/cost-code-log-tags")
    public String saveCostCodeLogTag(ModelMap model,
                                     @RequestParam String costCode,
                                     @RequestParam String tag) {
        Long projectId = (Long) model.get("sessionProjectId");
        costCodeService.saveCostCodeLogTag(projectId, costCode, tag);
        return RESPONSE_SUCCESS;
    }

    @PreAuthorize("hasAnyRole('ROLE_admin', 'ROLE_project_manager')")
    @DeleteMapping(value = "/cost-code-log-tags")
    public String deleteCostCodeLogTag(ModelMap model,
                                       @RequestParam String costCode,
                                       @RequestParam String tag) {
        Long projectId = (Long) model.get("sessionProjectId");
        costCodeService.deleteCostCodeLogTag(projectId, costCode, tag);
        return RESPONSE_SUCCESS;
    }

    @PreAuthorize("hasAnyRole('ROLE_admin', 'ROLE_project_manager')")
    @PostMapping(value = "/equipment-tags")
    public String saveEquipmentTag(@RequestParam String equipmentId,
                                   @RequestParam String tag) {
        equipmentService.saveEquipmentTag(equipmentId, tag);
        return RESPONSE_SUCCESS;
    }

    @PreAuthorize("hasAnyRole('ROLE_admin', 'ROLE_project_manager')")
    @DeleteMapping(value = "/equipment-tags")
    public String deleteEquipmentTag(@RequestParam String equipmentId,
                                     @RequestParam String tag) {
        equipmentService.deleteEquipmentTag(equipmentId, tag);
        return RESPONSE_SUCCESS;
    }
}

package com.timaven.timekeeping.controller;

import com.timaven.timekeeping.service.WeeklyProcessService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.time.LocalDate;


@Controller
@PreAuthorize("hasAnyRole('ROLE_admin', 'ROLE_corporate_accountant', 'ROLE_corporate_timekeeper')")
public class AdminCorporateAccountantController extends BaseController {

    private final WeeklyProcessService weeklyProcessService;

    @Autowired
    public AdminCorporateAccountantController(WeeklyProcessService weeklyProcessService) {
        this.weeklyProcessService = weeklyProcessService;
    }


    @GetMapping(value = {"/home-corporate-accountant", "/home-admin"})
    public ModelAndView accountantHome() {
        ModelAndView modelAndView = new ModelAndView("projects-dashboard");
        return modelAndView;
    }

    @PreAuthorize("hasAnyRole('ROLE_admin', 'ROLE_corporate_accountant')")
    @GetMapping(value = "/weekly-payroll")
    public ModelAndView weeklyPayroll(@RequestParam LocalDate weekEndDate) {
        ModelAndView modelAndView = new ModelAndView("weekly-payroll");
        modelAndView.addObject("weekEndDate", weekEndDate);
        return modelAndView;
    }
}

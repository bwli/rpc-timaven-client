package com.timaven.timekeeping.controller;

import com.timaven.timekeeping.model.*;
import com.timaven.timekeeping.model.dto.RequisitionDto;
import com.timaven.timekeeping.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Controller
@SessionAttributes({"sessionProjectId", "allowedDayOfWeek", "rule", "sessionTeamName", "sessionTimesheetsMode", "sessionTimesheetsDate"})
@PreAuthorize("hasAnyRole('ROLE_corporate_accountant', 'ROLE_accountant', 'ROLE_accountant_foreman', 'ROLE_timekeeper', 'ROLE_purchasing')")
public class CorporateProjectAccountantController extends BaseController {

    private final PurchaseOrderService purchaseOrderService;
    private final CraftService craftService;
    private final ProjectService projectService;
    private final PerDiemService perDiemService;
    private final CostCodeService costCodeService;
    // TM-233 add rule service
    private final RuleService ruleService;

    @Autowired
    public CorporateProjectAccountantController(PurchaseOrderService purchaseOrderService,
                                                CraftService craftService, ProjectService projectService,
                                                PerDiemService perDiemService, CostCodeService costCodeService,
                                                RuleService ruleService) {
        this.purchaseOrderService = purchaseOrderService;
        this.craftService = craftService;
        this.projectService = projectService;
        this.perDiemService = perDiemService;
        this.costCodeService = costCodeService;
        // TM-233 add rule service
        this.ruleService = ruleService;
    }

    @GetMapping(value = "/purchase-orders")
    public ModelAndView getPurchaseOrders() {
        return new ModelAndView("purchase-orders");
    }

    @GetMapping(value = "/purchase-vendor-roster")
    public ModelAndView getPurchaseVendorRoster() {
        return new ModelAndView("purchase-vendor-roster");
    }

    @GetMapping(value = "/purchase-requisitions")
    public ModelAndView getPurchaseRequisitions() {
        return new ModelAndView("purchase-requisitions");
    }

    @GetMapping(value = "/purchase-order-details")
    public ModelAndView getPurchaseOrderDetails(@RequestParam String purchaseOrderNumber) {
        ModelAndView modelAndView = new ModelAndView("purchase-order-details");
        modelAndView.addObject("purchaseOrderNumber", purchaseOrderNumber);
        return modelAndView;
    }

    @GetMapping(value = "/requisition")
    public ModelAndView requisition(ModelMap model, @RequestParam(required = false) String requisitionNumber) {
        ModelAndView modelAndView = new ModelAndView("requisition");
        RequisitionDto requisitionDto;
        List<CostCodeLog> costCodeLogSet = new ArrayList<>();
        Long projectId = (Long) model.get("sessionProjectId");
        if (requisitionNumber == null) {
            requisitionDto = new RequisitionDto();
            // Only project level user can add new requisition
            assert projectId != null;
            Project project = projectService.getProjectById(projectId);
            if (!StringUtils.hasText(project.getJobNumber()) || !StringUtils.hasText(project.getSubJob())) {
                modelAndView = new ModelAndView("operation-error");
                modelAndView.addObject("errorMsg", "Job # or Sub Job # has not been set.");
                modelAndView.addObject("solution", "Please set the Job Number and SubJob first.");
                return modelAndView;
            }
            requisitionNumber = purchaseOrderService.generateRequisitionNumber(project);
            requisitionDto.setRequisitionNumber(requisitionNumber);
        } else {
            requisitionDto = purchaseOrderService.getRequisitionDto(requisitionNumber);
        }
        if (projectId != null) {
            costCodeLogSet =
                    costCodeService.getAllCostCodeLogsByProjectIdAndDateOfService(projectId, LocalDate.now());
        }
        modelAndView.addObject("costCodeLogs", costCodeLogSet);
        modelAndView.addObject("requisitionDto", requisitionDto);
        return modelAndView;
    }

    @GetMapping(value = "/crafts")
    public ModelAndView getCrafts(ModelMap model) {
        Long projectId = (Long) model.get("sessionProjectId");
        ModelAndView modelAndView = new ModelAndView("craft-list");
        List<PerDiem> perDiemList = perDiemService.getPerDiems(projectId);
        modelAndView.addObject("perDiems", perDiemList);
        // TM-233 add rule info
        Rule rule = (Rule) model.get("rule");
        if(rule == null){
            rule = ruleService.getRule(projectId).orElse(null);
        }
        modelAndView.addObject("rule", rule);
        return modelAndView;
    }

    @PreAuthorize("hasAnyRole('ROLE_accountant', 'ROLE_accountant_foreman', 'ROLE_corporate_accountant')")
    @GetMapping(value = "/craft")
    public ModelAndView craft(@RequestParam(value = "id", required = false) Long id, ModelMap model) {
        Long projectId = (Long) model.get("sessionProjectId");
        Craft craft;
        if (id == null) {
            craft = new Craft();
            craft.setProjectId(projectId);
        } else {
            craft = craftService.findById(id);
        }
        ModelAndView modelAndView = new ModelAndView("craft", "craft", craft);
        List<PerDiem> perDiemList = perDiemService.getPerDiems(projectId);
        modelAndView.addObject("perDiems", perDiemList);
        return modelAndView;
    }

    @PreAuthorize("hasAnyRole('ROLE_accountant', 'ROLE_accountant_foreman', 'ROLE_corporate_accountant')")
    @RequestMapping(value = "/craft", method = {RequestMethod.POST, RequestMethod.PUT})
    public String addOrUpdateCraft(@ModelAttribute("craft") Craft craft) {
        craftService.saveCraft(craft);
        return "redirect:crafts";
    }
}

package com.timaven.timekeeping.controller;

import com.timaven.timekeeping.model.Employee;
import com.timaven.timekeeping.model.EmployeeFilter;
import com.timaven.timekeeping.model.Project;
import com.timaven.timekeeping.model.Rule;
import com.timaven.timekeeping.model.dto.ProjectPayrollDto;
import com.timaven.timekeeping.model.dto.RequisitionDto;
import com.timaven.timekeeping.model.paging.Page;
import com.timaven.timekeeping.model.paging.PagingRequest;
import com.timaven.timekeeping.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@RestController
@SessionAttributes({"sessionProjectId", "allowedDayOfWeek", "rule", "sessionTeamName", "sessionTimesheetsMode", "sessionTimesheetsDate"})
@PreAuthorize("hasAnyRole('ROLE_accountant', 'ROLE_accountant_foreman', 'ROLE_timekeeper')")
public class AccountantRestController extends BaseController {

    private final ContentService contentService;
    private final RuleService mRuleService;
    private final PurchaseOrderService purchaseOrderService;
    private final PayrollService payrollService;
    private final WeeklyProcessService weeklyProcessService;
    private final UserProjectService userProjectService;
    private final EmployeeService employeeService;

    @Autowired
    public AccountantRestController(ContentService contentService, RuleService mRuleService,
                                    PurchaseOrderService purchaseOrderService, PayrollService payrollService,
                                    WeeklyProcessService weeklyProcessService, UserProjectService userProjectService, EmployeeService employeeService) {
        this.contentService = contentService;
        this.mRuleService = mRuleService;
        this.purchaseOrderService = purchaseOrderService;
        this.payrollService = payrollService;
        this.weeklyProcessService = weeklyProcessService;
        this.userProjectService = userProjectService;
        this.employeeService = employeeService;
    }

    @GetMapping(value = "/sign-in-sheets")
    public Set<String> getSignInSheets(@RequestParam LocalDate dateOfService,
                                    @ModelAttribute("sessionProjectId") Long projectId) {
        return contentService.getSignInSheets(projectId, dateOfService);
    }

    @GetMapping(value = "/payroll-date")
    public ResponseEntity<String> getPayrollDate(@RequestParam LocalDate dateOfService,
                                                @ModelAttribute("sessionProjectId") Long projectId) {
        Optional<Rule> ruleOpt = mRuleService.getRule(projectId);
        return ruleOpt.map(r -> ResponseEntity.ok().body(r.getEndOfWeek(dateOfService).format(DateTimeFormatter.ofPattern("MM/dd/yyyy"))))
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

    @GetMapping(value = "/last-week-end-date")
    public ResponseEntity<String> getPayrollDate(@ModelAttribute("sessionProjectId") Long projectId) {
        Optional<Rule> ruleOpt = mRuleService.getRule(projectId);
        return ruleOpt.map(r -> ResponseEntity.ok().body(r.getEndOfWeek(LocalDate.now().minusWeeks(1)).format(DateTimeFormatter.ofPattern("MM/dd/yyyy"))))
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

    @PreAuthorize("hasAnyRole('ROLE_accountant', 'ROLE_accountant_foreman', 'ROLE_purchasing')")
    @PostMapping(value = "/requisition")
    public String requisition(@ModelAttribute("sessionProjectId") Long projectId,
                              @RequestBody RequisitionDto requisitionDto) {
        purchaseOrderService.deleteByRequisitionNumber(requisitionDto.getRequisitionNumber());
        purchaseOrderService.saveRequisition(requisitionDto, projectId);
        return "success";
    }

    @PreAuthorize("hasAnyRole('ROLE_accountant', 'ROLE_accountant_foreman')")
    @PostMapping(value = "/project-payroll")
    public Page<ProjectPayrollDto> getProjectPayrollDto(@RequestBody PagingRequest pagingRequest,
                                                        @ModelAttribute("sessionProjectId") Long projectId) {
        return payrollService.getProjectPayrollPage(projectId, pagingRequest);
    }

    @PreAuthorize("hasAnyRole('ROLE_accountant', 'ROLE_accountant_foreman')")
    @PutMapping(value = "/project-payroll-status")
    public String updateProjectPayrollStatus(@ModelAttribute("sessionProjectId") Long projectId,
                                             @RequestParam LocalDate weekEndDate,
                                             @RequestParam int type) {
        weeklyProcessService.approveWeeklyTimesheets(getUserId(), weekEndDate, projectId, type);
        return "success";
    }

    @PostMapping(value = "/weekly-process-apply-rule")
    public void weeklyProcessApplyRule(@ModelAttribute("sessionProjectId") Long projectId,
                                       @ModelAttribute("rule") Rule rule,
                                       @RequestParam LocalDate weekEndDate) {
        weeklyProcessService.addPerDiemAllocationTimes(projectId, weekEndDate, rule);
    }

    @GetMapping(value = "/my-projects")
    public Set<Project> getMyProjects(@ModelAttribute("sessionProjectId") Long projectId) {
        Set<Project> projects =  userProjectService.getProjectsByUsername(getUsername());
        return projects.stream().filter(p -> !p.getId().equals(projectId)).collect(Collectors.toSet());
    }

    @PostMapping(value = "/duplicate-roster-to-projects")
    public void duplicateRosterToProjects(@ModelAttribute("sessionProjectId") Long projectId,
                                          @RequestBody Set<Long> projectIds) {
        if (CollectionUtils.isEmpty(projectIds)) return;
        EmployeeFilter filter = new EmployeeFilter(projectId, null, null, null, null, LocalDate.now(), null);
        List<Employee> employees = employeeService.getEmployees(filter);
        for (Long otherProjectId : projectIds) {
            List<Employee> employeesToBeAdded = new ArrayList<>();
            employees.forEach(e -> {
                e.setId(null);
                e.setPerDiemId(null);
                e.setProjectId(otherProjectId);
                try {
                    employeesToBeAdded.add((Employee) e.clone());
                } catch (CloneNotSupportedException ex) {
                    ex.printStackTrace();
                }
            });
            // Save for each project to avoid data too big
            employeeService.saveEmployees(employeesToBeAdded);
        }
    }
}

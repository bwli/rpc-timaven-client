package com.timaven.timekeeping.controller;

import com.timaven.timekeeping.model.*;
import com.timaven.timekeeping.model.dto.BillingCodeDto;
import com.timaven.timekeeping.model.dto.EquipmentDto;
import com.timaven.timekeeping.model.enums.EquipmentHourlyType;
import com.timaven.timekeeping.model.enums.EquipmentOwnershipType;
import com.timaven.timekeeping.service.*;
import com.timaven.timekeeping.util.AuthenticationHelper;
import com.timaven.timekeeping.validator.EquipmentValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDate;
import java.time.Period;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

@Controller
@SessionAttributes({"sessionProjectId", "allowedDayOfWeek", "sessionTeamName", "rule"})
@PreAuthorize("hasAnyRole('ROLE_admin', 'ROLE_project_manager', 'ROLE_timekeeper')")
public class AdminManagerController extends BaseController {

    private final RuleService ruleService;
    private final ShiftService mShiftService;
    private final ProjectShiftService mProjectShiftService;
    private final EquipmentService equipmentService;
    private final BillingCodeService billingCodeService;
    private final CostCodeService costCodeService;
    private final EmployeeService employeeService;
    private final ContentService contentService;
    private final AccountService accountService;
    private final PurchaseOrderService purchaseOrderService;
    private final EquipmentValidator equipmentValidator;
    private final PerDiemService perDiemService;

    @Autowired
    public AdminManagerController(RuleService ruleService, ShiftService shiftService,
                                  ProjectShiftService projectShiftService,
                                  EquipmentService equipmentService, BillingCodeService billingCodeService,
                                  CostCodeService costCodeService, EmployeeService employeeService,
                                  ContentService contentService, AccountService accountService,
                                  PurchaseOrderService purchaseOrderService, EquipmentValidator equipmentValidator, PerDiemService perDiemService) {
        this.ruleService = ruleService;
        mShiftService = shiftService;
        mProjectShiftService = projectShiftService;
        this.equipmentService = equipmentService;
        this.billingCodeService = billingCodeService;
        this.costCodeService = costCodeService;
        this.employeeService = employeeService;
        this.contentService = contentService;
        this.accountService = accountService;
        this.purchaseOrderService = purchaseOrderService;
        this.equipmentValidator = equipmentValidator;
        this.perDiemService = perDiemService;
    }

    @InitBinder("equipment")
    protected void initEquipmentBinder(WebDataBinder binder) {
        binder.addValidators(equipmentValidator);
    }

    @PreAuthorize("hasAnyRole('ROLE_admin', 'ROLE_project_manager')")
    @GetMapping(value = "/users")
    public ModelAndView getUsers(ModelMap model) {
        ModelAndView modelAndView = new ModelAndView("users");
        Set<User> users = new HashSet<>();
        if (AuthenticationHelper.hasRole("ROLE_admin")) {
            users = accountService.getUsersForAdmin();
        } else if (AuthenticationHelper.hasRole("ROLE_project_manager")) {
            if (!model.containsAttribute("sessionProjectId")) {
                modelAndView = new ModelAndView("operation-error");
                modelAndView.addObject("errorMsg", "This user does not belong to any project");
                modelAndView.addObject("solution", "Please ask the admin to help");
                return modelAndView;
            }
            Long projectId = (Long) model.get("sessionProjectId");
            users = accountService.getProjectManagerUsersWithRoles(getUsername(), projectId);
        }
        modelAndView.addObject("users", users);
        return modelAndView;
    }

    @PreAuthorize("hasAnyRole('ROLE_admin', 'ROLE_project_manager')")
    @GetMapping(value = "/rule")
    public ModelAndView rule(ModelMap model, @RequestParam(value = "projectId", required = false) Long projectId) {
        if (projectId == null) {
            projectId = (Long) model.get("sessionProjectId");
        }
        Rule rule;
        Optional<Rule> ruleOpt = ruleService.getRule(projectId);
        if (ruleOpt.isEmpty()) {
            rule = new Rule();
            rule.setProjectId(projectId);
        } else {
            rule = ruleOpt.get();
        }
        List<CostCodeLog> costCodes;
        costCodes = costCodeService.getAllCostCodeLogsByProjectIdAndDateOfService(projectId, LocalDate.now()).stream()
                .sorted(Comparator.comparing(CostCodeLog::getCostCodeFull)).collect(Collectors.toList());
        ModelAndView modelAndView = new ModelAndView("rule", "rule", rule);
        List<PerDiem> perDiemList = perDiemService.getPerDiems(projectId);
        modelAndView.addObject("perDiems", perDiemList);
        modelAndView.addObject("costCodes", costCodes);
        return modelAndView;
    }

    @PreAuthorize("hasAnyRole('ROLE_admin', 'ROLE_project_manager')")
    @RequestMapping(value = "/rule", method = {RequestMethod.POST, RequestMethod.PUT})
    public String addOrUpdateRule(ModelMap model, @ModelAttribute("rule") Rule rule) {
        ruleService.saveRule(rule);
        model.put("rule", rule);
        if (AuthenticationHelper.hasRole("ROLE_admin")) {
            return "redirect:projects";
        } else {
            return "redirect:home";
        }
    }

    @PreAuthorize("hasAnyRole('ROLE_admin', 'ROLE_project_manager')")
    @GetMapping(value = "/shifts")
    public ModelAndView shifts(ModelMap model, @RequestParam(value = "projectId", required = false) Long projectId) {
        if (projectId == null) {
            projectId = (Long) model.get("sessionProjectId");
        }
        if (projectId == null) {
            ModelAndView modelAndView = new ModelAndView("error");
            modelAndView.addObject("errorMsg", "Project Id is null");
            modelAndView.addObject("cause", "Couldn't find the project");
            return modelAndView;
        }
        Set<Shift> shifts = mShiftService.getShiftsByProjectId(projectId);
        ModelAndView modelAndView = new ModelAndView("shifts", "shifts", shifts);
        Long defShiftId = mProjectShiftService.getShiftIdByProjectId(projectId);
        modelAndView.addObject("defShiftId", defShiftId);
        modelAndView.addObject("projectId", projectId);
        return modelAndView;
    }

    @PreAuthorize("hasAnyRole('ROLE_admin', 'ROLE_project_manager')")
    @GetMapping(value = "/shift")
    public ModelAndView shift(ModelMap model, @RequestParam(value = "shiftId", required = false) Long shiftId,
                              @RequestParam(value = "projectId", required = false) Long projectId) {
        if (projectId == null) {
            projectId = (Long) model.get("sessionProjectId");
        }
        if (projectId == null) {
            ModelAndView modelAndView = new ModelAndView("error");
            modelAndView.addObject("errorMsg", "Project Id is null");
            modelAndView.addObject("cause", "Couldn't find the project");
            return modelAndView;
        }
        Shift shift;
        if (null == shiftId) {
            shift = new Shift();
            shift.setProjectId(projectId);
        } else {
            shift = mShiftService.getShiftById(shiftId);
        }
        return new ModelAndView("shift", "shift", shift);
    }

    @PreAuthorize("hasAnyRole('ROLE_admin', 'ROLE_project_manager')")
    @RequestMapping(value = "/shift", method = {RequestMethod.POST, RequestMethod.PUT})
    public String addOrUpdateShift(@ModelAttribute("shift") Shift shift) {
        mShiftService.saveShift(shift);
        Set<Shift> shifts = mShiftService.getShiftsByProjectId(shift.getProjectId());
        if (shifts.size() == 1 && !Objects.equals(shifts.iterator().next().getId(), shift.getId())) {
            //Set this shift to default
            ProjectShift projectShift = new ProjectShift();
            projectShift.setProjectId(shift.getProjectId());
            projectShift.setShiftId(shifts.iterator().next().getId());
            mProjectShiftService.saveProjectShift(projectShift);
        }
        if (AuthenticationHelper.hasRole("ROLE_admin")) {
            return rdShiftsOfProject(shift.getProjectId());
        } else if (AuthenticationHelper.hasRole("ROLE_project_manager")) {
            return rdShifts();
        }
        return "error";
    }

    @PreAuthorize("hasAnyRole('ROLE_admin', 'ROLE_project_manager')")
    @DeleteMapping(value = "/shift")
    public String deleteShift(@RequestParam Long id, @RequestParam Long projectId) {
        Long defShiftId = mProjectShiftService.getShiftIdByProjectId(projectId);
        mShiftService.deleteShiftById(id);
        if (defShiftId != null && defShiftId.equals(id)) {
            Set<Shift> shifts = mShiftService.getShiftsByProjectId(projectId);
            if (shifts.size() > 0) {
                //Set this shift to default
                ProjectShift projectShift = new ProjectShift();
                projectShift.setProjectId(projectId);
                projectShift.setShiftId(shifts.iterator().next().getId());
                mProjectShiftService.saveProjectShift(projectShift);
            }
        }
        if (AuthenticationHelper.hasRole("ROLE_admin")) {
            return rdShiftsOfProject(projectId);
        } else if (AuthenticationHelper.hasRole("ROLE_project_manager")) {
            return rdShifts();
        }
        return "error";
    }

    @PreAuthorize("hasAnyRole('ROLE_admin', 'ROLE_project_manager')")
    @PutMapping(value = "default-shift")
    public @ResponseBody
    String changeDefShift(@RequestBody ProjectShift projectShift) {
        mProjectShiftService.upsertProjectShift(projectShift);
        return RESPONSE_SUCCESS;
    }

    @PreAuthorize("hasAnyRole('ROLE_admin', 'ROLE_project_manager')")
    @GetMapping(value = {"/equipment-log"}) // TM-301  hide page /equipment-list
    public ModelAndView equipmentList(ModelMap model, HttpServletRequest request) {
        ModelAndView modelAndView = new ModelAndView("equipment-list");

        String servletPath = request.getServletPath();

        boolean isEquipmentLog = servletPath.startsWith("/equipment-log");
        Long projectId = (Long) model.get("sessionProjectId");

        boolean isAdmin = false;
        if (AuthenticationHelper.hasRole("ROLE_admin")) {
            isAdmin = true;
        }
        List<Equipment> equipmentList;

        if (!isEquipmentLog) {
            equipmentList = equipmentService.getAllEquipmentByOwnershipType(EquipmentOwnershipType.COMPANY_OWNED);
        } else {
            equipmentList = new ArrayList<>(equipmentService.getRentalAndBeingUsedEquipments(projectId));
            equipmentList.sort(Comparator.comparing(Equipment::getEquipmentId));
        }
        List<EquipmentTag> equipmentTags = equipmentService.findEquipmentTags();

        Map<String, List<EquipmentTag>> idTagMap = equipmentTags.stream()
                .collect(Collectors.groupingBy(EquipmentTag::getEquipmentId, TreeMap::new,
                        Collectors.mapping(Function.identity(),
                                Collectors.collectingAndThen(Collectors.toList(), e -> e.stream()
                                        .sorted(Comparator.comparing(EquipmentTag::getTag))
                                        .collect(Collectors.toList())))));

        // Also display usage by other projects if both current project and other projects use the same equipment
        List<EquipmentUsage> equipmentUsages = equipmentService.getCurrentEquipmentUsages();
        Map<Long, List<EquipmentUsage>> equipmentUsagesMap = equipmentUsages.stream()
                .collect(Collectors.groupingBy(EquipmentUsage::getEquipmentId,
                        Collectors.mapping(Function.identity(),
                                Collectors.collectingAndThen(Collectors.toList(),
                                        e -> e.stream()
                                                .sorted(Comparator.comparing(EquipmentUsage::getStartDate))
                                                .collect(Collectors.toList())))));

        List<EquipmentDto> equipmentDtos = new ArrayList<>();
        for (Equipment equipment : equipmentList) {
            EquipmentDto equipmentDto = new EquipmentDto(equipment);
            if (equipmentUsagesMap.containsKey(equipment.getId())) {
                List<EquipmentUsage> usages = equipmentUsagesMap.get(equipment.getId());
                equipmentDto.setEquipmentUsages(usages);
                EquipmentUsage usage = usages.get(0);
                if (usage.getStartDate().isAfter(LocalDate.now())) {
                    long days = Period.between(LocalDate.now(), usage.getStartDate()).getDays();
                    equipmentDto.setAvailableDays(days);
                } else {
                    equipmentDto.setAvailable(false);
                    equipmentDto.setAvailableDays(0L);
                }
            }
            if (!isAdmin || equipmentDto.getEquipmentUsages().size() == 0) {
                EquipmentUsage usage = new EquipmentUsage();
                equipmentDto.getEquipmentUsages().add(usage);
            }

            equipmentDto.setTags(idTagMap.getOrDefault(equipment.getEquipmentId(), new ArrayList<>()).stream()
                    .map(EquipmentTag::getTag).collect(Collectors.toList()));
            equipmentDtos.add(equipmentDto);
        }

        Set<String> tags = equipmentTags.stream().map(EquipmentTag::getTag).collect(Collectors.toSet());
        modelAndView.addObject("tags", tags);
        modelAndView.addObject("equipmentDtos", equipmentDtos);
        modelAndView.addObject("isEquipmentLog", isEquipmentLog);
        return modelAndView;
    }

    @PreAuthorize("hasAnyRole('ROLE_admin', 'ROLE_project_manager')")
    @GetMapping(value = "/equipment")
    public ModelAndView equipment(@RequestParam(required = false) Long id,
                                  ModelMap model) {
        Equipment equipment;
        if (id == null) {
            equipment = new Equipment();
        } else {
            equipment = equipmentService.getEquipmentById(id);
        }
        ModelAndView modelAndView = new ModelAndView("equipment", "equipment", equipment);
        bindEquipmentAttributes(model);
        return modelAndView;
    }

    private void bindEquipmentAttributes(ModelMap model) {
        if (AuthenticationHelper.hasRole("ROLE_project_manager")) {
            Long projectId = (Long) model.get("sessionProjectId");
            EmployeeFilter filter = new EmployeeFilter(projectId, null, null, null, null, LocalDate.now(), null);
            model.put("employees", employeeService.getEmployees(filter));
            Set<String> purchaseOrders = purchaseOrderService.getDistinctLatestPurchaseOrderNumbers();
            model.put("purchaseOrders", purchaseOrders);
        }
    }

    @PreAuthorize("hasAnyRole('ROLE_admin', 'ROLE_project_manager')")
    @RequestMapping(value = "/equipment", method = {RequestMethod.POST, RequestMethod.PUT})
    public String addOrUpdateEquipment(@Validated @ModelAttribute("equipment") Equipment equipment,
                                       BindingResult bindingResult, ModelMap modelMap) {
        if (bindingResult.hasErrors()) {
            bindEquipmentAttributes(modelMap);
            return "equipment";
        }
        if (EquipmentHourlyType.HOURLY != equipment.getHourlyType()) {
            // Ignore employee if daily
            equipment.setEmpId(null);
        }
        equipmentService.saveEquipment(equipment);
        if (equipment.getOwnershipType() == EquipmentOwnershipType.RENTAL) {
            return "redirect:equipment-list?ownershipType=1";
        }
        return "redirect:equipment-list";
    }

    @PreAuthorize("hasAnyRole('ROLE_admin', 'ROLE_project_manager')")
    @PutMapping(value = "/equipment-validity")
    public String updateEquipmentValidity(@RequestParam(value = "id") Long id,
                                          @RequestParam(value = "active") boolean active) {
        equipmentService.changeEquipmentValidity(id, active);
        return "redirect:equipment-list";
    }

    @PreAuthorize("hasAnyRole('ROLE_admin', 'ROLE_project_manager', 'ROLE_timekeeper')")
    @GetMapping(value = "/cost-codes")
    public ModelAndView getCodeManagement(ModelMap model) {
        ModelAndView modelAndView = new ModelAndView("cost-codes");
        List<BillingCodeType> sortedTypes = billingCodeService.findBillingCodeTypesSortByLevel();
        Long projectId = (Long) model.get("sessionProjectId");
        Set<String> tags = costCodeService.getTagsByProjectId(projectId);
        modelAndView.addObject("tags", tags);
        modelAndView.addObject("sortedTypes", sortedTypes);
        return modelAndView;
    }

    @PreAuthorize("hasAnyRole('ROLE_admin', 'ROLE_project_manager')")
    @GetMapping(value = "/code-detail")
    public ModelAndView getCodeDetail(@RequestParam Long id) {
        ModelAndView modelAndView = new ModelAndView("code-detail");
        BillingCodeType billingCodeType = billingCodeService.findBillingCodeTypeById(id);
        modelAndView.addObject("billingCodeType", billingCodeType);
        return modelAndView;
    }

    @PreAuthorize("hasAnyRole('ROLE_admin', 'ROLE_project_manager')")
    @GetMapping(value = "/code-segments")
    public ModelAndView getCostCodeDetail() {
        ModelAndView modelAndView = new ModelAndView("code-segments");
        List<BillingCodeType> billingCodeTypes = billingCodeService.findAllBillingCodeTypes();
        modelAndView.addObject("billingCodeTypes", billingCodeTypes);
        return modelAndView;
    }

    @PreAuthorize("hasAnyRole('ROLE_admin', 'ROLE_project_manager')")
    @DeleteMapping(value = "/cost-code")
    public String deleteCostCode(@RequestParam Long id) {
        costCodeService.deleteById(id);
        return "redirect:cost-codes";
    }

    @PreAuthorize("hasAnyRole('ROLE_admin', 'ROLE_project_manager')")
    @GetMapping(value = "/cost-code-association")
    public ModelAndView getCostCodeAssociation(ModelMap model, @RequestParam(required = false) Long id) {
        ModelAndView modelAndView = new ModelAndView("cost-code-association");

        Long projectId = (Long) model.get("sessionProjectId");
        List<BillingCodeDto> billingCodes = billingCodeService.findBillingCodeByCostCodeIdAndProjectId(id, projectId);
        List<BillingCodeType> billingCodeTypes = billingCodeService.findAllBillingCodeTypes();
        CostCodeLog costCodeLog;
        if (null != id) {
            costCodeLog = costCodeService.getCostCodeById(id);
        } else {
            costCodeLog = new CostCodeLog();
        }

        modelAndView.addObject("costCodeLog", costCodeLog);
        modelAndView.addObject("billingCodes", billingCodes);
        modelAndView.addObject("billingCodeTypes", billingCodeTypes);
        return modelAndView;
    }

    @PreAuthorize("hasAnyRole('ROLE_admin', 'ROLE_project_manager')")
    @GetMapping(value = "/billing-code")
    public ModelAndView getBillingCode(ModelMap model,
                                       @RequestParam(value = "billingCodeId", required = false) Long billingCodeId,
                                       @RequestParam(value = "billingCodeTypeId", required = false) Long billingCodeTypeId) {
        ModelAndView modelAndView = new ModelAndView("billing-code");
        BillingCodeDto billingCodeDto = new BillingCodeDto();
        Long projectId = (Long) model.get("sessionProjectId");
        if (null == billingCodeId) {
            billingCodeDto.setTypeId(billingCodeTypeId);
        } else {
            BillingCode billingCode = billingCodeService.findBillingCodeById(billingCodeId);
            billingCodeDto.setId(billingCodeId);
            billingCodeDto.setCodeName(billingCode.getCodeName());
            billingCodeDto.setClientAlias(billingCode.getClientAlias());
            billingCodeDto.setDescription(billingCode.getDescription());
            billingCodeDto.setTypeId(billingCode.getTypeId());
            if (projectId != null) {
                BillingCodeOverride billingCodeOverride = billingCodeService.findBillingCodeOverride(billingCodeId, projectId);
                if (null != billingCodeOverride) {
                    billingCodeDto.setClientAlias(billingCodeOverride.getClientAlias());
                    billingCodeDto.setDescription(billingCodeOverride.getDescription());
                }
            }
        }
        modelAndView.addObject("billingCodeDto", billingCodeDto);
        return modelAndView;
    }

    @PreAuthorize("hasAnyRole('ROLE_admin', 'ROLE_project_manager')")
    @RequestMapping(value = "/billing-code", method = {RequestMethod.POST, RequestMethod.PUT})
    public String addOrUpdateBillingCode(ModelMap model,
                                         @ModelAttribute("billingCodeDto") BillingCodeDto billingCodeDto) {
        Long projectId = (Long) model.get("sessionProjectId");
        if (projectId != null) {
            // project manager cannot add billing code
            assert billingCodeDto.getId() != null;
            BillingCodeOverride billingCodeOverride = billingCodeService.findBillingCodeOverride(billingCodeDto.getId(),
                    projectId);
            if (null == billingCodeOverride) {
                billingCodeOverride = new BillingCodeOverride();
                billingCodeOverride.setBillingCodeId(billingCodeDto.getId());
                billingCodeOverride.setProjectId(projectId);
                billingCodeOverride.setCodeName(billingCodeDto.getCodeName());
            }
            billingCodeOverride.setDescription(billingCodeDto.getDescription());
            billingCodeOverride.setClientAlias(billingCodeDto.getClientAlias());
            billingCodeService.saveBillingCodeOverride(billingCodeOverride);
        } else {
            BillingCode billingCode = billingCodeService.findBillingCodeByTypeIdAndCodeName(billingCodeDto.getTypeId(),
                    billingCodeDto.getCodeName());
            if (billingCode == null) {
                billingCode = new BillingCode(billingCodeDto);
            } else {
                billingCode.setDescription(billingCodeDto.getDescription());
                billingCode.setClientAlias(billingCodeDto.getClientAlias());
            }
            billingCodeService.saveBillingCode(billingCode);
        }
        return String.format("redirect:code-detail?id=%d", billingCodeDto.getTypeId());
    }

    @PreAuthorize("hasAnyRole('ROLE_admin', 'ROLE_project_manager')")
    @DeleteMapping(value = "/billing-code")
    public String deleteBillingCode(ModelMap model, @RequestParam Long id, @RequestParam Long typeId) {
        Long projectId = (Long) model.get("sessionProjectId");
        if (projectId == null) {
            billingCodeService.deleteBillingCodeById(id);
        } else {
            billingCodeService.deleteBillingCodeOverrideByBillingCodeIdAndProjectId(id, projectId);
        }
        return String.format("redirect:code-detail?id=%d", typeId);
    }

    private String rdShifts() {
        return "redirect:shifts";
    }

    private String rdShiftsOfProject(Long projectId) {
        return "redirect:shifts?projectId=" + projectId;
    }
}

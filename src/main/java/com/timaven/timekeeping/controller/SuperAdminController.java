package com.timaven.timekeeping.controller;

import com.timaven.timekeeping.model.User;
import com.timaven.timekeeping.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.Set;

@Controller
@PreAuthorize("hasRole('ROLE_super_admin')")
public class SuperAdminController extends BaseController {

    private final AccountService mAccountService;

    @Autowired
    public SuperAdminController(AccountService accountService) {
        mAccountService = accountService;
    }

    /**
     * Show manager timekeeper's home page
     */
    @GetMapping(value = "/home-super-admin")
    public ModelAndView adminHome() {
        ModelAndView modelAndView = new ModelAndView("super-admin-home");
        Set<User> users = mAccountService.getAdminUsers();
        modelAndView.addObject("users", users);
        return modelAndView;
    }
}

package com.timaven.timekeeping.controller;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

@SessionAttributes({"sessionProjectId", "allowedDayOfWeek", "rule", "sessionTeamName", "sessionTimesheetsMode", "sessionTimesheetsDate", "idProjectMap"})
@RestController
@PreAuthorize("isAuthenticated()")
public class DocumentController {
    @GetMapping(value = "/project-template")
    public ModelAndView projectTemplate() {
        return new ModelAndView("project-template");
    }

    @PreAuthorize("hasRole('ROLE_admin')")
    @GetMapping(value = "/company-documents")
    public ModelAndView companyDocuments() {
        return new ModelAndView("company-documents");
    }
}

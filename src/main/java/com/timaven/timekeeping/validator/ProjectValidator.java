package com.timaven.timekeeping.validator;

import com.timaven.timekeeping.model.Project;
import com.timaven.timekeeping.service.ProjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Component
public class ProjectValidator implements Validator {
    private final ProjectService projectService;

    @Autowired
    public ProjectValidator(ProjectService projectService) {
        this.projectService = projectService;
    }

    @Override
    public boolean supports(Class<?> aClass) {
        return Project.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        Project project = (Project) o;

        Project existingProject = projectService.getProjectByJobNumberAndSubJob(project.getJobNumber(), project.getSubJob());
        if (existingProject != null && (project.getId() == null ||  !project.getId().equals(existingProject.getId()))) {
            errors.rejectValue("subJob", "Duplicate.projectForm.subJob", "Combination of job number and Sub job supposes to be unique");
        }
    }
}

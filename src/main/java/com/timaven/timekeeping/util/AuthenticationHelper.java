package com.timaven.timekeeping.util;

import lombok.Getter;
import lombok.Setter;
import org.springframework.security.access.hierarchicalroles.RoleHierarchyImpl;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.util.CollectionUtils;

import java.util.Collection;

@Getter
@Setter
public class AuthenticationHelper {
    private static RoleHierarchyImpl roleHierarchy;

    public static void setRoleHierarchy(RoleHierarchyImpl roleHierarchy) {
        AuthenticationHelper.roleHierarchy = roleHierarchy;
    }

    public static boolean hasRole(String role) {

        Collection<? extends GrantedAuthority> authorities =
                SecurityContextHolder.getContext().getAuthentication().getAuthorities();

        Collection<? extends GrantedAuthority> hierarchyAuthorities =
                roleHierarchy.getReachableGrantedAuthorities(authorities);

        for (GrantedAuthority authority : authorities) {
            if (authority.getAuthority().equals(role)) {
                return true;
            }
        }

        for (GrantedAuthority authority : hierarchyAuthorities) {
            if (authority.getAuthority().equals(role)) {
                return true;
            }
        }

        return false;
    }

    public static boolean hasAnyRole(Collection<String> roles) {
        if (CollectionUtils.isEmpty(roles)) return false;
        for (String role : roles) {
            if (hasRole(role)) return true;
        }
        return false;
    }
}

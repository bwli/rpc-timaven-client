package com.timaven.timekeeping.util;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data //  getter/setter/equals/canEqual/hashCode/toString
@NoArgsConstructor  //
@AllArgsConstructor //
@Builder
public class ResponseBean {
    @JsonProperty("code")
    int code;
    @JsonProperty("msg")
    String msg;
    @JsonProperty("data")
    String data;
}

package com.timaven.timekeeping.util;


public enum ErrorCode {

    SYSTEM_ERROR(1,"SYSTEM EXCEPTION"),
    BAD_REQUEST(2,"BAD_REQUEST"),
    NOT_FOUND(3,"REQUEST PATH NOT FOUND"),
    CONNECTION_ERROR(4,"CONNECTION_ERROR"),
    METHOD_NOT_ALLOWED(5,"METHOD_NOT_ALLOWED"),
    BOUND_STATEMENT_NOT_FOUNT(6,"BOUND_STATEMENT_NOT_FOUNT"),
    SUCCESS_OPTION(7,"SUCCESS_OPTION"),
    DATABASE_ERROR(8,"DATABASE_ERROR"),
    NO_PERMISSION(9,"TOKEN NO_PERMISSION"),
    INVALID_MOBILE(10,"INVALID_MOBILE"),
    ILLEGAL_ARGUMENT(11,"ILLEGAL_ARGUMENT");


    private int code;
    private String msg;

    private ErrorCode(int code, String msg) {
      this.code = code;
      this.msg = msg;
    }

    public int getCode() {
      return code;
    }

    public void setCode(int code) {
      this.code = code;
    }

    public String getMsg() {
      return msg;
    }

    public void setMsg(String msg) {
      this.msg = msg;
    }

}

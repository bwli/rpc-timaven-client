package com.timaven.timekeeping.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.time.LocalDate;

@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode(of = {"projectId", "weekEndDate", "type"})
public class ProjectPayrollPK implements Serializable {
    private Long projectId;

    private LocalDate weekEndDate;

    private int type;
}

package com.timaven.timekeeping.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = BillingCodeOverride.class)
@Getter
@Setter
@NoArgsConstructor
public class BillingCodeOverride {

    private Long id;

    private BillingCode billingCode;

    private Long billingCodeId;

    private String codeName;

    private String clientAlias;

    private String description;

    private Long projectId;

    @Override
    public int hashCode() {
        HashCodeBuilder hcb = new HashCodeBuilder();
        hcb.append(id);
        return hcb.toHashCode();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof BillingCodeOverride)) return false;
        BillingCodeOverride that = (BillingCodeOverride) o;
        EqualsBuilder eb = new EqualsBuilder();
        eb.append(id, that.id);
        return eb.isEquals();
    }
}

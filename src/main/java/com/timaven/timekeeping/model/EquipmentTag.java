package com.timaven.timekeeping.model;

import lombok.Data;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

@Data
public class EquipmentTag {

    private Long id;

    private String equipmentId;

    private String tag;

    @Override
    public int hashCode() {
        HashCodeBuilder hcb = new HashCodeBuilder();
        hcb.append(equipmentId);
        return hcb.toHashCode();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof EquipmentTag)) return false;
        EquipmentTag that = (EquipmentTag) o;
        EqualsBuilder eb = new EqualsBuilder();
        eb.append(equipmentId, that.equipmentId);
        return eb.isEquals();
    }
}

package com.timaven.timekeeping.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Objects;


@Getter
@Setter
@NoArgsConstructor
public class UserProject implements Comparable<UserProject> {

    private Long id;

    private Long userId;

    private User user;

    private Long projectId;

    private Project project;

    public UserProject(Long userId, Long projectId) {
        this.userId = userId;
        this.projectId = projectId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserProject that = (UserProject) o;
        if (id != null && that.id != null) {
            return Objects.equals(id, that.id);
        } else if (userId != null && that.userId != null
                && projectId != null && that.projectId != null) {
            return Objects.equals(userId, that.userId) &&
                    Objects.equals(projectId, that.projectId);
        }
        return super.equals(o);
    }

    @Override
    public int hashCode() {
        if (id != null) return Objects.hash(id);
        if (userId != null && projectId != null) return Objects.hash(id, userId, projectId);
        return super.hashCode();
    }

    @Override
    public int compareTo(UserProject userProject) {
        int result = projectId.compareTo(userProject.projectId);
        if (result == 0) {
            result = userId.compareTo(userProject.userId);
        }
        return result;
    }
}

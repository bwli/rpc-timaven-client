package com.timaven.timekeeping.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
public class StageRecord {
    private Long id;

    @JsonBackReference
    private StageTransaction stageTransaction;

    private String doorName;

    private Boolean side;

    private boolean accessGranted = false;

    private boolean valid = false;

    private LocalDateTime adjustedTime;

    private LocalDateTime netEventTime;

    @Override
    public int hashCode() {
        if (null != id) {
            HashCodeBuilder hcb = new HashCodeBuilder();
            hcb.append(id);
            return hcb.toHashCode();
        }
        return super.hashCode();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof StageRecord)) return false;
        StageRecord that = (StageRecord) o;
        if (null != id) {
            EqualsBuilder eb = new EqualsBuilder();
            eb.append(id, that.id);
            return eb.isEquals();
        }
        return super.equals(o);
    }
}

package com.timaven.timekeeping.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalTime;
import java.util.Comparator;

@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode(of = {"name", "projectId"})
public class Shift implements Comparable<Shift> {

    private Long id;

    private String name;

    private LocalTime scheduleStart;

    private LocalTime scheduleEnd;

    private LocalTime breakStart;

    private LocalTime breakEnd;

    private int paidBreakDuration;

    private Long projectId;

    @Override
    public int compareTo(Shift o) {
        return Comparator.comparing(Shift::getProjectId, Comparator.nullsLast(Comparator.naturalOrder()))
                .thenComparing(Shift::getName, Comparator.nullsLast(Comparator.naturalOrder()))
                .compare(this, o);
    }
}

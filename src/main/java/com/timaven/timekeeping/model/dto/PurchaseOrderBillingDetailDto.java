package com.timaven.timekeeping.model.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@NoArgsConstructor
public class PurchaseOrderBillingDetailDto {
    private String purchaseOrderNumber;
    private Integer lineNumber;
    private BigDecimal unitCost;
    private String part;
    private BigDecimal billingQuantity;
    private BigDecimal markupQuantity;
    private BigDecimal markupAmount;
    private Long billingId;
    private BigDecimal tax;
}

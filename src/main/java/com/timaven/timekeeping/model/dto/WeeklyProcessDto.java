package com.timaven.timekeeping.model.dto;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.util.CollectionUtils;

import java.time.LocalDate;
import java.util.Comparator;
import java.util.List;

@Data
@NoArgsConstructor
public class WeeklyProcessDto {
    private List<EmpCostCodeDateDto> empCostCodeDateDtos;
    private List<LocalDate> days;
    private boolean finalized;
    private LocalDate weekEndDate;
    private int type;

    public WeeklyProcessDto(List<EmpCostCodeDateDto> empCostCodeDateDtos, List<LocalDate> days, LocalDate weekEndDate,
                            int type) {
        this.empCostCodeDateDtos = empCostCodeDateDtos;
        this.days = days;
        this.weekEndDate = weekEndDate;
        this.type = type;
    }
}

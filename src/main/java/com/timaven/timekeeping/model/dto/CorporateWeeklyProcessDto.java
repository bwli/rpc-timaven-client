package com.timaven.timekeeping.model.dto;

import com.timaven.timekeeping.model.WeeklyProcess;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.stream.Collectors;

@Data
@NoArgsConstructor
public class CorporateWeeklyProcessDto {
    private Long id;
    private LocalDate weekEndDate;
    private String teamName;
    private List<EmpDateDto> empDateDtos = new ArrayList<>();
    private BigDecimal totalStraightTime;
    private BigDecimal totalOvertime;
    private BigDecimal totalNonOvertime;
    private BigDecimal totalTime;
    private String reviewedBy;
    private String approvedBy;
    private Map<Long, String> activityCodeMap;

    public CorporateWeeklyProcessDto(WeeklyProcess weeklyProcess, List<EmpDateDto> empDateDtos) {
        this.id = weeklyProcess.getId();
        this.weekEndDate = weeklyProcess.getWeekEndDate();
        this.teamName = weeklyProcess.getTeamName();
        if (weeklyProcess.getReviewUser() != null) {
            this.reviewedBy = weeklyProcess.getReviewUser().getDisplayName();
        }
        if (weeklyProcess.getApproveUser() != null) {
            this.approvedBy = weeklyProcess.getApproveUser().getDisplayName();
        }
        this.empDateDtos = empDateDtos.stream().sorted(Comparator.comparing(EmpDateDto::getLastName)
                .thenComparing(EmpDateDto::getFirstName)).collect(Collectors.toList());
        this.totalStraightTime = empDateDtos.stream()
                .map(EmpDateDto::getStTotal)
                .reduce(BigDecimal.ZERO, BigDecimal::add);
        this.totalOvertime = empDateDtos.stream()
                .map(EmpDateDto::getOtTotal)
                .reduce(BigDecimal.ZERO, BigDecimal::add);
        this.totalNonOvertime = empDateDtos.stream()
                .map(EmpDateDto::getNonOt)
                .reduce(BigDecimal.ZERO, BigDecimal::add);
        this.totalTime = empDateDtos.stream()
                .map(EmpDateDto::getTotalTime)
                .reduce(BigDecimal.ZERO, BigDecimal::add);
    }
}

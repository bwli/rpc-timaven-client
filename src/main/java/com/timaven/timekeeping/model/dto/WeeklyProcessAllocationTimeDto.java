package com.timaven.timekeeping.model.dto;

import com.timaven.timekeeping.model.AllocationTime;
import com.timaven.timekeeping.model.PerDiem;
import com.timaven.timekeeping.model.enums.CostCodeType;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.*;

@Data
@NoArgsConstructor
public class WeeklyProcessAllocationTimeDto {
    private Long id;
    private LocalDate dateOfService;
    // Used to decide whether the employee has per diem on that date
    private boolean hasPerDiem;
//    private String perDiemCostCode;
    private Set<String> perDiemCostCodes;
    private Set<String> mobCostCodes;
    private BigDecimal perDiem;
    private BigDecimal mobMileage = BigDecimal.ZERO;
    private BigDecimal mobAmount = BigDecimal.ZERO;
    private BigDecimal mobMileageRate = BigDecimal.ZERO;
    private boolean absent;
    private PerDiem perDiemEntity;

    private Map<String, WeeklyProcessCostCodePercDto> empCostCodePercMap;
    private List<CostCodeTagAssociationDto> perDiemCostCodeTagAssociations = new ArrayList<>();
    private List<CostCodeTagAssociationDto> mobCostCodeTagAssociations = new ArrayList<>();

    public WeeklyProcessAllocationTimeDto(AllocationTime allocationTime, LocalDate dateOfService) {
        this.id = allocationTime.getId();
        this.dateOfService = dateOfService;
        this.hasPerDiem = allocationTime.isHasPerDiem();
        this.perDiem = allocationTime.getPerDiemAmount();
        this.mobMileage = allocationTime.getMobMileage();
        this.mobAmount = allocationTime.getMobAmount();
        this.mobMileageRate = allocationTime.getMobMileageRate();
    }

    public Map<String, WeeklyProcessCostCodePercDto> getEmpCostCodePercMap() {
        if (null == empCostCodePercMap) {
            empCostCodePercMap = new HashMap<>();
        }
        return empCostCodePercMap;
    }

    public Set<String> getPerDiemCostCodes() {
        if (null == perDiemCostCodes) {
            perDiemCostCodes = new TreeSet<>();
        }
        return perDiemCostCodes;
    }

    public Set<String> getMobCostCodes() {
        if (null == mobCostCodes) {
            mobCostCodes = new TreeSet<>();
        }
        return mobCostCodes;
    }

    public WeeklyProcessCostCodePercDto getCostCodePerc(String costCode) {
        return getEmpCostCodePercMap().get(costCode);
    }

    public BigDecimal getPerDiem() {
        return perDiem == null ? BigDecimal.ZERO : new BigDecimal(perDiem.stripTrailingZeros().toPlainString());
    }

    public List<CostCodeTagAssociationDto> getPerDiemCostCodeTagAssociations() {
        if (null == perDiemCostCodeTagAssociations) perDiemCostCodeTagAssociations = new ArrayList<>();
        return perDiemCostCodeTagAssociations;
    }

    public List<CostCodeTagAssociationDto> getMobCostCodeTagAssociations() {
        if (null == mobCostCodeTagAssociations) mobCostCodeTagAssociations = new ArrayList<>();
        return mobCostCodeTagAssociations;
    }

    public void addCostCodeTagAssociationDto(String tagType, String typeDescription, String codeName, String codeDescription, CostCodeType costCodeType) {
        CostCodeTagAssociationDto costCodeTagAssociationDto = new CostCodeTagAssociationDto(null, tagType, typeDescription, codeName, codeDescription);
        if (costCodeType == CostCodeType.PERDIEM) {
            getPerDiemCostCodeTagAssociations().add(costCodeTagAssociationDto);
        } else if (costCodeType == CostCodeType.MOB) {
            getMobCostCodeTagAssociations().add(costCodeTagAssociationDto);
        }
    }

    public void removeCostCodeTagAssociationDto(String tagType, String codeName, CostCodeType costCodeType) {
        if (costCodeType == CostCodeType.PERDIEM) {
            getPerDiemCostCodeTagAssociations().removeIf(a -> a.getTagType().equals(tagType) && a.getCodeName().equals(codeName));
        } else if (costCodeType == CostCodeType.MOB) {
            getMobCostCodeTagAssociations().removeIf(a -> a.getTagType().equals(tagType) && a.getCodeName().equals(codeName));
        }
    }

    public BigDecimal getTotalHour() {
        return this.getEmpCostCodePercMap().values().stream()
                .map(p -> p.getStHour().add(p.getOtHour()).add(p.getDtHour()))
                .reduce(BigDecimal.ZERO, BigDecimal::add);
    }
}

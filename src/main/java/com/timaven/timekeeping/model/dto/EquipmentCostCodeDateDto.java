package com.timaven.timekeeping.model.dto;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.*;

@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode(of = "equipmentId")
public class EquipmentCostCodeDateDto {
    private String equipmentId;
    private String serialNumber;
    private String description;
    private String alias;
    private String department;
    private String equipmentClass;
    private String empId;
    private String hourlyType;
    private String ownershipType;
    private Set<String> costCodes;

    public EquipmentCostCodeDateDto(String equipmentId, String serialNumber, String description, String alias,
                                    String department, String equipmentClass, String empId, String hourlyType,
                                    String ownershipType) {
        this.equipmentId = equipmentId;
        this.serialNumber = serialNumber;
        this.description = description;
        this.alias = alias;
        this.department = department;
        this.equipmentClass = equipmentClass;
        this.empId = empId;
        this.hourlyType = hourlyType;
        this.ownershipType = ownershipType;
    }

    private Map<String, EquipmentProcessAllocationTimeDto> equipmentAllocationTimeMap;

    public Map<String, EquipmentProcessAllocationTimeDto> getEquipmentAllocationTimeMap() {
        if (equipmentAllocationTimeMap == null) {
            equipmentAllocationTimeMap = new HashMap<>();
        }
        return equipmentAllocationTimeMap;
    }

    public EquipmentProcessAllocationTimeDto getAllocationTime(LocalDate dateOfService) {
        return getEquipmentAllocationTimeMap().get(dateOfService.toString());
    }

    public Set<String> getCostCodes() {
        if (null == costCodes) {
            costCodes = new TreeSet<>();
        }
        return costCodes;
    }

    public BigDecimal getTotalHour() {
        return getEquipmentAllocationTimeMap().values().stream()
                .map(EquipmentProcessAllocationTimeDto::getEquipmentCostCodePercMap)
                .map(Map::values)
                .flatMap(Collection::stream)
                .map(EquipmentProcessCostCodePercDto::getTotalHour)
                .filter(Objects::nonNull).reduce(BigDecimal.ZERO, BigDecimal::add);
    }

    public BigDecimal getTotalCharge() {
        return new BigDecimal(getEquipmentAllocationTimeMap().values().stream()
                .map(EquipmentProcessAllocationTimeDto::getEquipmentCostCodePercMap)
                .map(Map::values)
                .flatMap(Collection::stream)
                .map(EquipmentProcessCostCodePercDto::getTotalCharge)
                .filter(Objects::nonNull).reduce(BigDecimal.ZERO, BigDecimal::add).stripTrailingZeros().toPlainString());
    }
}

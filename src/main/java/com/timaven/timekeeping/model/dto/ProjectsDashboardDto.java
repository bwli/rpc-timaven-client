package com.timaven.timekeeping.model.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class ProjectsDashboardDto {
    private Long projectId;
    private String jobSubJob;
    private String description;
    private Integer empCount = 0;
    private String hours;
    private String clientCost;
    private String committedCost;
    private String payrollProcessed;

    // TM-192 Start
    private String clientName;
    private String city;
    private String state;
    // TM-192 End

    private Integer rosterCount = 0; // TM-412
}

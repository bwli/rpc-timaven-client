package com.timaven.timekeeping.model.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.Map;

@Data
@NoArgsConstructor
public class EquipmentPriceDto {
    private Map<String, Object> equipmentClass;
    private String description;
    private String hourlyRate;
    private String dailyRate;
    private String weeklyRate;
    private String monthlyRate;
    private Long id;
}

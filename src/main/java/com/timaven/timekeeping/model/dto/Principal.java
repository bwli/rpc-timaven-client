package com.timaven.timekeeping.model.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.time.LocalDateTime;

@Data
@NoArgsConstructor
public class Principal implements Serializable {
    private Long id;
    private String username;
    private String displayName;
    private String ipAddress;
    private String tenantId;
    private LocalDateTime loggedInAt;
}

package com.timaven.timekeeping.model.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.timaven.timekeeping.model.Requisition;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.util.CollectionUtils;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Data
@NoArgsConstructor
public class RequisitionDto {
    private String purchaseOrderNumber;

    @JsonFormat(pattern = "MM/dd/yyyy")
    private LocalDate requisitionDate;
    private String vendorName;
    private String requestor;
    private String requisitionNumber;

    private boolean newItem = true;

    private Integer itemCount;

    List<RequisitionLineDto> requisitionLineDtos = new ArrayList<>();
}

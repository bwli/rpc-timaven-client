package com.timaven.timekeeping.model.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@EqualsAndHashCode(of = {"projectId", "code", "startDate", "endDate"})
@NoArgsConstructor
public class CraftDto {
    private Long id;
    private String code;
    private Long projectId;
    private String billableST;
    private String billableOT;
    private String billableDT;
    private String description;
    private String perDiemAmt;
    private String perDiemName;
    private Long perDiemId;
    private String rigPay;
    private LocalDate startDate;
    private LocalDate endDate;
    private String company;
}

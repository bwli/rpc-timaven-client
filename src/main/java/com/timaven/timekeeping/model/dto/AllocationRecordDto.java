package com.timaven.timekeeping.model.dto;

import com.timaven.timekeeping.model.AllocationRecord;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.Comparator;

@Data
@NoArgsConstructor
public class AllocationRecordDto implements Comparable<AllocationRecordDto> {
    private Long id;
    private String doorName;
    private Boolean side;
    private boolean accessGranted = false;
    private boolean valid = false;
    private LocalDateTime adjustedTime;
    private LocalDateTime netEventTime;

    @Override
    public int compareTo(AllocationRecordDto allocationRecord) {
        return Comparator.comparing(AllocationRecordDto::getAdjustedTime, Comparator.nullsLast(Comparator.naturalOrder()))
                .compare(this, allocationRecord);
    }
}

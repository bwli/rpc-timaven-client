package com.timaven.timekeeping.model.dto;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.util.CollectionUtils;

import java.time.LocalDate;
import java.util.Comparator;
import java.util.List;

@Data
@NoArgsConstructor
public class EquipmentProcessDto {
    private List<EquipmentCostCodeDateDto> equipmentCostCodeDateDtos;
    private List<LocalDate> days;
    private int type;
    private LocalDate weekEndDate;

    public EquipmentProcessDto(List<EquipmentCostCodeDateDto> equipmentCostCodeDateDtos, List<LocalDate> days,
                               int type, LocalDate weekEndDate) {
        if (!CollectionUtils.isEmpty(equipmentCostCodeDateDtos)) {
            equipmentCostCodeDateDtos.sort(Comparator.comparing(EquipmentCostCodeDateDto::getEquipmentId));
        }
        this.equipmentCostCodeDateDtos = equipmentCostCodeDateDtos;
        this.days = days;
        this.type = type;
        this.weekEndDate = weekEndDate;
    }
}

package com.timaven.timekeeping.model.dto;

import java.util.HashMap;
import java.util.Map;

public class EmpExtraTimeMap extends HashMap<Long, Map<String, EmpDateHourDto>> {

    /**
     * TM-373
     * Automatic assembly of spring. Do not support Map<String , Map<String , Values>>
     * In order to solve this problem. Override HashMap get Function
     * but Before use need containsKey
     */
    @Override
    public Map<String, EmpDateHourDto> get(Object key) {
        Map<String, EmpDateHourDto> dateMap = super.get(key);
        if(dateMap == null){
            dateMap = new HashMap<>();
            put((Long) key,dateMap);
        }
        return dateMap;
    }
}

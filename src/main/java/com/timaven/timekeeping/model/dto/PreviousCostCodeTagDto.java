package com.timaven.timekeeping.model.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Map;
import java.util.Set;

@Data
@NoArgsConstructor

public class PreviousCostCodeTagDto {
    Set<CostCodeTagDto> costCodeTagDtos;
    Map<String, List<CostCodeTagAssociationDto>> empIdPerDiemCostCodeTagAssociationsMap;
    Map<String, List<CostCodeTagAssociationDto>> empIdMobCostCodeTagAssociationsMap;
    Map<String, String> empIdPerDiemCostCodeMap;
    Map<String, String> empIdMobCostCodeMap;
}

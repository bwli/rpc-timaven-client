package com.timaven.timekeeping.model.dto;

import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDate;

@Data
public class PurchaseOrderDetailDto {
    private String purchaseOrderNumber;
    private LocalDate purchaseOrderDate;
    private String vendorName;
    private String requestor;
    private String requisitionNumber;
    private Integer lineNumber;
    private String partDescription;
    private BigDecimal quantity;
    private BigDecimal quantityReceived;
    private String unitCost;
    private String extendedCost;
    private String formattedCostDistribution;
    private String costType;
    private BigDecimal billedQuantity;
}

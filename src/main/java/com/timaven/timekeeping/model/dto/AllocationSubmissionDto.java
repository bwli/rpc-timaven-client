package com.timaven.timekeeping.model.dto;

import com.timaven.timekeeping.model.User;
import com.timaven.timekeeping.model.enums.AllocationSubmissionStatus;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@Data
@NoArgsConstructor
public class AllocationSubmissionDto {
    private Long id;
    private Long submitUserId;
    private Long approveUserId;
    private Long finalizeUserId;
    private LocalDateTime approvedAt;
    private LocalDateTime finalizedAt;
    private Long reportSubmissionId;
    private String teamName;
    private AllocationSubmissionStatus status;
    private AllocationSubmissionStatus previousStatus;
    private LocalDate dateOfService;
    private LocalDate payrollDate;
    private Long projectId;
    private List<AllocationTimeDto> allocationTimes;
    private Boolean billingPurposeOnly = false;
    private Boolean isManual = false;

    private User submitUser;
    private User approveUser;
    private Boolean exported = false;
}

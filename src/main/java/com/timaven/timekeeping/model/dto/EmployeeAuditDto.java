package com.timaven.timekeeping.model.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.time.ZonedDateTime;

@Data
@NoArgsConstructor
public class EmployeeAuditDto {
    private String empId;
    private String craft;
    private String team;
    private String crew;
    private String name;
    private String company;
    private String jobNumber;
    private String activityCode;
    private Boolean isOffsite;
    private String badge;
    private String signInSheet;
    private LocalDate effectedOn;
    private LocalDate hiredAt;
    private LocalDate terminatedAt;
    private Boolean hasPerDiem = false;
    private Boolean hasRigPay = false;
    private int rev;
    private String revType;
    private String by;
    private ZonedDateTime revisionTime;
}

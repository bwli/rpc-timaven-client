package com.timaven.timekeeping.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CostCodeBillingCodeFilter {
    Set<Long> costCodeIds;
    Set<String> includes;
}

package com.timaven.timekeeping.model.dto;

import com.timaven.timekeeping.model.Project;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.util.CollectionUtils;

import java.util.*;
import java.util.stream.Collectors;

@Data
@NoArgsConstructor
public class ReconcileDto {
    private List<EmpCostCodeProjectDto> empCostCodeProjectDtos;
    private List<Project> projects;
    private Set<Long> projectIds;
    private Map<Long, Boolean> hideDtMap = new HashMap<>();
    private Map<Long, String> projectUserNames;
    private String allocationSubmissionIds;
}

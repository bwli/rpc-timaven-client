package com.timaven.timekeeping.model.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Comparator;

@Data
@NoArgsConstructor
public class PunchExceptionDto implements Comparable<PunchExceptionDto> {
    private Long id;
    private String exception;
    private int order;
    private int exceptionType;

    @Override
    public int compareTo(PunchExceptionDto e) {
        return Comparator.comparing(PunchExceptionDto::getOrder).compare(this, e);
    }
}

package com.timaven.timekeeping.model.dto;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
public class CostCodePercDto {
    private Long id;
    private String costCodeFull;
    private String purchaseOrder;
    private BigDecimal stHour;
    private BigDecimal otHour;
    private BigDecimal dtHour;
    private BigDecimal totalHour;
    private Integer percentage;
    private Integer displayOrder;
    private List<CostCodeTagAssociationDto> costCodeTagAssociationDtos = new ArrayList<>();

    @Override
    public int hashCode() {
        HashCodeBuilder hcb = new HashCodeBuilder();
        hcb.append(costCodeFull);
        hcb.append(costCodeTagAssociationDtos);
        return hcb.toHashCode();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CostCodePercDto)) return false;
        CostCodePercDto that = (CostCodePercDto) o;
        EqualsBuilder eb = new EqualsBuilder();
        eb.append(costCodeFull, that.costCodeFull);
        eb.append(costCodeTagAssociationDtos, that.costCodeTagAssociationDtos);
        return eb.isEquals();
    }
}

package com.timaven.timekeeping.model.dto;

import com.timaven.timekeeping.model.Equipment;
import com.timaven.timekeeping.model.EquipmentUsage;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
public class EquipmentDto {
    private Equipment equipment;
    private boolean available = true;
    private Long availableDays;
    private List<EquipmentUsage> equipmentUsages = new ArrayList<>();
    private List<String> tags = new ArrayList<>();

    public EquipmentDto(Equipment equipment) {
        this.equipment = equipment;
    }
}

package com.timaven.timekeeping.model.dto;

import com.timaven.timekeeping.model.User;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

@Data
@NoArgsConstructor
public class WeeklyPayrollDto {
    private Long id;
    private String project;
    private String teamName;
    private int employeeCount;
    private BigDecimal totalHours;
    private LocalDateTime approvedAt;
    private LocalDateTime generatedAt;
    private boolean taxablePerDiem;
    private boolean isTimeLeftOff = false;
    private List<String> projectManagers;
    private List<String> projectAccountants;
    private String comment;
    private LocalDateTime rejectedAt;
    private LocalDateTime finalizedAt;
    private User approveUser;
    private User finalizeUser;
    private User rejectUser;
}

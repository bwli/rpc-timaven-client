package com.timaven.timekeeping.model.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class BillingCodeDto {
    private Long id;
    private String codeName;
    private String codeType;
    private String clientAlias;
    private String description;
    private Long typeId;
}

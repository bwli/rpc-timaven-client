package com.timaven.timekeeping.model.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class ProjectsDashboardProfitDto {
    private Long projectId;
    private String jobSubJob;
    private String description;
    private int empCount;
    private String committedValue;
    private String committedCost;
}

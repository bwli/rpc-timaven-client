package com.timaven.timekeeping.model.dto;

import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDate;

@Data
public class PurchaseOrderSummaryDto {
    private String purchaseOrderNumber;
    private LocalDate purchaseOrderDate;
    private String vendorName;
    private String requestor;
    private String extendedCost;
    private BigDecimal quantity;
    private BigDecimal billedQuantity;
    private String billedAmount;
    private LocalDate billedOn;
    private String comment;
    private boolean flag;
}

package com.timaven.timekeeping.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Comparator;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class EmployeeIdNameDto implements Comparable<EmployeeIdNameDto> {
    private Long id;
    private String fullName;
    private String craft;
    private String crew;

    @Override
    public int compareTo(EmployeeIdNameDto o) {
        return Comparator.comparing(EmployeeIdNameDto::getFullName, Comparator.nullsLast(Comparator.naturalOrder()))
                .compare(this, o);
    }
}

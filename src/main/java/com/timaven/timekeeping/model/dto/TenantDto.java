package com.timaven.timekeeping.model.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class TenantDto {
    private String name;
    private String host;
}

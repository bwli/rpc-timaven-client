package com.timaven.timekeeping.model.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@NoArgsConstructor
public class PurchaseOrderBillingDetailViewDto {
    private Long id;
    private Long billingId;
    private String billingNumber;
    private String invoiceNumber;
    private String part;
    private BigDecimal billingQuantity;
    private String amount;
    private String tax;
}

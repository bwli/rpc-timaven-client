package com.timaven.timekeeping.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.time.LocalDate;

@Getter
@Setter
@NoArgsConstructor
public class CostCodeAssociation {

    private Long id;

    LocalDate startDate;

    LocalDate endDate;

    private String costCode;

    private String jobNumber;

    private String description;

    private String subJob;

    private Long projectId;

    private String level1;

    private String level2;

    private String level3;

    private String level4;

    private String level5;

    private String level6;

    private String level7;

    private String level8;

    private String level9;

    private String level10;

    private String level11;

    private String level12;

    private String level13;

    private String level14;

    private String level15;

    private String level16;

    private String level17;

    private String level18;

    private String level19;

    private String level20;

    @Override
    public int hashCode() {
        HashCodeBuilder hcb = new HashCodeBuilder();
        hcb.append(id);
        return hcb.toHashCode();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CostCodeAssociation)) return false;
        CostCodeAssociation that = (CostCodeAssociation) o;
        EqualsBuilder eb = new EqualsBuilder();
        eb.append(id, that.id);
        return eb.isEquals();
    }
}

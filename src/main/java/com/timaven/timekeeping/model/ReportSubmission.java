package com.timaven.timekeeping.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.Set;

@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = ReportSubmission.class)
@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode(of = {"id", "reportedAt"})
public class ReportSubmission {
    private Long id;
    private Long userId;
    private String teamName;
    private LocalDateTime reportedAt;
    private Set<ReportPerc> reportPercs;
    @NotNull
    private Long projectId;
}

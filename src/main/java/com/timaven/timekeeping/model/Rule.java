package com.timaven.timekeeping.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.DayOfWeek;
import java.time.LocalDate;


@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode(of = "id")
public class Rule implements Serializable {

    private Long id;

    @NotNull
    private int roundingMinutes;

    @NotNull
    private String roundingMode = "ROUNDING_TO_NEAREST_NTH_MINUTE";

    @NotNull
    private int graceIn;

    @NotNull
    private int graceOut;

    private String description;

    private Long projectId;

    private Long perDiemId;

    private int rigPayThreshold;

    private DayOfWeek weekEndDay = DayOfWeek.SUNDAY;

    private BigDecimal equipmentWorkdayHour = BigDecimal.valueOf(8);

    private boolean equipmentExcludeWeekend = true;

    private String equipmentCostCode;

    private String perDiemCostCode;

    private String stCostCode;

    private String otCostCode;

    private String dtCostCode;

    private String sickCostCode;

    private String holidayCostCode;

    private String vacationCostCode;

    private String otherCostCode;

    private int dayOffThreshold = 13;

    private BigDecimal mobMileageRate = BigDecimal.ZERO;

//    private boolean perDiemByLaborCostCode = false;

    @NotNull
    private boolean taxablePerDiem = false;

    @NotNull
    private boolean hideDt;

    public LocalDate getStartOfWeek(LocalDate dateOfService) {
        LocalDate startOfWeek = dateOfService.with(weekEndDay.plus(1));
        if (startOfWeek.isAfter(dateOfService)) startOfWeek = startOfWeek.minusWeeks(1);
        return startOfWeek;
    }

    public LocalDate getEndOfWeek(LocalDate dateOfService) {
        LocalDate endOfWeek = dateOfService.with(weekEndDay);
        if (endOfWeek.isBefore(dateOfService)) endOfWeek = endOfWeek.plusWeeks(1);
        return endOfWeek;
    }

    public LocalDate getPreviousEndOfWeek(LocalDate dateOfService) {
        LocalDate endOfWeek = dateOfService.with(weekEndDay);
        if (endOfWeek.isAfter(dateOfService)) endOfWeek = endOfWeek.minusWeeks(1);
        return endOfWeek;
    }
}

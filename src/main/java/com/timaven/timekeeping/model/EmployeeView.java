package com.timaven.timekeeping.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode(of = "id")
public class EmployeeView {

    private Long id;

    private String empId;

    private String clientEmpId;

    private String craft;

    private String clientCraft;

    private String teamName;

    private String crew;

    private Long projectId;

    private String firstName;

    private String lastName;

    private String middleName;

    // Either F or M
    private String gender;

    private String company;

    private String badge;

    private LocalDate dob;

    private String jobNumber;

    private LocalDate hiredAt;

    private LocalDate effectedOn;

    private LocalDate terminatedAt;

    private Boolean hasPerDiem = false;

    private Boolean hasRigPay = false;

    private String activityCode;
}

package com.timaven.timekeeping.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@NoArgsConstructor
public class PurchaseOrderBilling {
    private Long id;
    private String purchaseOrderNumber;
    private String billingNumber;
    private String invoiceNumber;
    private BigDecimal freightShipping;
    private String otherCharge;
    private BigDecimal otherChargeAmount;
}

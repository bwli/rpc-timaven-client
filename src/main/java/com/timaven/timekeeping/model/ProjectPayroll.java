package com.timaven.timekeeping.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.math.BigDecimal;
import java.time.LocalDate;

@Getter
@Setter
@NoArgsConstructor
public class ProjectPayroll {

    private ProjectPayrollPK id;

    private Long projectId;

    private LocalDate weekEndDate;

    private Long employeeCount;

    private BigDecimal totalHours;

    private Long timesheets;

    private LocalDate approvedAt;

    private LocalDate finalizedAt;

    private int type;

    @Override
    public int hashCode() {
        if (null != id) {
            HashCodeBuilder hcb = new HashCodeBuilder();
            hcb.append(id);
            return hcb.toHashCode();
        }
        return super.hashCode();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ProjectPayroll)) return false;
        ProjectPayroll that = (ProjectPayroll) o;
        if (null != id) {
            EqualsBuilder eb = new EqualsBuilder();
            eb.append(id, that.id);
            return eb.isEquals();
        }
        return super.equals(o);
    }
}

package com.timaven.timekeeping.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.timaven.timekeeping.model.dto.AllocationTimeDto;
import com.timaven.timekeeping.model.dto.CostCodeTagAssociationDto;
import com.timaven.timekeeping.model.enums.AllocationTimeHourType;
import com.timaven.timekeeping.model.enums.CostCodeType;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = AllocationTime.class)
@Getter
@Setter
@NoArgsConstructor
public class AllocationTime {
    private Long id;

    private String empId;

    private String clientEmpId;

    private String badge;

    private String teamName;

    private BigDecimal stHour;

    private BigDecimal otHour;

    private BigDecimal dtHour;

    private AllocationTimeHourType extraTimeType;

    private BigDecimal totalHour;

    private BigDecimal allocatedHour;

    private BigDecimal netHour;

    private boolean hasPerDiem;

    private boolean hasMob;

    private String mobCostCode;

    private BigDecimal mobAmount = BigDecimal.ZERO;

    private BigDecimal mobMileageRate = BigDecimal.ZERO;

    private BigDecimal mobMileage = BigDecimal.ZERO;

    private BigDecimal perDiemAmount = BigDecimal.ZERO;

    private BigDecimal rigPay;

    private AllocationSubmission allocationSubmission;

    private Set<CostCodePerc> costCodePercs;

    private Set<PunchException> punchExceptions;

    private Set<AllocationRecord> allocationRecords;

    private Set<AllocationException> allocationExceptions;

    private AllocationException allocationException;

    private String firstName;

    private String lastName;

    private String jobNumber;

    private String maxTimeCostCode;

    private String perDiemCostCode;

    private Long weeklyProcessId;

    private WeeklyProcess weeklyProcess;

    private Employee employee;

    private PerDiem perDiem;

    private List<CostCodeTagAssociation> mobCostCodeTagAssociations;

    private List<CostCodeTagAssociation> perDiemCostCodeTagAssociations;

    // TM-307
    private Boolean isOffsite = false;

    private Integer mobDisplayOrder;

    private Set<ActivityCodePerc> activityCodePercs = new HashSet<>();

    public AllocationTime(AllocationTimeDto dto) {
        this.id = dto.getId();
        this.firstName = dto.getFirstName();
        this.lastName = dto.getLastName();
        this.empId = dto.getEmpId();
        this.badge = dto.getBadge();
        this.clientEmpId = dto.getClientEmpId();
        this.teamName = dto.getTeamName();
        this.stHour = dto.getStHour();
        this.otHour = dto.getOtHour();
        this.dtHour = dto.getDtHour();
        this.totalHour = dto.getTotalHour();
        this.netHour = dto.getNetHour();
        this.extraTimeType = dto.getExtraTimeType();
        this.allocatedHour = dto.getAllocatedHour();
        this.hasPerDiem = dto.isHasPerDiem();
        this.hasMob = dto.isHasMob();
        this.rigPay = dto.getRigPay();
        this.mobCostCode = dto.getMobCostCode();
        this.mobAmount = dto.getMobAmount() == null ? BigDecimal.ZERO : dto.getMobAmount();
        this.mobMileage = dto.getMobMileage() == null ? BigDecimal.ZERO : dto.getMobMileage();
        this.mobMileageRate = dto.getMobMileageRate() == null ? BigDecimal.ZERO : dto.getMobMileageRate();
        this.mobDisplayOrder = dto.getMobDisplayOrder();
        this.perDiemAmount = dto.getPerDiemAmount() == null ? BigDecimal.ZERO : dto.getPerDiemAmount();
        this.perDiemCostCode = dto.getPerDiemCostCode();
        this.weeklyProcessId = dto.getWeeklyProcessId();

        if (!CollectionUtils.isEmpty(dto.getPunchExceptions())) {
            this.punchExceptions = dto.getPunchExceptions().stream()
                    .map(PunchException::new).collect(Collectors.toSet());
        }

        if (!CollectionUtils.isEmpty(dto.getAllocationRecords())) {
            this.allocationRecords = dto.getAllocationRecords().stream()
                    .map(AllocationRecord::new).collect(Collectors.toSet());
        }

        if (!CollectionUtils.isEmpty(dto.getCostCodePercs())) {
            this.costCodePercs = dto.getCostCodePercs().stream()
                    .map(CostCodePerc::new).collect(Collectors.toSet());
        }

        this.allocationExceptions = new HashSet<>();
        if (dto.getAllocationException() != null) {
            if (StringUtils.hasText(dto.getAllocationException().getException()) || dto.getAllocationException().getId() != null) {
                this.allocationExceptions.add(new AllocationException(dto.getAllocationException()));
            }
        }

        if (!CollectionUtils.isEmpty(dto.getMobCostCodeTagAssociations())) {
            this.mobCostCodeTagAssociations = dto.getMobCostCodeTagAssociations().stream()
                    .map(a -> new CostCodeTagAssociation(a, CostCodeType.MOB)).collect(Collectors.toList());
        }
        if (!CollectionUtils.isEmpty(dto.getPerDiemCostCodeTagAssociations())) {
            this.perDiemCostCodeTagAssociations = dto.getPerDiemCostCodeTagAssociations().stream()
                    .map(a -> new CostCodeTagAssociation(a, CostCodeType.PERDIEM)).collect(Collectors.toList());
        }
    }

    public BigDecimal getStHour() {
        return stHour == null ? BigDecimal.ZERO : stHour;
    }

    public BigDecimal getOtHour() {
        return otHour == null ? BigDecimal.ZERO : otHour;
    }

    public BigDecimal getDtHour() {
        return dtHour == null ? BigDecimal.ZERO : dtHour;
    }

    public BigDecimal getTotalHour() {
        return totalHour == null ? BigDecimal.ZERO : totalHour;
    }

    public BigDecimal getAllocatedHour() {
        return allocatedHour == null ? BigDecimal.ZERO : allocatedHour;
    }

    public AllocationException getAllocationException() {
        if (allocationException == null && allocationExceptions != null) return allocationExceptions.stream()
                .findFirst().orElse(null);
        return allocationException;
    }

    public Set<AllocationException> getAllocationExceptions() {
        if (null == allocationExceptions) {
            allocationExceptions = new HashSet<>();
        }
        return allocationExceptions;
    }

    public String getFullName() {
        if (StringUtils.hasText(firstName) && StringUtils.hasText(lastName)) {
            return String.format("%s, %s", lastName, firstName);
        } else if (StringUtils.hasText(firstName)) {
            return firstName;
        } else if (StringUtils.hasText(lastName)) {
            return lastName;
        }
        return "Unknown";
    }

    @Override
    public int hashCode() {
        if (null != id) {
            HashCodeBuilder hcb = new HashCodeBuilder();
            hcb.append(id);
            return hcb.toHashCode();
        }
        return super.hashCode();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AllocationTime)) return false;
        if (null != id) {
            AllocationTime that = (AllocationTime) o;
            EqualsBuilder eb = new EqualsBuilder();
            eb.append(id, that.id);
            return eb.isEquals();
        }
        return super.equals(o);
    }
}

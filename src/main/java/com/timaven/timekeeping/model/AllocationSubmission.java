package com.timaven.timekeeping.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.timaven.timekeeping.model.dto.AllocationSubmissionDto;
import com.timaven.timekeeping.model.enums.AllocationSubmissionStatus;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.springframework.util.CollectionUtils;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = AllocationSubmission.class)
@Getter
@Setter
@NoArgsConstructor
public class AllocationSubmission implements Comparable<AllocationSubmission> {

    private Long id;
    private Long submitUserId;
    private Long approveUserId;
    private Long finalizeUserId;
    private LocalDateTime approvedAt;
    private LocalDateTime finalizedAt;
    private Long reportSubmissionId;
    private ReportSubmission reportSubmission;
    private String teamName;
    private AllocationSubmissionStatus status;
    private LocalDate dateOfService;
    private LocalDate payrollDate;
    private Long projectId;
    private LocalDateTime createdAt = LocalDateTime.now();
    private Set<AllocationTime> allocationTimes = new HashSet<>();
    private Boolean isBySystem = false;
    private Boolean billingPurposeOnly = false;
    private Boolean isManual = false;

    private User submitUser;
    private User approveUser;
    private Boolean exported = false;

    private boolean isTlo = false;

    public AllocationSubmission(AllocationSubmissionDto dto) {
        this.id = dto.getId();
        this.submitUserId = dto.getSubmitUserId();
        this.approveUserId = dto.getApproveUserId();
        this.finalizeUserId = dto.getFinalizeUserId();
        this.approvedAt = dto.getApprovedAt();
        this.finalizedAt = dto.getFinalizedAt();
        this.reportSubmissionId = dto.getReportSubmissionId();
        this.teamName = dto.getTeamName();
        this.status = dto.getStatus();
        this.dateOfService = dto.getDateOfService();
        this.projectId = dto.getProjectId();
        if (!CollectionUtils.isEmpty(dto.getAllocationTimes())) {
            this.allocationTimes = dto.getAllocationTimes().stream()
                    .map(AllocationTime::new).collect(Collectors.toSet());
        }
        this.billingPurposeOnly = dto.getBillingPurposeOnly() != null && dto.getBillingPurposeOnly();
        this.isManual = dto.getIsManual();
        this.payrollDate = dto.getPayrollDate();
    }

    public Set<AllocationTime> getAllocationTimes() {
        return allocationTimes == null ? new HashSet<>() : allocationTimes;
    }

    @Override
    public int hashCode() {
        HashCodeBuilder hcb = new HashCodeBuilder();
        if (AllocationSubmissionStatus.DENIED.equals(status)) {
            hcb.append(id);
        } else {
            hcb.append(projectId);
            hcb.append(teamName);
            hcb.append(dateOfService);
            hcb.append(payrollDate);
        }
        return hcb.toHashCode();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AllocationSubmission)) return false;
        AllocationSubmission that = (AllocationSubmission) o;
        EqualsBuilder eb = new EqualsBuilder();
        if (AllocationSubmissionStatus.DENIED.equals(status)) {
            eb.append(id, that.id);
        } else {
            eb.append(projectId, that.projectId);
            eb.append(teamName, that.teamName);
            eb.append(dateOfService, that.dateOfService);
            eb.append(payrollDate, that.payrollDate);
        }
        return eb.isEquals();
    }

    @Override
    public int compareTo(AllocationSubmission o) {
        int result = Comparator.comparing(AllocationSubmission::getProjectId, Comparator.nullsFirst(Comparator.naturalOrder()))
                .thenComparing(AllocationSubmission::getDateOfService, Comparator.nullsLast(Comparator.reverseOrder()))
                .thenComparing(AllocationSubmission::getTeamName, Comparator.nullsLast(Comparator.naturalOrder()))
                .thenComparing(AllocationSubmission::getPayrollDate, Comparator.nullsLast(Comparator.reverseOrder()))
                .compare(this, o);
        if (result == 0 && this.status == AllocationSubmissionStatus.DENIED) return 1;
        if (result == 0 && o.status == AllocationSubmissionStatus.DENIED) return -1;
        return result;
    }
}

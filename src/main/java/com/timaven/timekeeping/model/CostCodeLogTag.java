package com.timaven.timekeeping.model;

import lombok.Data;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.util.Comparator;

@Data
public class CostCodeLogTag implements Comparable<CostCodeLogTag> {
    private Long id;
    private String costCodeFull;
    private String tag;
    private Long projectId;

    @Override
    public int hashCode() {
        HashCodeBuilder hcb = new HashCodeBuilder();
        hcb.append(costCodeFull);
        hcb.append(projectId);
        hcb.append(tag);
        return hcb.toHashCode();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CostCodeLogTag)) return false;
        CostCodeLogTag that = (CostCodeLogTag) o;
        EqualsBuilder eb = new EqualsBuilder();
        eb.append(costCodeFull, that.costCodeFull);
        eb.append(projectId, that.projectId);
        eb.append(tag, that.tag);
        return eb.isEquals();
    }

    @Override
    public int compareTo(CostCodeLogTag o) {
        return Comparator.comparing(CostCodeLogTag::getTag).compare(this, o);
    }
}

package com.timaven.timekeeping.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.util.Comparator;
import java.util.Map;

@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = CostCodeTag.class)
@Getter
@Setter
@NoArgsConstructor
public class CostCodeTag implements Comparable<CostCodeTag> {

    private Long id;

    private Long typeId;

    private String codeName;

    private String description;

    private Map<String, Object> idCode;

    @Override
    public int hashCode() {
        HashCodeBuilder hcb = new HashCodeBuilder();
        hcb.append(id);
        return hcb.toHashCode();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CostCodeTag)) return false;
        CostCodeTag that = (CostCodeTag) o;
        EqualsBuilder eb = new EqualsBuilder();
        eb.append(id, that.id);
        return eb.isEquals();
    }

    @Override
    public int compareTo(CostCodeTag o) {
        return Comparator.comparing(CostCodeTag::getTypeId)
                .thenComparing(CostCodeTag::getCodeName)
                .compare(this, o);
    }
}

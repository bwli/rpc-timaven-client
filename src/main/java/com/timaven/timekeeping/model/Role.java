package com.timaven.timekeeping.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.util.Comparator;

/**
 * Entity bean with JPA annotations Hibernate provides JPA implementation
 */
@Getter
@Setter
@EqualsAndHashCode(of = "name")
@NoArgsConstructor
public class Role implements Comparator<Role> {

    private Long id;

    @NotNull
    private String name;

    private String description;

    private int displayOrder;

    private String displayName;

    @Override
    public int compare(Role role0, Role role1) {
        return role0.name.compareTo(role1.name);
    }
}

package com.timaven.timekeeping.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
@NoArgsConstructor
public class RequisitionGroup {
    private String requisitionNumber;
    private String purchaseOrderNumber;
    private LocalDate requisitionDate;
    private String vendorName;
    private String requestor;
    private String jobNumber;
    private String subJob;
    private long itemCount;
}

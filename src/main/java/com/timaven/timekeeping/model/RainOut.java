package com.timaven.timekeeping.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode(of = {"name", "projectId"})
public class RainOut {

    private Long id;

    private String name;

    private int time;

    private Long projectId;
}

package com.timaven.timekeeping.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
public class Craft implements Cloneable {

    private Long id;

    private String code;

    private Long projectId;

    private BigDecimal billableST;

    private BigDecimal billableOT;

    private BigDecimal billableDT;

    private String description;

    private BigDecimal perDiem;

    private Long perDiemId;

    private BigDecimal rigPay;

    private LocalDate startDate;

    private LocalDate endDate;

    private LocalDateTime createdAt;

    private String company;

    @Override
    public int hashCode() {
        HashCodeBuilder hcb = new HashCodeBuilder();
        if (null != id) {
            hcb.append(id);
            return hcb.toHashCode();
        } else {
            hcb.append(projectId);
            hcb.append(code);
            hcb.append(startDate);
            hcb.append(endDate);
        }
        return hcb.toHashCode();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Craft)) return false;
        Craft that = (Craft) o;
        EqualsBuilder eb = new EqualsBuilder();
        if (null != id || null != that.id) {
            eb.append(id, that.id);
        } else {
            eb.append(projectId, that.projectId);
            eb.append(code, that.code);
            eb.append(startDate, that.startDate);
            eb.append(endDate, that.endDate);
        }
        return eb.isEquals();
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}

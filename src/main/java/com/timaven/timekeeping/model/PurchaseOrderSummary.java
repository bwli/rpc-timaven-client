package com.timaven.timekeeping.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Objects;

@Getter
@Setter
@NoArgsConstructor
public class PurchaseOrderSummary {
    private String purchaseOrderNumber;
    private LocalDate purchaseOrderDate;
    private String vendorName;
    private String requestor;
    private String jobNumber;
    private String subJob;
    private Long submissionId;
    private BigDecimal totalExtendedCost;
    private int quantity;
    private int billedQuantity;
    private BigDecimal billedAmount;
    private LocalDate billedOn;
    private String comment;
    private boolean flag;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PurchaseOrderSummary that = (PurchaseOrderSummary) o;
        if (purchaseOrderNumber != null && that.purchaseOrderNumber != null)
            return purchaseOrderNumber.equals(that.getPurchaseOrderNumber());
        return false;
    }

    @Override
    public int hashCode() {
        if (purchaseOrderNumber != null) return Objects.hash(purchaseOrderNumber);
        return super.hashCode();
    }
}
